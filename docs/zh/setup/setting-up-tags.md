# 设置标签 Tags

MkDocs 的 Material 主题增加了一流的对使用标签对页面进行分类的支持，这增加了将相关页面分组并通过搜索和专门的[标签索引][tags index]使它们可被发现的可能性。如果您的文档很大，标签可以帮助您更快地找到相关信息。

  [tags index]: #adding-a-tags-index

## 配置

### 内置 tags 插件

<!-- md:version 8.2.0 -->
<!-- md:plugin -->


内置的标签插件增加了使用标签对任何页面进行分类的能力，这是页面前言部分。为了添加对标签的支持，请在 `mkdocs.yml` 中添加以下行：



``` yaml
plugins:
  - tags
```

有关所有设置的列表，请参阅[插件文档][plugin documentation]。

  [plugin documentation]: ../plugins/tags.md

#### 高级设置

<!-- md:sponsors -->
<!-- md:version insiders-4.48.0 -->
<!-- md:flag experimental -->


以下高级设置目前仅保留给我们的[赞助者][Insiders]。它们完全是可选的，并且只为标签插件添加了额外功能：

<!-- - [`listings_layout`][config.listings_layout] -->
- [`listings_toc`][config.listings_toc]


我们近期将会在这里添加更多设置。


  [Insiders]: ../insiders/index.md
  [config.listings_layout]: ../plugins/tags.md#config.listings_layout
  [config.listings_toc]: ../plugins/tags.md#config.listings_toc

### 标签图标和标识符


<!-- md:version 8.5.0 -->
<!-- md:flag experimental -->
<!-- md:example tags-with-icons -->


每个标签都可以与一个图标相关联，该图标随后将在标签内部呈现。在为标签分配图标之前，请通过向 `mkdocs.yml` 中添加以下内容，将每个标签与一个唯一标识符相关联：

``` yaml
extra:
  tags:
    <tag>: <identifier> # (1)!
```

1、标识符只能包含字母数字字符以及破折号和下划线。例如，如果您有一个标签 C`ompatibility`，您可以将其标识符设置为 `compat`：

    ``` yaml
    extra:
      tags:
        Compatibility: compat
    ```

    标签之间可以重复使用标识符。未明确关联的标签将使用默认的标签图标，即 :material-pound:。

接下来，您可以通过在 `mkdocs.yml` 的 `theme.icon` 配置设置下添加以下行，将每个标识符与图标（甚至是[自定义图标][custom icon]）相关联：

=== "Tag icon"

    ``` yaml
    theme:
      icon:
        tag:
          <identifier>: <icon> # (1)!
    ```

    1、 输入一些关键字以使用我们的[图标搜索][icon search]功能找到完美的图标，并点击简码以将其复制到剪贴板：


        <div class="mdx-iconsearch" data-mdx-component="iconsearch">
          <input class="md-input md-input--stretch mdx-iconsearch__input" placeholder="Search icon" data-mdx-component="iconsearch-query" value="tag" />
          <div class="mdx-iconsearch-result" data-mdx-component="iconsearch-result" data-mdx-mode="file">
            <div class="mdx-iconsearch-result__meta"></div>
            <ol class="mdx-iconsearch-result__list"></ol>
          </div>
        </div>

=== "Tag default icon"

    ``` yaml
    theme:
      icon:
        tag:
          default: <icon>
    ```

??? example "Expand to inspect example"

    ``` yaml
    theme:
      icon:
        tag:
          html: fontawesome/brands/html5
          js: fontawesome/brands/js
          css:  fontawesome/brands/css3
    extra:
      tags:
        HTML5: html
        JavaScript: js
        CSS: css
    ```

  [custom icon]: changing-the-logo-and-icons.md#additional-icons
  [icon search]: ../reference/icons-emojis.md#search

## 文章使用

### 添加 Tags

<!-- md:version 8.2.0 -->
<!-- md:example tags -->

When the [built-in tags plugin] is enabled, tags can be added for a document
with the front matter `tags` property. Add the following lines at the top of a
Markdown file:

当启用[内置标签插件][built-in tags plugin]时，可以通过前言（front matter）中的 `tags` 属性为文档添加标签。在 Markdown 文件的顶部添加以下行：

``` sh
---
tags:
  - HTML5
  - JavaScript
  - CSS
---

...
```

该页面现在将在主标题上方和搜索预览中显示这些标签，从而可以 __通过标签查找页面__ 。



??? question "如何为整个文件夹设置标签?"

    借助[内置meta插件][built-in meta plugin]，您可以通过在相应文件夹中创建一个包含以下内容的`.meta.yml`文件，来确保为整个部分及其所有嵌套页面设置标签：

    ``` yaml
    tags:
      - HTML5
      - JavaScript
      - CSS
    ```

    `.meta.yml`中设置的标签将与页面定义的标签进行合并和去重，这意味着您可以在.meta.yml中定义公共标签，然后为每个页面添加特定标签。`.meta.yml`中的标签会被附加到页面标签之后。



  [built-in tags plugin]: ../plugins/tags.md
  [built-in meta plugin]: ../plugins/meta.md

### 添加一个 Tag 索引

<!-- md:version 8.2.0 -->
<!-- md:example tags -->

[内置标签插件][built-in tags plugin]允许定义一个文件来渲染标签索引，该文件可以是nav部分中的任何页面。要添加标签索引，请创建一个页面，例如`tags.md`：

``` markdown
# Tags

Following is a list of relevant tags:

<!-- material/tags -->
```

然后在`mkdocs.yml` 文件中，添加如下内容：

``` yaml
plugins:
  - tags:
      tags_file: tags.md # (1)!
```

1、 在使用[Insiders]时，此设置不是必需的。


请注意，`tags.md`的路径是相对于`docs/`目录的。


标签标记指定了标签索引的位置，即当页面被渲染时，它会被实际的标签索引所替换。您可以在标记之前和之后包含任意内容：

[![Tags index][tags index enabled]][tags index enabled]

  [tags.tags_file]: #tags-file
  [tags index enabled]: ../assets/screenshots/tags-index.png

### 高级特性

[Insiders] ships a __ground up rewrite of the tags plugin__ which is infinitely
more powerful than the current version in the community edition. It allows
for an arbitrary number of tags indexes (listings), [scoped listings],
[shadow tags], [nested tags], and much more.

[Insiders] 提供了一个 __从头开始重写的标签插件__，其功能比社区版的当前版本要强大得多。它允许任意数量的标签索引（列表）、[范围列表][scoped listings]、[影子标签][shadow tags]、[嵌套标签] [nested tags]等等。

  [scoped listings]: #scoped-listings
  [shadow tags]: #shadow-tags
  [nested tags]: #nested-tags

#### 可配置的列表

<!-- md:sponsors -->
<!-- md:version insiders-4.48.0 -->
<!-- md:flag experimental -->


可以在`mkdocs.yml`中或直接在 Markdown 文档中您放置标记的位置配置列表。以下是一些示例：

- 使用[范围列表][scoped listings]：将标签索引限制为与页面所在文档的子部分处于同一级别的页面：


    ``` html
    <!-- material/tags { scope: true } -->
    ```

- __仅列出特定标签__：将标签索引限制为单个或多个选定的标签，例如`Foo`和`Bar`，排除所有其他标签：


    ``` html
    <!-- material/tags { include: [Foo, Bar] } -->
    ```

- __排除具有特定标签的页面__：不包括具有特定标签的页面，例如`Internal`。这可以是任何标签，包括影子标签

    ``` html
    <!-- material/tags { exclude: [Internal] } -->
    ```

- __在目录中启用或禁用标签__：指定目录是否在最近的标题下列出所有标签：

    ``` html
    <!-- material/tags { toc: false } -->
    ```

请参阅[列表配置][listing configuration]以了解所有选项。

  [listing configuration]: ../plugins/tags.md#listing-configuration

#### 范围列表

<!-- md:sponsors -->
<!-- md:version insiders-4.48.0 -->
<!-- md:flag experimental -->

如果您的文档很大，您可能希望考虑使用范围列表，它只包括与包含列表的页面处于同一级别或更低级别的页面。只需使用：

``` html
<!-- material/tags { scope: true } -->
```

如果您计划使用多个范围索引，最好在 `mkdocs.yml` 中定义一个列表配置，然后可以通过其ID进行引用：


``` yaml
plugins:
  - tags:
      listings_map:
        scoped:
          scope: true
```

您现在可以使用：


``` html
<!-- material/tags scoped -->
```

#### 影子标签

<!-- md:sponsors -->
<!-- md:version insiders-4.48.0 -->
<!-- md:flag experimental -->

影子标签是仅用于组织的标签，可以通过一个简单的标志来包含或排除它们以进行渲染。它们可以在[`shadow_tags`][config.shadow_tags]设置中进行枚举：

``` yaml
plugins:
  - tags:
      shadow_tags:
        - Draft
        - Internal
```

如果文档被标记为`Draft`，则该标签只有在启用[`shadow`][config.shadow]设置时才会被渲染，禁用时则会被排除。这为使用标签进行结构化提供了极好的机会。

  [config.shadow]: ../plugins/tags.md#config.shadow
  [config.shadow_tags]: ../plugins/tags.md#config.shadow_tags

#### 嵌套标签

<!-- md:sponsors -->
<!-- md:version insiders-4.48.0 -->
<!-- md:flag experimental -->

[Insiders] 提供了对嵌套标签的支持。[`tags_hierarchy_separator`][config.tags_hierarchy_separator]允许创建标签的层次结构，例如`Foo/Bar`。嵌套标签将作为父标签的子标签进行渲染：

``` yaml
plugins:
  - tags:
      tags_hierarchy: true
```

  [config.tags_hierarchy_separator]: ../plugins/tags.md#config.tags_hierarchy_separator

### 隐藏页面上的标签


虽然标签会在主标题上方进行渲染，但有时可能希望隐藏特定页面上的标签，这可以通过使用前言的 `hide` 属性来实现：

``` yaml
---
hide:
  - tags
---

# Page title
...
```

## 参考
- <https://github.com/squidfunk/mkdocs-material/blob/master/docs/setup/setting-up-tags.md>