# 添加评论功能

Material-for-MkDocs 提供了一个通过 [主题扩展] 轻松将您选择的第三方评论系统添加到任何页面底部的功能。作为示例，我们将集成 [Giscus]，它是一个开源的、免费的评论系统，使用 GitHub Discussions 作为后端。



## 自定义

### Giscus 集成

在使用 [Giscus] 前，你需要完成以下步骤：

1.  __安装 [Giscus GitHub App]__ 并且授权仓库访问权限，注意可以与文档是不同的仓库。 
2.  __访问 [Giscus] 并生成代码片段__ ，可以通过评论系统的配置工具来生成，复制代码片段为下一步做准备。这个代码片段类似如下示例：

    ``` html
    <script
      src="https://giscus.app/client.js"
      data-repo="<username>/<repository>"
      data-repo-id="..."
      data-category="..."
      data-category-id="..."
      data-mapping="pathname"
      data-reactions-enabled="1"
      data-emit-metadata="1"
      data-theme="light"
      data-lang="en"
      crossorigin="anonymous"
      async
    >
    </script>
    ```

在  [`comments.html`][comments] 部分（默认为空）是配置  [Giscus] 代码片段最合适的地方。
按照 [主题扩展] 和  [覆盖 `comments.html` 部分][overriding partials] 的说明进行配置：

``` html hl_lines="3"
{% if page.meta.comments %}
  <h2 id="__comments">{{ lang.t("meta.comments") }}</h2>
  <!-- Insert generated snippet here -->

  <!-- Synchronize Giscus theme with palette -->
  <script>
    var giscus = document.querySelector("script[src*=giscus]")

    // Set palette on initial load
    var palette = __md_get("__palette")
    if (palette && typeof palette.color === "object") {
      var theme = palette.color.scheme === "slate"
        ? "transparent_dark"
        : "light"

      // Instruct Giscus to set theme
      giscus.setAttribute("data-theme", theme) // (1)!
    }

    // Register event handlers after documented loaded
    document.addEventListener("DOMContentLoaded", function() {
      var ref = document.querySelector("[data-md-component=palette]")
      ref.addEventListener("change", function() {
        var palette = __md_get("__palette")
        if (palette && typeof palette.color === "object") {
          var theme = palette.color.scheme === "slate"
            ? "transparent_dark"
            : "light"

          // Instruct Giscus to change theme
          var frame = document.querySelector(".giscus-frame")
          frame.contentWindow.postMessage(
            { giscus: { setConfig: { theme } } },
            "https://giscus.app"
          )
        }
      })
    })
  </script>
{% endif %}
```

这个代码块确保当调色板设置为 `slate` 时，[Giscus]呈现一个黑暗的主题。请注意，有多个黑暗主题可用，所以你可以根据自己的喜好进行更改。

将突出显示的行替换为在上一步中使用[Giscus]配置工具生成的代码片段。
如果复制上面的代码片段，你可以通过设置 Markdown 文档页面的 `comments` 属性为 `true` 来启用页面上的注释：

``` yaml
---
comments: true
---

# Page title
...
```

如果希望为整个文件夹启用注释，可以使用 [built-in meta plugin].

  [Giscus GitHub App]: https://github.com/apps/giscus
  [主题扩展]: ../customization.md#extending-the-theme
  [comments]: https://github.com/squidfunk/mkdocs-material/blob/master/src/templates/partials/comments.html
  [overriding partials]: ../customization.md#overriding-partials
  [built-in meta plugin]: ../plugins/meta.md
  [Giscus]: https://giscus.app/