# 网站优化

Material-for-MkDocs 默认情况下允许构建优化良好的网站，这些网站在搜索引擎中排名靠前，加载速度快（即使在慢速网络上），并且在没有 JavaScript 的情况下也能完美运行。此外，[内置优化插件] 还增加了对其他有用自动优化技术的支持。

  [内置优化插件]: #built-in-optimize-plugin

## 配置

### 内置项目插件

<!-- md:sponsors -->
<!-- md:version insiders-4.38.0 -->
<!-- md:plugin [projects] – built-in -->
<!-- md:flag experimental -->

内置的项目插件（projects plugin）允许您将文档拆分成多个独立的 MkDocs 项目，同时构建它们并一起提供服务。要在 mkdocs.yml 文件中配置此插件，您需要添加相关的配置信息。

``` yaml
plugins:
  - projects
```

要了解所有设置的完整列表，请查阅[插件文档]。

  [projects]: ../plugins/projects.md
  [插件文档]: ../plugins/projects.md

??? info "项目插件示例"

    理想情况下使用项目插件的场景：

    - 构建多语言站点
    - 在文档中心构建博客
    - 为了更好的性能拆分大型代码库

    注意该插件目前处于实验性阶段。该插件虽然早已经发布，但其功能、性能或稳定性可能尚未完全成熟。开发者选择在这个阶段发布插件，通常是为了能够从早期用户那里获得反馈，以便共同改进插件，并在发现新的用例时进一步增强其功能。

#### scope

<!-- md:version 8.0.0 -->
<!-- md:default none -->

在处理多个 MkDocs 项目时，如果您希望在所有项目中共享用户级别的设置，比如选定的颜色主题或 [Cookie 同意设置]，您可以通过在 `mkdocs.yml` 配置文件中添加特定的配置来实现这一点。

``` yaml
extra:
  scope: /
```

!!! example "如何工作"

    假设您的网站结构如下：

    ```
    .
    └── /
        ├── subsite-a/
        ├── subsite-b/
        └── subsite-c/
    ```
    默认情况下，每个站点都有自己的 scope (`/subsite-a/`, `/subsite-b/`, `/subsite-c/`).  在 `mkdocs.yml` 中通过如下行来修改这个特性：

    ``` yaml
    extra:
      scope: /
    ```

    通过将其设置为 `/`，它应该允许您共享以下首选项在主站点和所有子站点：

    - [Cookie 同意设置][Cookie 同意设置]
    - [链接内容标签，即活动标签] 
    - [Color palette][color palette]

  [Scope support]: https://github.com/squidfunk/mkdocs-material/releases/tag/8.0.0
  [cookie consent]: ../setup/ensuring-data-privacy.md#cookie-consent
  [链接内容标签，即活动标签]: ../reference/content-tabs.md
  [Cookie 同意设置]: ../setup/changing-the-colors.md#color-palette

### 内置优化插件

<!-- md:sponsors -->
<!-- md:version insiders-4.29.0 -->
<!-- md:plugin [optimize] – built-in -->
<!-- md:flag experimental -->


内置的优化插件自动识别和优化所有媒体文件，使用压缩和转换技术将文件作为构建的一部分。

添加以下几行添加到 `mkdocs.yml`：

``` yaml
plugins:
  - optimize
```

有关所有设置的列表，请参考 [插件文档][optimize]。

  [optimize]: ../plugins/optimize.md