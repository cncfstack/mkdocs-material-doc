# 设置社交明信片

Material-for-MkDocs 可以为您的文档自动生成美观的社交卡片，这些卡片在社交媒体平台上会显示为链接预览。您可以从多个[预设布局][default layouts]中选择，或者创建[自定义布局][custom layouts]以匹配您独特的风格和品牌形象。

---

:fontawesome-brands-youtube:{ style="color: #EE0F0F" }
__[如何构建自定义社交卡片][How to build custom social cards]__ by @james-willett – :octicons-clock-24: 24分钟 – 学习如何为每一页自动创建与您品牌形象完美匹配的完全自定义的社交卡片！

  [How to build custom social cards]: https://www.youtube.com/watch?v=4OjnOc6ftJ8

<figure markdown>

[![Layout default variant]][Layout default variant]

  <figcaption markdown>

Social card of our [formatting] reference

  </figcaption>
</figure>

  [default layouts]: ../plugins/social.md#layouts
  [custom layouts]: #customization
  [formatting]: ../reference/formatting.md
  [Layout default variant]: ../assets/screenshots/social-cards-variant.png

## 配置

### 内置社交插件


<!-- md:version 8.5.0 -->
<!-- md:plugin -->
<!-- md:flag experimental -->

内置社交插件会自动为每一页生成一个自定义的预览图片。安装所有[图像处理依赖项][dependencies for image processing，并在`mkdocs.yml`中添加以下行：


``` yaml
plugins:
  - social
```

有关所有设置的列表，请参阅[插件文档][plugin documentation]。


  [plugin documentation]: ../plugins/social.md

!!! info " [`site_url`][site_url] 必须设置"

    请注意，在使用社交插件时，您必须设置[`site_url`][site_url]，否则生成的卡片将无法正确链接。像 X 和 Facebook 这样的社交媒体服务要求社交预览指向绝对URL，而插件只有在设置了[`site_url`][site_url]时才能计算出这个URL。例如：

    ``` yaml
    site_url: https://example.com
    ```

  [dependencies for image processing]: ../plugins/requirements/image-processing.md
  [site_url]: https://www.mkdocs.org/user-guide/configuration/#site_url

## 文章使用


如果你想调整社交卡片的标题或为其设置自定义描述，你可以设置元数据中的[`title`][Changing the title]和[`description`][Changing the description]属性，这些属性会优先于默认值，或者使用：

- [`cards_layout_options.title`](../plugins/social.md#option.title)
- [`cards_layout_options.description`](../plugins/social.md#option.description)

  [Changing the title]: ../reference/index.md#setting-the-page-title
  [Changing the description]: ../reference/index.md#setting-the-page-description

### 选择字体

有些字体不包含 CJK（中文、日文、韩文）字符，例如[默认字体`Roboto`][font]。如果您的`site_name`、`site_description`或页面标题包含CJK字符，请从[Google Fonts]中选择另一个包含CJK字符的字体，例如`Noto Sans`字体家族中的一个：

=== "Chinese (Simplified)"

    ``` yaml
    plugins:
      - social:
          cards_layout_options:
            font_family: Noto Sans SC
    ```

=== "Chinese (Traditional)"

    ``` yaml
    plugins:
      - social:
          cards_layout_options:
            font_family: Noto Sans TC
    ```

=== "Japanese"

    ``` yaml
    plugins:
      - social:
          cards_layout_options:
            font_family: Noto Sans JP
    ```

=== "Korean"

    ``` yaml
    plugins:
      - social:
          cards_layout_options:
            font_family: Noto Sans KR
    ```

  [font]: changing-the-fonts.md#regular-font

### 更改布局

<!-- md:version insiders-4.37.0 -->
<!-- md:flag metadata -->
<!-- md:flag experimental -->

如果你想给不同的页面设置不同的布局 (例如：推广宣传页), 您可以使用在页面的元数据属性中使用 `social` [`cards_layout`](../plugins/social.md#meta.social.cards_layout) 设置，就像在`mkdocs.yml`中配置的一样：


``` yaml
---
social:
  cards_layout: custom
---

# Page title
...
```

您可以使用[内置meta插件][built-in meta plugin]为整个文档子树应用这些更改，例如，为您的博客和API参考生成不同的社交卡片。


  [built-in meta plugin]: ../plugins/meta.md

### 参数化布局


<!-- md:version insiders-4.37.0 -->
<!-- md:flag metadata -->
<!-- md:flag experimental -->


除了改变整个布局之外，您还可以覆盖布局所公开的所有选项。这意味着您可以使用自定义的元数据属性（如`tags`（标签）、`date`（日期）、`author`（作者）或您能想到的任何其他属性）对社交卡片进行参数化设置。只需定义[cards_layout_options](../plugins/social.md#meta.social.cards_layout_options)即可。

``` yaml
---
social:
  cards_layout_options:
    background_color: blue # Change background color
    background_image: null # Remove background image
---

# Page title
...
```


您可以通过使用[内置元数据插件][built-in meta plugin]，将这些更改应用于您的文档的整个子树，例如，为您的博客和API参考生成不同的社交卡片。

### 禁用社交明信片

<!-- md:version insiders-4.37.0 -->
<!-- md:flag metadata -->
<!-- md:flag experimental -->

如果您希望禁用某个页面的社交名片，只需在该 Markdown 文档的元数据中添加以下内容：


``` yaml
---
social:
  cards: false
---

# Page title
...
```

## 自定义

<!-- md:sponsors -->
<!-- md:version insiders-4.33.0 -->
<!-- md:flag experimental -->

[Insiders] 版本对[内置社交插件][built-in social plugin]进行了彻底的重写，并引入了一个全新的布局系统，该系统结合了 YAML 和 [Jinja 模板][Jinja templates] —— 与 Material-for-MkDocs 用于 HTML 模板化的同一引擎——从而允许创建复杂的自定义布局：



<div class="mdx-social">
  <div class="mdx-social__layer">
    <div class="mdx-social__image">
      <span class="mdx-social__label">Layer 0</span>
      <img src="../../../assets/screenshots/social-cards-layer-0.png" />
    </div>
  </div>
  <div class="mdx-social__layer">
    <div class="mdx-social__image">
      <span class="mdx-social__label">Layer 1</span>
      <img src="../../../assets/screenshots/social-cards-layer-1.png" />
    </div>
  </div>
  <div class="mdx-social__layer">
    <div class="mdx-social__image">
      <span class="mdx-social__label">Layer 2</span>
      <img src="../../../assets/screenshots/social-cards-layer-2.png" />
    </div>
  </div>
  <div class="mdx-social__layer">
    <div class="mdx-social__image">
      <span class="mdx-social__label">Layer 3</span>
      <img src="../../../assets/screenshots/social-cards-layer-3.png" />
    </div>
  </div>
  <div class="mdx-social__layer">
    <div class="mdx-social__image">
      <span class="mdx-social__label">Layer 4</span>
      <img src="../../../assets/screenshots/social-cards-layer-4.png" />
    </div>
  </div>
  <div class="mdx-social__layer">
    <div class="mdx-social__image">
      <span class="mdx-social__label">Layer 5</span>
      <img src="../../../assets/screenshots/social-cards-layer-5.png" />
    </div>
  </div>
</div>

社交名片由多个图层组成，这与它们在 Adobe Photoshop 等图形设计软件中的表示方式类似。由于为每页生成的卡片中有很多图层是通用的（例如背景或徽标），内置社交插件可以自动去重图层并仅渲染一次，从而大大加快卡片的生成速度。生成的卡片会被缓存，以确保只有在内容发生变化时才会重新生成。

布局采用YAML语法编写。在开始创建自定义布局之前，最好先[研究一下预设计的布局][study the pre-designed layouts]（链接到[Insiders]存储库），以便更好地了解它们的工作原理。然后，创建一个新的布局并在`mkdocs.yml`中引用它：

=== ":octicons-file-code-16: `layouts/custom.yml`"

    ``` yaml
    size: { width: 1200, height: 630 }
    layers: []
    ```

=== ":octicons-file-code-16: `mkdocs.yml`"

    ``` yaml
    plugins:
      - social:
          cards_layout_dir: layouts
          cards_layout: custom
          debug: true
    ```



请注意，应省略`.yml`文件扩展名。接下来，运行`mkdocs serve`，并观察`.cache`目录是如何被生成的卡片填充的。在编辑器中打开任何一张卡片，这样您就可以立即看到所做的更改。由于我们还没有定义任何图层，所以这些卡片是透明的。

以下部分将解释如何创建自定义布局。


  [Insiders]: ../insiders/index.md
  [built-in social plugin]: ../plugins/social.md
  [Google Fonts]: https://fonts.google.com/
  [Jinja templates]: https://jinja.palletsprojects.com/en/3.1.x/
  [study the pre-designed layouts]: https://github.com/squidfunk/mkdocs-material-insiders/tree/master/src/plugins/social/layouts

### 尺寸和偏移量

每个图层都有一个与之相关的尺寸和偏移量，它们都是以像素为单位定义的。`尺寸`由宽度（`width`）和高度（`heigh`t）属性定义，而偏`移量`则由`x和y`属性定义：

``` yaml
size: { width: 1200, height: 630 }
layers:
  - size: { width: 1200, height: 630 }
    offset: { x: 0, y: 0 }
```

如果省略了 `size` 尺寸，它将默认为布局的尺寸。如果省略了 `offset`偏移量，它将默认为左上角，即默认的原点 `origin`。保存布局并重新加载进行渲染：


![Layer size]


图层轮廓和网格是可见的，因为我们在`mkdocs.yml`中启用了[`debug`][debug]模式。左上角显示了图层索引和偏移量，这对于对齐和构图非常有用。


  [Layer size]: ../assets/screenshots/social-cards-layer-size.png
  [debug]: ../plugins/social.md#debugging

#### 原点 `origin`

<!-- md:version insiders-4.35.0 -->
<!-- md:flag experimental -->


`x和y`值的原点可以更改，以便将图层与布局的边缘或角落之一对齐，例如，与布局的右下角对齐：


``` yaml hl_lines="5"
size: { width: 1200, height: 630 }
layers:
  - size: { width: 1200, height: 630 }
    offset: { x: 0, y: 0 }
    origin: end bottom
```

下面表格是当前支持的值：

<figure markdown>

| Origin         |                 |              |
| -------------- | --------------- | ------------ |
| :material-arrow-top-left:    `start top`    | :material-arrow-up:     `center top`    | :material-arrow-top-right:    `end top`    |
| :material-arrow-left:        `start center` | :material-circle-small: `center`        | :material-arrow-right:        `end center` |
| :material-arrow-bottom-left: `start bottom` | :material-arrow-down:   `center bottom` | :material-arrow-bottom-right: `end bottom` |

  <figcaption>
    Supported values for origin
  </figcaption>
</figure>

### 背景

每一层都可以被分配一个背景颜色和图像。如果两者都提供，颜色将覆盖在图像之上，从而实现半透明、有色调的背景效果：

=== "背景颜色"

    ``` yaml
    size: { width: 1200, height: 630 }
    layers:
      - background:
          color: "#4051b5"
    ```

    ![Layer background color]

=== "背景图片"

    ``` yaml
    size: { width: 1200, height: 630 }
    layers:
      - background:
          image: layouts/background.png
    ```

    ![Layer background image]

=== "带有色调的背景图"

    ``` yaml
    size: { width: 1200, height: 630 }
    layers:
      - background:
          image: layouts/background.png
          color: "#4051b5ee" # (1)!
    ```

    1、颜色值可以设置为[CSS颜色关键字][CSS color keyword]，或者3、4、6或8位十六进制颜色代码，从而实现半透明图层。

    ![Layer background]


背景图像会自动缩放以适应图层，同时保持宽高比。注意我们省略了`size`和`offset`属性，因为我们希望背景图像填充社交卡片的整个区域。

[Layer background color]: ../assets/screenshots/social-cards-layer-background-color.png
[Layer background image]: ../assets/screenshots/social-cards-layer-background-image.png
[Layer background]: ../assets/screenshots/social-cards-layer-background.png

### 排版

现在，我们可以添加来自Markdown文件的动态排版——这正是[内置社交插件][built-in social plugin]的真正存在意义。[Jinja模板][Jinja templates]被用来渲染一个文本字符串，然后将其添加到图像上：

``` yaml
size: { width: 1200, height: 630 }
layers:
  - size: { width: 832, height: 310 }
    offset: { x: 62, y: 160 }
    typography:
      content: "{{ page.title }}" # (1)!
      align: start
      color: white
      line:
        amount: 3
        height: 1.25
      font:
        family: Roboto
        style: Bold
```

1、 下面这些变量可以在 [Jinja templates] 中使用：

    - [`config.*`][config variable]
    - [`page.*`][page variable]
    - [`layout.*`][layout options]

    作者可以自由地定义`layout.*`选项，这些选项可用于从`mkdocs.yml`向布局传递任意数据。

这会在页面上渲染一个文本层，该文本层包含页面的标题，行高为1.25，最多3行。该插件会根据行高、行数以及上升部（ascender）和下降部（descender）等字体度量标准自动计算字体大小。[^2] 渲染效果如下：

  [^2]:
    如果插件要求作者手动指定字体大小和行高，那么就无法保证文本能够完全适应图层。因此，我们实现了一种声明式方法，其中作者指定所需的行高和行数，而插件则自动计算字体大小。

![Layer typography]

  [config variable]: https://www.mkdocs.org/dev-guide/themes/#config
  [page variable]: https://www.mkdocs.org/dev-guide/themes/#page
  [Layer typography]: ../assets/screenshots/social-cards-layer-typography.png

#### 内容溢出 Overflow

如果文本超出了图层，会有两种可能的行为：要么文本自动截断并以省略号表示缩短，要么文本自动缩小以适应图层：

``` { .markdown .no-copy }
# If we use a very long headline, we can see how the text will be truncated
```

=== ":octicons-ellipsis-16: Ellipsis"

    ![Layer typography ellipsis]

=== ":material-arrow-collapse: Shrink"

    ![Layer typography shrink]


虽然使用省略号截断是默认行为，但通过将`overflow`设置为`shrink`可以启用自动缩小功能：


``` yaml hl_lines="7"
size: { width: 1200, height: 630 }
layers:
  - size: { width: 832, height: 310 }
    offset: { x: 62, y: 160 }
    typography:
      content: "{{ page.title }}"
      overflow: shrink
      align: start
      color: white
      line:
        amount: 3
        height: 1.25
      font:
        family: Roboto
        style: Bold
```

  [Layer typography ellipsis]: ../assets/screenshots/social-cards-layer-typography-ellipsis.png
  [Layer typography shrink]: ../assets/screenshots/social-cards-layer-typography-shrink.png

#### 对齐

文本可以对齐到图层的所有角落和边缘。例如，如果我们想将文本对齐到图层的中间，我们可以将`align`设置为`start center`，这将呈现为：

![Layer typography align]

  [Layer typography align]: ../assets/screenshots/social-cards-layer-typography-align.png

下面表格中显示当前支持的值

<figure markdown>

| Alignment      |                 |              |
| -------------- | --------------- | ------------ |
| :material-arrow-top-left:    `start top`    | :material-arrow-up:     `center top`    | :material-arrow-top-right:    `end top`    |
| :material-arrow-left:        `start center` | :material-circle-small: `center`        | :material-arrow-right:        `end center` |
| :material-arrow-bottom-left: `start bottom` | :material-arrow-down:   `center bottom` | :material-arrow-bottom-right: `end bottom` |

  <figcaption>
    Supported values for text alignment
  </figcaption>
</figure>

#### 字体

[内置社交插件][built-in social plugin]与[Google Fonts]集成，并会自动为您下载字体文件。`font`属性接受`family`和`style`属性，其中`family`必须设置为字体的名称，而`style`则设置为支持的字体样式之一。例如，将`family`设置为`Roboto`将自动下载以下文件：


``` { .sh .no-copy #example }
.cache/plugins/social/fonts
└─ Roboto/
    ├─ Black.ttf
    ├─ Black Italic.ttf
    ├─ Bold.ttf
    ├─ Bold Italic.ttf
    ├─ Italic.ttf
    ├─ Light.ttf
    ├─ Light Italic.ttf
    ├─ Medium.ttf
    ├─ Medium Italic.ttf
    ├─ Regular.ttf
    ├─ Thin.ttf
    └─ Thin Italic.ttf
```

在这种情况下，作者可以使用`Bold`或`Medium Italic`作为`style`。如果图层中指定的字体样式不属于该字体家族，字体将始终回退到`Regular`样式，并在[`debug`][debug]模式下打印警告，因为`Regular`样式包含在所有字体家族中。

### Icons

作者可以利用 Material-for-MkDocs 提供的全套图标，甚至可以通过使用主题扩展并按照[附加图标][additional icons]指南中描述的过程提供自定义图标。还可以使用`color`属性为图标着色：

``` yaml
size: { width: 1200, height: 630 }
layers:
  - background:
      color: "#4051b5"
  - size: { width: 144, height: 144 }
    offset: { x: 992, y: 64 }
    icon:
      value: material/cat
      color: white
```

这将把图标渲染在社交卡片的右上角：


![Layer icon]


可能性是无限的。例如，可以使用图标来绘制形状，如圆形：


``` yaml
size: { width: 1200, height: 630 }
layers:
  - background:
      color: "#4051b5"
  - size: { width: 2400, height: 2400 }
    offset: { x: -1024, y: 64 }
    icon:
      value: material/circle
      color: "#5c6bc0"
  - size: { width: 1800, height: 1800 }
    offset: { x: 512, y: -1024 }
    icon:
      value: material/circle
      color: "#3949ab"
```

这将在背景中添加两个圆形：

![Layer icon circles]

### Tags


新的[内置社交插件][built-in social plugin]为您的网站添加的元标签提供了极大的灵活性，这些元标签对于指导X或Discord等服务如何显示您的社交卡片至关重要。所有默认布局都使用以下一组标签，您可以将其复制到您的布局并进行调整：


``` yaml
definitions:

  - &page_title_with_site_name >-
    {%- if not page.is_homepage -%}
      {{ page.meta.get("title", page.title) }} - {{ config.site_name }}
    {%- else -%}
      {{ page.meta.get("title", page.title) }}
    {%- endif -%}

  - &page_description >-
    {{ page.meta.get("description", config.site_description) or "" }}

tags:

  og:type: website
  og:title: *page_title_with_site_name
  og:description: *page_description
  og:image: "{{ image.url }}"
  og:image:type: "{{ image.type }}"
  og:image:width: "{{ image.width }}"
  og:image:height: "{{ image.height }}"
  og:url: "{{ page.canonical_url }}"

  twitter:card: summary_large_image
  twitter:title: *page_title_with_site_name
  twitter:description: *page_description
  twitter:image: "{{ image.url }}"
```


请注意，此示例使用了[YAML锚点][YAML anchors]来减少重复。`definitions`属性仅用于定义可以通过锚点引用的别名。


  [YAML anchors]: https://support.atlassian.com/bitbucket-cloud/docs/yaml-anchors/

__您是否觉得遗漏了什么？请[开启讨论][open a discussion]并告诉我们！__



  [additional icons]: ./changing-the-logo-and-icons.md#additional-icons
  [Layer icon]: ../assets/screenshots/social-cards-layer-icon.png
  [Layer icon circles]: ../assets/screenshots/social-cards-layer-icon-circles.png
  [open a discussion]: https://github.com/squidfunk/mkdocs-material/discussions/new
