# Blog 设置

MkDocs Material 使得构建博客变得非常简单，无论是作为您文档的辅助部分还是独立存在。您只需专注于内容创作，而引擎将负责繁重的任务，包括自动生成[归档][archive]和[分类][category]索引、[文章 Slug][post slugs]、可配置的[分页][pagination]等更多功能。


---

__快来看看我们用全新的[内置博客插件][built-in blog plugin]创建的[博客][blog]吧！__



  [archive]: ../plugins/blog.md#archive
  [category]: ../plugins/blog.md#categories
  [post slugs]: ../plugins/blog.md#config.post_url_format
  [pagination]: ../plugins/blog.md#pagination
  [blog]: ../blog/index.md

## 配置

### 内置博客插件

<!-- md:version 9.2.0 -->
<!-- md:plugin -->
<!-- md:flag experimental -->


内置博客插件增加了从包含文章的文件夹中构建博客的支持，这些文章带有日期和其他结构化数据的注释。首先，将以下行添加到 `mkdocs.yml `文件中：



``` yaml
plugins:
  - blog
```

如果您在`mkdocs.yml`中没有定义导航（`nav`），那么您无需进行其他操作，因为博客插件会自动添加导航。如果您已经定义了导航，那么您只需要将博客索引页面添加到其中。您不需要也不应该添加单个的博客文章。例如：

```yaml
nav:
  - index.md
  - Blog:
    - blog/index.md
```

要了解所有设置的列表，请参阅[插件文档][plugin documentation]。

  [plugin documentation]: ../plugins/blog.md

#### 高级设置

<!-- md:sponsors -->
<!-- md:version insiders-4.44.0 -->

以下高级设置目前仅对我们的[赞助者][Insiders]开放。这些设置完全是可选的，不会影响博客的功能，但有助于进行自定义：

- [`archive_pagination`][config.archive_pagination]
- [`archive_pagination_per_page`][config.archive_pagination_per_page]
- [`categories_sort_by`][config.categories_sort_by]
- [`categories_sort_reverse`][config.categories_sort_reverse]
- [`categories_pagination`][config.categories_pagination]
- [`categories_pagination_per_page`][config.categories_pagination_per_page]
- [`authors_profiles_pagination`][config.authors_profiles_pagination]
- [`authors_profiles_pagination_per_page`][config.authors_profiles_pagination_per_page]

随着我们发现新的场景，我们将会在这里添加更多的设置。


  [Insiders]: ../insiders/index.md
  [built-in blog plugin]: ../plugins/blog.md
  [built-in plugins]: ../insiders/getting-started.md#built-in-plugins
  [start writing your first post]: #writing-your-first-post

  [config.archive_pagination]: ../plugins/blog.md#config.archive_pagination
  [config.archive_pagination_per_page]: ../plugins/blog.md#config.archive_pagination_per_page
  [config.categories_sort_by]: ../plugins/blog.md#config.categories_sort_by
  [config.categories_sort_reverse]: ../plugins/blog.md#config.categories_sort_reverse
  [config.categories_pagination]: ../plugins/blog.md#config.categories_pagination
  [config.categories_pagination_per_page]: ../plugins/blog.md#config.categories_pagination_per_page
  [config.authors_profiles_pagination]: ../plugins/blog.md#config.authors_profiles_pagination
  [config.authors_profiles_pagination_per_page]: ../plugins/blog.md#config.authors_profiles_pagination_per_page

### RSS

<!-- md:version 9.2.0 -->
<!-- md:plugin [rss] -->

[内置博客插件][built-in blog plugin]与[RSS插件][rss]无缝集成，后者提供了一种简单的方法，可以将RSS订阅源添加到您的博客（或整个文档）中。您可以使用pip来安装它：

```
pip install mkdocs-rss-plugin
```

然后，在 `mkdocs.yml` 中添加以下内容：

``` yaml
plugins:
  - rss:
      match_path: blog/posts/.* # (1)!
      date_from_meta:
        as_creation: date
      categories:
        - categories
        - tags # (2)!
```

1、RSS插件允许过滤要包含在订阅源中的URL。在这个例子中，只有博客文章会成为订阅源的一部分。

2、如果您想在订阅源中包含文章的类别以及标签，请在这里同时添加categories和tags。

支持以下配置选项：

<!-- md:option rss.enabled -->

:   <!-- md:default `true` --> 此选项用于指定在构建项目时是否启用该插件。如果您想加快本地构建速度，可以使用[环境变量][mkdocs.env]：

    ``` yaml
    plugins:
      - rss:
          enabled: !ENV [CI, false]
    ```

<!-- md:option rss.match_path -->

:   <!-- md:default `.*` -->  此选项用于指定哪些页面应包含在订阅源中。例如，要仅将博客文章包含在订阅源中，请使用以下正则表达式：


    ``` yaml
    plugins:
      - rss:
          match_path: blog/posts/.*
    ```

<!-- md:option rss.date_from_meta -->

:   <!-- md:default none --> 此选项用于指定订阅源中页面的创建日期应使用哪个前置事项（front matter）属性。建议使用`date`属性：


    ``` yaml
    plugins:
      - rss:
          date_from_meta:
            as_creation: date
    ```

<!-- md:option rss.categories -->

:   <!-- md:default none --> 
    此选项用于指定订阅源中哪一类的博客文章（front matter）属性。如果您使用[categories]和[tags]，请按以下行添加它们：

    ``` yaml
    plugins:
      - rss:
          categories:
            - categories
            - tags
    ```

<!-- md:option rss.comments_path -->

:   <!-- md:default none --> 此选项用于指定可以找到帖子或页面评论的锚点。如果您已经集成了[评论系统][comment system]，请添加以下行：


    ``` yaml
    plugins:
      - rss:
          comments_path: "#__comments"
    ```

MkDocs Material 会自动为您的网站添加[必要的元数据][necessary metadata]，这样浏览器和订阅源阅读器就可以发现 RSS 订阅源了。

此扩展的其他配置选项并未得到 MkDocs Material 的官方支持，因此可能会产生意外的结果。请自行承担使用它们的风险。

  [rss]: https://guts.github.io/mkdocs-rss-plugin/
  [categories]: ../plugins/blog.md#categories
  [tags]: setting-up-tags.md#built-in-tags-plugin
  [comment system]: adding-a-comment-system.md
  [necessary metadata]: https://guts.github.io/mkdocs-rss-plugin/configuration/#integration
  [theme extension]: ../customization.md

###  仅 Blog 

您可能需要构建一个不包含任何文档的纯博客。在这种情况下，您可以创建如下的文件夹结构：

``` { .sh .no-copy }
.
├─ docs/
│  ├─ posts/ # (1)!
│  ├─ .authors.yml
│  └─ index.md
└─ mkdocs.yml
```

1、请注意，`posts` 目录位于 `docs` 的根目录下，而不是在一个中间的 `blog` 目录中。


在 `mkdocs.yml` 中天际如下行:

``` yaml
plugins:
  - blog:
      blog_dir: . # (1)!
```

1、请查阅 [plugin documentation] 获取关于 [`blog_dir`][blog_dir] 更详细的设置.

这种配置将会使 `/blog/<post_slug>` URL 变成 `/<post_slug>`


## 文章使用

### 撰写您的第一篇博文

在成功安装[内置博客插件][built-in blog plugin]之后，是时候撰写您的第一篇博文了。该插件不预设任何特定的目录结构，因此您可以完全自由地组织您的博文，只要它们全部位于`posts`目录内即可：

``` { .sh .no-copy }
.
├─ docs/
│  └─ blog/
│     ├─ posts/
│     │  └─ hello-world.md # (1)!
│     └─ index.md
└─ mkdocs.yml
```
    
1、如果您想以不同的方式排列博文，完全可以自行决定。URL是根据[`post_url_format`][post slugs]中指定的格式以及博文的标题和日期构建的，与它们在posts目录内的组织方式无关。

创建一个名为 `hello-world.md` 的新文件，并添加以下行：



``` yaml
---
draft: true # (1)!
date: 2024-01-31 # (2)!
categories:
  - Hello
  - World
---

# Hello world!
...
```


1、如果您将一篇文章标记为[草稿][draft]，那么在索引页面上，文章日期旁边会出现一个红色标记。在构建网站时，草稿不会被包含在输出中。[可以改变这种行为][This behavior can be changed]，例如，在构建部署预览时渲染草稿。

2、如果您希望提供多个日期，可以使用以下语法，这样您就可以定义博客文章的最后更新日期，以及您可以添加到模板中的其他自定义日期：

    ``` yaml
    ---
    date:
      created: 2022-01-31
      updated: 2022-02-02
    ---

    # Hello world!
    ```

    请注意，创建日期必须设置在`date.created`下，因为每篇博客文章都必须设置创建日期。

当您启动[实时预览服务器][live preview server]时，应该会看到您的第一篇博文！您还会发现，[归档][archive]和[类别][category]索引已经为您自动生成了。

  [draft]: ../plugins/blog.md#drafts
  [This behavior can be changed]: ../plugins/blog.md#config.draft
  [live preview server]: ../creating-your-site.md#previewing-as-you-write

#### 添加摘要

博客索引以及[归档][archive]和[类别][category]索引可以列出每篇文章的全部内容，也可以仅列出摘要。您可以在文章的前几段之后添加一个`<!-- more -->`分隔符来创建摘要：

``` py
# Hello world!

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
massa, nec semper lorem quam in massa.

<!-- more -->
...
```

当[内置博客插件][built-in blog plugin]生成所有索引时，会自动提取[摘要分隔符]之前的内容，这样用户就可以在决定是否深入阅读之前先开始阅读文章的摘要部分。

  [excerpt separator]: ../plugins/blog.md#config.post_excerpt_separator

#### 添加作者

为了给您的帖子增添一些个性，您可以将每个帖子与一个或多个[作者][authors]关联起来。首先，在您的博客目录中创建[`.authors.yml`][authors_file]文件，并添加一个作者：

``` yaml
authors:
  squidfunk:
    name: Martin Donath
    description: Creator
    avatar: https://github.com/squidfunk.png
```

[`.authors.yml`][authors_file]文件将每个作者与一个标识符（在此示例中为squidfunk）关联起来，然后可以在帖子中使用该标识符。可以配置不同的属性。要了解所有可能的属性列表，请参阅[`authors_file`][authors_file]文档。

现在，您可以通过在Markdown文件的头部信息（front matter）中的authors属性下引用作者的标识符，将一个或多个作者分配给帖子。对于每个作者，都会在每篇帖子的左侧侧边栏以及索引页面上的帖子摘要中显示一个小型个人资料。

``` yaml
---
date: 2024-01-31
authors:
  - squidfunk
    ...
---

# Hello world!
...
```

  [authors]: ../plugins/blog.md#authors
  [authors_file]: ../plugins/blog.md#config.authors_file

#### 添加作者资料

<!-- md:sponsors -->
<!-- md:version insiders-4.46.0 -->
<!-- md:flag experimental -->

如果您希望为每个作者添加一个专门的页面，您可以通过将[`authors_profiles`][authors_profiles]配置选项设置为`true`来启用作者资料。只需在`mkdocs.yml`文件中添加以下行：

``` yaml
plugins:
  - blog:
      authors_profiles: true
```

如果您将作者资料页面与[自定义索引页面][custom index pages]相结合，那么您就可以为每个作者创建一个包含简短描述、社交媒体链接等内容的专属页面——基本上，您可以在Markdown中编写任何内容。之后，文章列表会被附加到该页面的内容之后。

  [authors_profiles]: ../plugins/blog.md#config.authors_profiles
  [custom index pages]: #custom-index-pages

#### 添加分类

分类是在专用索引页面上按主题对帖子进行分组的一种绝佳方式。这样，对特定主题感兴趣的用户可以浏览您在该主题下的所有帖子。请确保已启用[类别][categories]功能，并将它们添加到Markdown文件头部信息的`categories`属性中：

``` yaml
---
date: 2024-01-31
categories:
  - Hello
  - World
---

# Hello world!
...
```

如果您想在输入类别时避免拼写错误，可以在`mkdocs.yml`文件中定义您想要的类别，作为[`categories_allowed`][categories_allowed]配置选项的一部分。如果某个类别不在此列表中，[内置博客插件][built-in blog plugin]将停止构建过程。

  [categories_allowed]: ../plugins/blog.md#config.categories_allowed

#### 添加 tags 标签

除了[类别][categories]之外，[内置博客插件][built-in blog plugin]还与[内置标签插件][built-in tags plugin]集成。如果您在帖子的头部信息`tags`属性中添加标签，则该帖子将与[标签索引][tags index]链接。

``` yaml
---
date: 2024-01-31
tags:
  - Foo
  - Bar
---

# Hello world!
...
```

通常，标签会显示在主标题之上，并且如果在标签索引页上进行了配置，帖子会与标签索引页链接。请注意，帖子（作为页面）仅通过其标题进行链接。

  [built-in tags plugin]: ../plugins/tags.md
  [tags index]: setting-up-tags.md#adding-a-tags-index

#### 更改 Slug（标识符）

Slug 是您在 URL 中使用的帖子的简短描述。它们通常是自动生成的，但您可以为页面指定一个自定义的 Slug：


``` yaml
---
slug: hello-world
---

# Hello there world!
...
```

#### 添加相关链接

<!-- md:sponsors -->
<!-- md:version insiders-4.23.0 -->
<!-- md:flag experimental -->


相关链接 为您的帖子提供了一个完美的方式，可以在左侧侧边栏中突出显示一个 _进一步阅读_ 部分，引导用户前往您文档的其他部分。要使用相关链接，请在Markdown文件的头部信息（front matter）中使用 `links` 属性来添加它们：

``` yaml
---
date: 2024-01-31
links:
  - plugins/search.md
  - insiders/how-to-sponsor.md
---

# Hello world!
...
```


您可以在`mkdocs.yml`文件中使用与[`nav`][mkdocs.nav]部分完全相同的语法，这意味着您可以为链接设置明确的标题，添加外部链接，甚至使用嵌套结构。

``` yaml
---
date: 2024-01-31
links:
  - plugins/search.md
  - insiders/how-to-sponsor.md
  - Nested section:
    - External link: https://example.com
    - setup/setting-up-site-search.md
---

# Hello world!
...
```


如果您仔细观察，您会发现甚至可以使用锚点来链接到文档的特定部分，从而在`mkdocs.yml`中扩展[`nav`][mkdocs.nav]语法的可能性。内置博客插件会解析锚点，并将锚点的标题设置为相关链接的[副标题][subtitle]。

请注意，与[`nav`][mkdocs.nav]设置一样，所有链接都必须是相对于[`docs_dir`][mkdocs.docs_dir]的。

  [subtitle]: ../reference/index.md#setting-the-page-subtitle

#### 在帖子之间建立链接

虽然[帖子URL（post slugs）][post slugs]是动态计算的，但[内置博客插件][built-in blog plugin]确保所有来自和指向帖子以及帖子资源的链接都是正确的。如果您想链接到一个帖子，只需使用指向Markdown文件的路径作为链接引用（链接必须是相对的）：

``` markdown
[Hello World!](blog/posts/hello-world.md)
```

从博客帖子链接到文档页面（例如索引页面）也遵循相同的方法：


``` markdown
[Blog](../index.md)
```


在构建网站时，`posts`目录中的所有资源都会被复制到`blog/assets`文件夹中。当然，您也可以从`posts`目录外部的帖子中引用资源。[内置博客插件][built-in blog plugin]会确保所有链接都是正确的。

#### 置顶帖子 :material-alert-decagram:{ .mdx-pulse title="Added on February 24, 2024" }

<!-- md:sponsors -->
<!-- md:version insiders-4.53.0 -->
<!-- md:flag experimental -->

如果您希望将某个帖子固定在索引页面的顶部，以及它所属的归档和分类索引中，您可以在Markdown文件的头部信息（front matter）中使用`pin`属性：

``` yaml
---
date: 2024-01-31
pin: true
---

# Hello world!
...
```

如果有多个帖子被置顶，它们将按照创建日期进行排序，最近置顶的帖子将首先显示，其他置顶帖子则按降序排列。

#### 设置阅读时间

当[启用][enabled]时，[readtime]包会被用来计算每篇帖子的预期阅读时间，该时间会显示在帖子和帖子摘要中。如今，许多博客都会显示阅读时间，因此[内置博客插件][built-in blog plugin]也提供了这一功能。

然而，有时计算出的阅读时间可能不够准确，或者得出一些奇怪且不令人愉快的数字。因此，可以通过在帖子的头部信息（front matter）中使用`readtime`属性来覆盖和明确设置阅读时间：

``` yaml
---
date: 2024-01-31
readtime: 15
---

# Hello world!
...
```

这将禁用阅读时间的自动计算功能。

  [readtime]: https://pypi.org/project/readtime/
  [enabled]: ../plugins/blog.md#config.post_readtime

####设置默认值

<!-- md:sponsors -->
<!-- md:version insiders-4.21.0 -->
<!-- md:plugin [meta][built-in meta plugin] – built-in -->
<!-- md:flag experimental -->


如果您有很多帖子，为每个帖子都定义上述所有内容可能会显得冗余。幸运的是，[内置元数据插件][built-in meta plugin]允许您为每个文件夹设置默认的头部信息（front matter）属性。您可以按类别或作者对帖子进行分组，并添加一个`.meta.yml`文件来设置公共属性：

``` { .sh .no-copy }
.
├─ docs/
│  └─ blog/
│     ├─ posts/
│     ├─ .meta.yml # (1)!
│     └─ index.md
└─ mkdocs.yml
```

1、如前所述，您还可以在`posts`目录的嵌套文件夹中放置一个`.meta.yml`文件。该文件可以定义帖子中所有有效的头部信息（front matter）属性，例如：

    ``` yaml
    authors:
      - squidfunk
    categories:
      - Hello
      - World
    ```

请注意，顺序很重要——在`mkdocs.yml`中，[内置元数据插件][built-in meta plugin]必须定义在博客插件之前，这样[内置博客插件][built-in blog plugin]才能正确获取所有设置的默认值：

``` yaml
plugins:
  - meta
  - blog
```

在`.meta.yml`文件中，列表和字典会与帖子中定义的值进行合并和去重，这意味着您可以在`.meta.yml`文件中定义公共属性，然后为每个帖子添加特定属性或进行覆盖。

  [built-in meta plugin]: ../plugins/meta.md

### 添加页面

除了帖子之外，您还可以通过在`mkdocs.yml`文件的[`nav`][mkdocs.nav]部分列出页面来向您的博客添加静态页面。所有生成的索引都会显示在最后指定的页面之后。例如，要向博客添加一个关于作者的页面，请在`mkdocs.yml`中添加以下内容：

``` yaml
nav:
  - Blog:
    - blog/index.md
    - blog/authors.md
      ...
```

## 自定义

### 自定义索引页面

<!-- md:sponsors -->
<!-- md:version insiders-4.24.0 -->
<!-- md:flag experimental -->

如果你想在自动生成的[归档][archive]和[分类][category]索引中添加自定义内容，例如，在帖子列表之前添加分类描述，你可以手动在与[内置博客插件][built-in blog plugin]会创建分类页面的相同位置创建该页面：

``` { .sh .no-copy }
.
├─ docs/
│  └─ blog/
│     ├─ category/
│     │  └─ hello.md # (1)!
│     ├─ posts/
│     └─ index.md
└─ mkdocs.yml
```

1、 最简单的方法是首先向博客文章[添加分类][add the category]，然后获取[内置博客插件][built-in blog plugin]生成的URL，并在[`blog_dir`][blog_dir]文件夹的相应位置创建文件。

    请注意，所显示的目录列表是基于默认配置的。如果您为以下选项指定了不同的值，请确保相应地调整路径：

    - [`blog_dir`][blog_dir]
    - [`categories_url_format`][categories_url_format]
    - [`categories_slugify`][categories_slugify]

现在，您可以向新创建的文件中添加任意内容，或者为此页面设置特定的前言（front matter）属性，例如更改[页面描述][page description]：

``` yaml
---
description: Nullam urna elit, malesuada eget finibus ut, ac tortor.
---

# Hello
...
```

属于该分类的所有文章摘要都会自动追加。


  [add the category]: #adding-categories
  [page description]: ../reference/index.md#setting-the-page-description
  [categories_url_format]: ../plugins/blog.md#config.categories_url_format
  [categories_slugify]: ../plugins/blog.md#config.categories_slugify
  [blog_dir]: ../plugins/blog.md#config.blog_dir

### 复写模板

[内置博客插件][built-in blog plugin]与MkDocs的Material主题建立在相同的基础上，这意味着您可以通过像往常一样使用[主题扩展][theme extension]来覆盖博客使用的所有模板。

[内置博客插件][built-in blog plugin]添加了以下模板：


- [`blog.html`][blog.html] – Template for blog, archive and category index
- [`blog-post.html`][blog-post.html] – Template for blog post

  [theme extension]: ../customization.md#extending-the-theme

  [blog.html]: https://github.com/squidfunk/mkdocs-material/blob/master/src/templates/blog.html
  [blog-post.html]: https://github.com/squidfunk/mkdocs-material/blob/master/src/templates/blog-post.html

