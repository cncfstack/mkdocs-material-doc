# 导航设置

清晰简洁的导航结构是优质项目文档的一个重要方面。Material-for-MkDocs 提供了多种选项来配置导航元素的行为，包括[tabs]和[sections]，以及其旗舰功能之一：[即时加载][instant loading]。

  [tabs]: #navigation-tabs
  [sections]: #navigation-sections
  [instant loading]: #instant-loading

额外的导航可以在[页脚][in the footer]  中配置，也可以使用[标签插件][tags plugin]进行配置。此外，[博客插件][blog plugin]也设置了额外的导航。

[in the footer]: setting-up-the-footer.md#navigation
[tags plugin]: ../plugins/tags.md
[blog plugin]: ../plugins/blog.md

## 配置

### 即时加载

<!-- md:version 5.0.0 -->
<!-- md:feature -->

当启用即时加载时，所有内部链接的点击都将被拦截，并通过 [XHR] 发送，而无需完全重新加载页面。在 `mkdocs.yml `文件中添加以下行：

``` yaml
theme:
  features:
    - navigation.instant
```

解析并注入生成的页面，同时自动重新绑定所有事件处理程序和组件，即 __Material-for-MkDocs 表现得像一个单页应用程序__ 。现在，搜索索引在导航过程中保持不变，这对于大型文档站点尤其有用。

!!! info "其中 [`site_url`][mkdocs.site_url] 必须设置"

    请注意，在使用即时导航时，您必须设置 [`site_url`][mkdocs.site_url]，因为即时导航依赖于生成的 `sitemap.xml`，如果省略此设置，该文件将为空。示例：

    ``` yaml
    site_url: https://example.com
    ```

  [XHR]: https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest

#### 即时预取(Instant prefetching)

<!-- md:sponsors -->
<!-- md:version insiders-4.36.0 -->
<!-- md:feature -->
<!-- md:flag experimental -->

即时预取是一项新的实验性功能，当用户将鼠标悬停在链接上时，该功能将开始获取页面。这将减少用户感知的加载时间，尤其是在网络连接较慢的情况下，因为页面将在导航后立即可用。可以通过以下方式启用它：

``` yaml
theme:
  features:
    - navigation.instant
    - navigation.instant.prefetch
```

#### 进度指示器

<!-- md:version 9.4.3 -->
<!-- md:feature -->
<!-- md:flag experimental -->

为了在使用即时导航时在网络连接较慢的情况下提供更好的用户体验，可以启用进度指示器。它将在页面顶部显示，并在页面完全加载后隐藏。您可以在 `mkdocs.yml` 文件中通过以下方式启用它：

``` yaml
theme:
  features:
    - navigation.instant
    - navigation.instant.progress
```

如果页面在400毫秒后仍未加载完成，进度指示器才会显示，这样在网络连接较快的情况下，为了提供更好的即时体验，它永远不会出现。


### 即时预览(Instant previews) :material-alert-decagram:{ .mdx-pulse title="Added on January 28, 2024" }

<!-- md:sponsors -->
<!-- md:version insiders-4.52.0 -->
<!-- md:feature -->
<!-- md:flag experimental -->

即时预览是一项全新功能，它允许用户在不导航离开当前页面的情况下预览您文档中的另一个站点。它们对于保持用户的上下文环境非常有帮助。可以通过带有 `data-preview` 属性的任意页眉链接启用即时预览：


````markdown title="Link with instant preview"
```markdown
[Attribute Lists](#){ data-preview }
```
````

例如：

<div class="result" markdown>

[Attribute Lists](extensions/python-markdown.md#attribute-lists){ data-preview }

</div>

!!! info "限制说明"

    即时预览仍然是一个实验性功能，目前仅限于标题链接。这意味着，您可以在任何指向另一页面上标题的内部链接上使用它们，但不能在具有 id 属性的其他元素上使用。在收集到足够的反馈后，我们将考虑将此功能扩展到其他元素，甚至可能是任意元素。



#### 自动预览

<!-- md:sponsors -->
<!-- md:version insiders-4.53.0 -->
<!-- md:extension -->
<!-- md:flag experimental -->

推荐使用即时预览功能的方式是利用 Material-for-MkDocs 题中自带的 Markdown 扩展，因为它允许您为文档的每一页或每一节启用即时预览功能：

``` yaml
markdown_extensions:
  - material.extensions.preview:
      targets:
        include:
          - changelog/index.md
          - customization.md
          - insiders/changelog/*
          - setup/extensions/*
```

上述配置是我们为文档所使用的。我们已为变更日志、自定义指南和 Insider 部分，以及我们支持的所有 Markdown 扩展启用了即时预览功能。

!!! info "完整配置示例"

    ``` yaml
    markdown_extensions:
      - material.extensions.preview:
          sources: # (1)!
            include:
              - ...
            exclude:
              - ...
          targets: # (2)!
            include:
              - ...
            exclude:
              - ...
    ```

    1、`Sources` 指定应启用即时预览的页面。如果省略此设置，则所有页面都将启用即时预览。您可以使用正则表达式来包含或排除页面。排除是在包含的基础上进行的评估，因此，如果某个页面同时匹配包含和排除模式，则该页面将被排除。

    2、`Targets` 指定应启用即时预览的目标页面。这是启用即时预览的推荐方式。


通过在 `mkdocs.yml` 中添加以下行，也可以全局启用即时预览，这将为所有标题链接启用即时预览，从而无需添加数据属性：



``` yaml
theme:
  features:
    - navigation.instant.preview
```

!!! info "其中 [`site_url`][mkdocs.site_url] 必须设置"

    请注意，在使用即时预览时，您必须设置[`site_url`][mkdocs.site_url]，因为即时预览依赖于生成的 `sitemap.xml`，如果省略此设置，则 `sitemap.xml` 将为空。示例：

    ``` yaml
    site_url: https://example.com
    ```

### 锚点追踪

<!-- md:version 8.0.0 -->
<!-- md:feature -->

当启用锚点追踪时，地址栏中的 URL 会自动更新为当前在目录中高亮显示的活跃锚点的 URL。在`mkdocs.yml`中添加以下行：

``` yaml
theme:
  features:
    - navigation.tracking
```

### 导航标签页

<!-- md:version 1.1.0 -->
<!-- md:feature -->

当启用标签页时，对于视口宽度大于`1220px`的设备，顶级部分(top-level sections)将在标题下方的菜单层中呈现，但在移动设备上则保持不变。[^1] 在`mkdocs.yml`中添加以下行：

  [^1]:
    在 <!-- md:version 6.2.0 --> 版本之前，导航标签页的行为略有不同。在 `mkdocs.yml `的nav条目中定义的所有顶级(top-level)页面（即所有直接引用 `*.md` 文件的顶级条目）都被分组在第一个标签页下，该标签页采用第一个页面的标题。这导致无法将顶级页面（或外部链接）作为标签页项包含在内，如 #1884 和 #2072 中所报告的那样。从 <!-- md:version 6.2.0 --> 版本开始，导航标签页包括所有顶级页面和部分。


``` yaml
theme:
  features:
    - navigation.tabs
```

=== "With tabs"

    [![Navigation tabs enabled]][Navigation tabs enabled]

=== "Without"

    [![Navigation tabs disabled]][Navigation tabs disabled]

  [Navigation tabs enabled]: ../../assets/screenshots/navigation-tabs.png
  [Navigation tabs disabled]: ../../assets/screenshots/navigation.png

#### 粘性导航标签页

<!-- md:version 7.3.0 -->
<!-- md:feature -->

当启用粘性标签页时，导航标签页将锁定在标题下方，并在向下滚动时始终可见。只需在 `mkdocs.yml` 中添加以下两个功能标志：

``` yaml
theme:
  features:
    - navigation.tabs
    - navigation.tabs.sticky
```

=== "有粘性导航"

    [![Sticky navigation tabs enabled]][Sticky navigation tabs enabled]

=== "没有"

    [![Sticky navigation tabs disabled]][Sticky navigation tabs disabled]

  [Sticky navigation tabs enabled]: ../../assets/screenshots/navigation-tabs-sticky.png
  [Sticky navigation tabs disabled]: ../../assets/screenshots/navigation-tabs-collapsed.png

### 章节导航

<!-- md:version 6.2.0 -->
<!-- md:feature -->

当启用章节导航（sections）功能时，对于分辨率宽度大于 `1220px` 的设备，顶级部分将在侧边栏中以组的形式呈现，但在移动设备上则保持不变。在 `mkdocs.yml` 中添加以下行：



``` yaml
theme:
  features:
    - navigation.sections
```

=== "With sections"

    [![Navigation sections enabled]][Navigation sections enabled]

=== "Without"

    [![Navigation sections disabled]][Navigation sections disabled]

  [Navigation sections enabled]: ../../assets/screenshots/navigation-sections.png
  [Navigation sections disabled]: ../../assets/screenshots/navigation.png

这两个功能标志，即 [`navigation.tabs`][tabs] 和 [`navigation.sections`][sections] ，可以相互结合使用。如果同时启用这两个功能标志，则二级导航项将以部分（sections）的形式呈现。

### 导航展开

<!-- md:version 6.2.0 -->
<!-- md:feature -->

当启用展开功能时，左侧侧边栏将默认展开所有可折叠的子部分，因此用户无需手动打开子部分。在 `mkdocs.yml` 中添加以下行：


``` yaml
theme:
  features:
    - navigation.expand
```

=== "With expansion"

    [![Navigation expansion enabled]][Navigation expansion enabled]

=== "Without"

    [![Navigation expansion disabled]][Navigation expansion disabled]

  [Navigation expansion enabled]: ../../assets/screenshots/navigation-expand.png
  [Navigation expansion disabled]: ../../assets/screenshots/navigation.png

### 导航路径 <small>面包屑导航</small> { id=navigation-path }

<!-- md:sponsors -->
<!-- md:version insiders-4.28.0 -->
<!-- md:feature -->
<!-- md:flag experimental -->

当导航路径被激活时，每个页面标题上方都会显示一个面包屑导航，这可能会让在小屏幕设备上访问你的文档的用户更容易找到方向。在 `mkdocs.yml` 中添加以下行：


``` yaml
theme:
  features:
    - navigation.path
```

=== "With navigation path"

    [![Navigation path enabled]][Navigation path enabled]

=== "Without"

    [![Navigation path disabled]][Navigation path disabled]

  [Navigation path enabled]: ../../assets/screenshots/navigation-path-on.png
  [Navigation path disabled]: ../../assets/screenshots/navigation-path-off.png

### 导航修剪

<!-- md:version 9.2.0 -->
<!-- md:feature -->
<!-- md:flag experimental -->

当启用修剪功能时，只有可见的导航项会被包含在生成的HTML中，__从而使构建后的网站大小减少33%或更多__。在 `mkdocs.yml` 中添加以下行：

``` yaml
theme:
  features:
    - navigation.prune # (1)!
```

1、此功能标志与 [`navigation.expand`][navigation.expand] 不兼容，因为导航展开需要完整的导航结构。

此功能标志对于拥有100页甚至1000页以上的文档网站特别有用，因为导航在HTML中占据了很大一部分。导航修剪会将所有可扩展的部分替换为指向该部分中第一页（或部分索引页）的链接。

  [navigation.expand]: #navigation-expansion

### 章节索引页

<!-- md:version 7.3.0 -->
<!-- md:feature -->

当启用章节索引页时，文档可以直接附加到 __章节标题__ 中，这对于提供概览页特别有用。在 `mkdocs.yml` 中添加以下行：

``` yaml
theme:
  features:
    - navigation.indexes # (1)!
```

1、此功能标志与 [`toc.integrate`][toc.integrate] 不兼容，因为部分由于空间不足而无法容纳目录。

=== "With section index pages"

    [![Section index pages enabled]][Section index pages enabled]

=== "Without"

    [![Section index pages disabled]][Section index pages disabled]


要将一个页面链接到一个章节，请在相应的文件夹中创建一个名为 `index.md` 的新文档，并将其添加到您的导航部分的开头：


``` yaml
nav:
  - Section:
    - section/index.md # (1)!
    - Page 1: section/page-1.md
    ...
    - Page n: section/page-n.md
```

1、MkDocs 还将名为 `README.md` 的文件视为[index pages]。

  [Section index pages enabled]: ../../assets/screenshots/navigation-index-on.png
  [Section index pages disabled]: ../../assets/screenshots/navigation-index-off.png
  [toc.integrate]: #navigation-integration
  [index pages]: https://www.mkdocs.org/user-guide/writing-your-docs/#index-pages

### 目录（TOC）

#### 锚点跟随

<!-- md:version 8.5.0 -->
<!-- md:feature -->
<!-- md:flag experimental -->

当启用 [table of contents] 的锚点跟随功能时，侧边栏会自动滚动，以确保当前活动的锚点始终可见。在 `mkdocs.yml` 中添加以下行：

``` yaml
theme:
  features:
    - toc.follow
```

#### 导航集成

<!-- md:version 6.2.0 -->
<!-- md:feature -->

当为 [table of contents]  启用导航集成时，它总是作为左侧导航侧边栏的一部分进行显示。请在 `mkdocs.yml` 文件中添加以下行：

注意：启用导航集成后，页面中右上角的页面导航就不会显示了。

``` yaml
theme:
  features:
    - toc.integrate # (1)!
```

1、此功能标志与 [`navigation.indexes`][navigation.indexes] 不兼容，因为缺少空间，各节无法容纳目录。

=== "With navigation integration"

    [![Navigation integration enabled]][Navigation integration enabled]

=== "Without"

    [![Navigation integration disabled]][Navigation integration disabled]

  [table of contents]: extensions/python-markdown.md#table-of-contents
  [Navigation integration enabled]: ../../assets/screenshots/toc-integrate.png
  [Navigation integration disabled]: ../../assets/screenshots/navigation-tabs.png
  [navigation.indexes]: #section-index-pages

### 回到页面顶部按钮

<!-- md:version 7.1.0 -->
<!-- md:feature -->

当用户向下滚动后又开始向上滚动时，可以显示一个返回顶部的按钮。该按钮在页眉正下方且居中对齐。请在 `mkdocs.yml` 中添加以下行：

``` yaml
theme:
  features:
    - navigation.top
```

## 使用（文章中）

### 隐藏侧边栏

<!-- md:version 6.2.0 -->
<!-- md:flag metadata -->

可以通过在文档的头部（front matter）属性中添加 `hide` 属性来隐藏导航目录侧边栏。在 Markdown 文件的顶部添加以下行：

``` yaml
---
hide:
  - navigation
  - toc
---

# Page title
...
```

=== "Hide navigation"

    [![Hide navigation enabled]][Hide navigation enabled]

=== "Hide table of contents"

    [![Hide table of contents enabled]][Hide table of contents enabled]

=== "Hide both"

    [![Hide both enabled]][Hide both enabled]

  [Hide navigation enabled]: ../../assets/screenshots/hide-navigation.png
  [Hide table of contents enabled]: ../../assets/screenshots/hide-toc.png
  [Hide both enabled]: ../../assets/screenshots/hide-navigation-toc.png

### 隐藏导航路径

<!-- md:sponsors -->
<!-- md:version insiders-4.28.0 -->
<!-- md:flag metadata -->

虽然[navigation path]是在主标题上方呈现的，但有时可能希望针对特定页面隐藏它，这可以通过在头部（front matter）属性中添加 `hide` 属性来实现：

``` yaml
---
hide:
  - path
---

# Page title
...
```

  [navigation path]: #navigation-path

## 自定义

### 键盘快捷键

Material-for-MkDocs 包含多个键盘快捷键，使您可以通过键盘浏览项目文档。有两种模式：


<!-- md:option mode:search -->

:  当 _搜索框处于聚焦状态时_，此模式处于激活状态。它提供了多个键盘绑定，使搜索功能可以通过键盘进行访问和导航：

    * ++arrow-down++ , ++arrow-up++ : select next / previous result
    * ++esc++ , ++tab++ : close search dialog
    * ++enter++ : follow selected result

<!-- md:option mode:global -->

:  当 _搜索框未处于聚焦状态_ 且没有其他可接受键盘输入的聚焦元素时，此模式处于激活状态。已绑定以下按键：

    * ++f++ , ++s++ , ++slash++ : open search dialog
    * ++p++ , ++comma++ : go to previous page
    * ++n++ , ++period++ : go to next page

假设您想将某些操作绑定到 ++x++ 键。通过使用[additional JavaScrip]，您可以订阅 `keyboard$` 可观察对象并附加您的自定义事件监听器：


=== ":octicons-file-code-16: `docs/javascripts/shortcuts.js`"

    ``` js
    keyboard$.subscribe(function(key) {
      if (key.mode === "global" && key.type === "x") {
        /* Add custom keyboard handler here */
        key.claim() // (1)!
      }
    })
    ```

    1、调用 `key.claim()` 将对底层事件执行 `preventDefault()`，因此按键操作不会进一步传播并影响其他事件监听器。


=== ":octicons-file-code-16: `mkdocs.yml`"

    ``` yaml
    extra_javascript:
      - javascripts/shortcuts.js
    ```

  [additional JavaScript]: ../customization.md#additional-javascript

### 内容区域宽度

内容区域的宽度被设置为每行的长度不超过80-100个字符，具体取决于字符的宽度。虽然这是一个合理的默认值，因为较长的行往往更难阅读，但有时可能希望增加内容区域的整体宽度，甚至使其扩展到整个可用空间。

这可以通过一个[additional style sheet]和几行CSS轻松实现：

=== ":octicons-file-code-16: `docs/stylesheets/extra.css`"

    ``` css
    .md-grid {
      max-width: 1440px; /* (1)! */
    }
    ```

    1.  如果您希望内容区域始终扩展到可用的屏幕空间，请使用以下CSS重置`max-width`：


        ``` css
        .md-grid {
          max-width: initial;
        }
        ```

=== ":octicons-file-code-16: `mkdocs.yml`"

    ``` yaml
    extra_css:
      - stylesheets/extra.css
    ```

  [additional style sheet]: ../customization.md#additional-css