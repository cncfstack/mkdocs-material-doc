# 站点统计分析设置

与网络上提供的任何其他服务一样，了解您的项目文档的实际使用情况可能是成功的重要因素。Material-for-MkDocs 原生集成了 [Google Analytics][Google Analytics]，并提供了可定制的 [cookie 同意][cookie consent] 功能和 [反馈小部件][feedback widget]。

  [Google Analytics]: https://developers.google.com/analytics
  [cookie consent]: ensuring-data-privacy.md#cookie-consent
  [feedback widget]: #was-this-page-helpful

## 配置

### Google 分析

<!-- md:version 7.1.8 -->
<!-- md:default none -->

Material-for-MkDocs 原生集成了 Google Analytics 4[^1]。如果您已经设置了 Google Analytics 并拥有了一个属性，可以通过在 `mkdocs.yml` 中添加以下行来启用它：


  [^1]:
    在 Material-for-MkDocs 9.2.0 之前，也支持 Universal Analytics（通用分析）。然而，由于 Universal Analytics 已被淘汰，因此在 9.2.0 版本中移除了对该集成的支持。


``` yaml
extra:
  analytics:
    provider: google
    property: G-XXXXXXXXXX
```

??? 疑问 "如何衡量网站搜索使用情况？"

    除了页面浏览量和事件之外，还可以跟踪[网站搜索][site search] 以更好地了解人们如何使用您的文档以及他们期望找到什么。为了启用网站搜索跟踪，需要执行以下步骤：

    1. 转到您的 Google Analytics（谷歌分析） __admin settings__
    2. 选择与跟踪代码相对应的属性
    3. 选择 __data streams__ 选项卡，并点击相应的 URL
    4. 在 __enhanced measurement__ 部分点击齿轮图标
    5. 确保已启用 __site search__

  [site search]: setting-up-site-search.md

### 这个页面对您有帮助吗？

<!-- md:version 8.4.0 -->
<!-- md:default none -->


可以在每一页的底部添加一个简单的[反馈小部件][feedback widget]，鼓励用户立即提供反馈，说明页面是否有帮助。将以下行添加到 `mkdocs.yml` 中：


``` yaml
extra:
  analytics: # (1)!
    feedback:
      title: Was this page helpful?
      ratings:
        - icon: material/emoticon-happy-outline
          name: This page was helpful
          data: 1
          note: >-
            Thanks for your feedback!
        - icon: material/emoticon-sad-outline
          name: This page could be improved
          data: 0
          note: >- # (2)!
            Thanks for your feedback! Help us improve this page by
            using our <a href="..." target="_blank" rel="noopener">feedback form</a>.
```

1、此功能原生集成了[Google Analytics][analytics]，因此也需要提供 `provider`（供应商）和 `property`（属性）。但是，也可以提供[自定义反馈集成][custom feedback integration]。

2、用户提交反馈后，您可以在显示的备注中添加任意HTML标签，例如链接到反馈表单。

`title`和`ratings`这两个属性都是必需的。请注意，允许定义超过两种评分，例如实现 1-5 星评分。由于反馈小部件将数据发送到第三方服务，因此它当然原生集成了[cookie同意] [cookie consent]功能[^2]。

  [^2]:
    如果用户不接受 `analytics`（分析）cookie，则不会显示反馈小部件。

??? 疑问 "如何可视化收集到的反馈评分？"

    为了可视化反馈评分，您需要使用[Google Analytics]（谷歌分析）创建一个自定义报告，该报告将快速显示您项目文档中获得最低评分和最高评分的页面。


    1、 前往您的 Google Analytics（谷歌分析）仪表板 __dashboard__

    2、 在左侧菜单（底部）转到管理页面 __Admin__ ，然后在数据显示卡片 __Data display__ 上选择自定义定义 __custom definitions__

    3、 点击自定义指标选项卡 __custom metrics__，然后点击创建自定义指标 __create custom metrics__，输入以下值：

        * Metric name（指标名称）：页面是否有帮助
        * Description（描述）：这个页面有帮助吗？
        * Event parameter（事件参数）：data
        * Unit of measurement（测量单位）：标准

    4、在左侧菜单转到探索 __explore__ 页面，创建一个新的空白探索 __blank exploration__

    5、 按以下方式配置报告：

        * Dimensions（维度）：添加事件名称和页面位置
        * Metrics（指标）：添加事件次数和页面是否有帮助（在步骤3中创建的自定义指标）
        * Rows（行）：页面位置
        * Values（值）：拖入事件次数和页面是否有帮助
        * Filters（过滤器）：为事件名称添加一个新过滤器，`Event name / exactly matches / feedback`


    !!! warning "数据可用性延迟"

        报告可能需要24小时或更长时间才能开始显示数据

    现在，在您保存报告并收集了一些反馈评分后，您将获得一个包含所有页面及其总评分数和每页平均评分的列表。这应该能帮助您识别出需要改进的页面：



    !!! danger "Google Analytics 4 不支持平均值"

        据我们所知，Google Analytics 4 目前没有允许定义自定义计算指标来计算页面平均评分的功能。参见#5740。

    [![feedback report]][feedback report]

每个评分具有以下属性：


<!-- md:option analytics.feedback.ratings.icon -->

:   <!-- md:default none --> <!-- md:flag required -->
    此属性必须指向引用[主题中包含的图标][custom icons]的有效图标路径，否则构建将不会成功。一些常见的组合包括：

    * :material-emoticon-happy-outline: + :material-emoticon-sad-outline: – `material/emoticon-happy-outline` + `material/emoticon-sad-outline`
    * :material-thumb-up-outline: + :material-thumb-down-outline: – `material/thumb-up-outline` + `material/thumb-down-outline`
    * :material-heart: + :material-heart-broken: – `material/heart` + `material/heart-broken`

<!-- md:option analytics.feedback.ratings.name -->

:   <!-- md:default none --> <!-- md:flag required -->
    此属性的值在用户交互时（即键盘聚焦或鼠标悬停）显示，解释图标背后评分的含义。

<!-- md:option analytics.feedback.ratings.data -->

:   <!-- md:default none --> <!-- md:flag required -->
    此属性的值将作为自定义事件的数据值发送到Google Analytics[^3]（或任何自定义集成）。

  [^3]:
    请注意，对于Google Analytics，数据值必须为整数。

<!-- md:option analytics.feedback.ratings.note -->

:   <!-- md:default none --> <!-- md:flag required -->
    此属性的值在用户选择评分后显示。它可以包含任意HTML标签，这对于通过表单请求用户提供对当前页面的更详细反馈特别有用。还可以使用以下占位符预先填写表单，其中包括当前页面的URL和标题：

    - `{url}` – Page URL
    - `{title}` – Page title

    ```
    https://github.com/.../issues/new/?title=[Feedback]+{title}+-+{url}
    ```

    在此示例中，当用户点击链接时，他们将被重定向到您的存储库的“新建问题”表单，并且标题已预先填写，包括当前文档的路径，例如：


    ```
    [Feedback] Setting up site analytics – /setup/setting-up-site-analytics/
    ```

    GitHub Issues 的替代方案是[Google Forms]（谷歌表单）。

  [feedback widget]: #feedback
  [analytics]: #google-analytics
  [feedback report]: ../assets/screenshots/feedback-report.png
  [custom feedback integration]: #custom-site-feedback
  [custom icons]: https://github.com/squidfunk/mkdocs-material/tree/master/material/templates/.icons
  [Google Forms]: https://www.google.com/forms/about/

## 文章内使用

### 隐藏反馈小部件

可以通过在文档的前言（front matter）中添加 `hide` 属性来隐藏[反馈小部件][feedback widget]。在 Markdown 文件的顶部添加以下几行：


``` yaml
---
hide:
  - feedback
---

# Page title
...
```

## 自定义

### 自定义站点分析

要集成另一个提供基于JavaScript的跟踪解决方案的分析服务提供商，只需遵循[主题扩展][theme extension]指南，并在 `overrides` 文件夹中创建一个新的局部文件。局部文件的名称用于通过 `mkdocs.yml` 配置自定义集成：

=== ":octicons-file-code-16: `overrides/partials/integrations/analytics/custom.html`"

    ``` html
    <script>
      /* Add custom analytics integration here, e.g. */
      var property = "{{ config.extra.analytics.property }}" // (1)!

      /* Wait for page to load and application to mount */
      document.addEventListener("DOMContentLoaded", function() {
        location$.subscribe(function(url) {
          /* Add custom page event tracking here */ // (2)!
        })
      })
    </script>
    ```

    1、例如，此变量 `mkdocs.yml` 中设置的值，对于 `property` 属性，其值为 `"foobar"` 。

    2、如果您正在使用[即时加载][instant loading]功能，则可以使用 `location$` 可观察对象来监听导航事件，该对象始终会发出当前的URL。

=== ":octicons-file-code-16: `mkdocs.yml`"

    ``` yaml
    extra:
      analytics:
        provider: custom
        property: foobar # (1)!
    ```

    1、您可以添加任意的键值对来配置自定义集成。如果您在多个存储库之间共享自定义集成，这将特别有用。


  [theme extension]: ../customization.md#extending-the-theme
  [instant loading]: setting-up-navigation.md#instant-loading

### 自定义站点反馈

自定义反馈小部件集成只需要处理一些由用户与反馈小部件交互生成的事件，这可以借助一些[额外的JavaScript][additional JavaScript]来实现：

=== ":octicons-file-code-16: `docs/javascripts/feedback.js`"

    ``` js
    var feedback = document.forms.feedback
    feedback.hidden = false // (1)!

    feedback.addEventListener("submit", function(ev) {
      ev.preventDefault()

      var page = document.location.pathname // (2)!
      var data = ev.submitter.getAttribute("data-md-value")

      console.log(page, data) // (3)!

      feedback.firstElementChild.disabled = true // (4)!

      var note = feedback.querySelector(
        ".md-feedback__note [data-md-value='" + data + "']"
      )
      if (note)
        note.hidden = false // (5)!
    })
    ```

    1、 默认情况下，反馈小部件是隐藏的，因此当用户禁用JavaScript时它不会显示。所以，需要在这里将其启用。

    2、 获取页面和反馈值。

    3、 用将数据发送到您的分析提供商的代码替换此部分。

    4、 提交后禁用表单。

    5、 显示配置的备注。显示哪一个取决于用户的反馈。

=== ":octicons-file-code-16: `mkdocs.yml`"

    ``` yaml
    extra_javascript:
      - javascripts/feedback.js
    ```

&nbsp;
{ #feedback style="margin: 0; height: 0" }

  [additional JavaScript]: ../customization.md#additional-javascript

## 建议

国内无法使用 Google 分析，需要寻找一个类似的服务，或者自己开发

