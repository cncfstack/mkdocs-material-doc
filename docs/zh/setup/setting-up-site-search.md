---
search:
  boost: 1.05
---

# 设置站点搜索

Material-for-MkDocs 提供了一个出色的客户端搜索实现，无需集成第三方服务(可能不符合隐私规定)。此外，搜索功能甚至可以在[offline]状态下工作，允许用户下载您的文档。

  [offline]: building-for-offline-usage.md

## 配置

### 内置搜索插件

<!-- md:version 0.1.0 -->
<!-- md:plugin -->

内置搜索插件与 Material-for-MkDocs 无缝集成，通过 [lunr] 和 [lunr-languages] 添加了多语言客户端搜索功能。它默认启用，但当使用其他插件时，必须重新将其添加到 `mkdocs.yml` 中：

``` yaml
plugins:
  - search
```

有关所有设置的列表，请参阅[plugin documentation]。

  [plugin documentation]: ../plugins/search.md

  [lunr]: https://lunrjs.com
  [lunr-languages]: https://github.com/MihaiValentin/lunr-languages

### 搜索建议

<!-- md:version 7.2.0 -->
<!-- md:feature -->
<!-- md:flag experimental -->

当搜索建议启用时，搜索将显示最后一个单词最可能的补全，您可以使用 ++arrow-right++ 键接受该补全。将以下行添加到 `mkdocs.yml`：

``` yaml
theme:
  features:
    - search.suggest
```

搜索 [:octicons-search-24: search su][Search suggestions example] 会产生 ^^search suggestions^^ 作为一个建议。

  [Search suggestions example]: ?q=search+su

### 搜索高亮

<!-- md:version 7.2.0 -->
<!-- md:feature -->
<!-- md:flag experimental -->

当搜索高亮功能启用并且用户点击搜索结果时，Material-for-MkDocs 会在跳转到链接后高亮显示所有匹配项。将以下行添加到 `mkdocs.yml`：


``` yaml
theme:
  features:
    - search.highlight
```

搜索 [:octicons-search-24: code blocks][Search highlighting example] 会高亮显示这两个术语的所有匹配项。

  [Search highlighting example]: ../reference/code-blocks.md?h=code+blocks

### 搜索分享

<!-- md:version 7.2.0 -->
<!-- md:feature -->

当搜索分享功能被激活时，一个 :material-share-variant:（共享）按钮会显示在重置按钮旁边，允许通过深度链接直接访问当前的搜索查询和结果。将以下行添加到`mkdocs.yml`：


``` yaml
theme:
  features:
    - search.share
```

当用户点击共享按钮时，URL 会自动复制到剪贴板。


## 文章使用

### 搜索加权


<!-- md:version 8.3.0 -->
<!-- md:flag metadata -->

可以通过在页面前置事项（front matter）中添加 `search.boost` 属性来对搜索中的页面进行加权，这将使它们在搜索结果中的排名更高。在 Markdown 文件的顶部添加以下行：


=== ":material-arrow-up-circle: Rank up"

    ``` yaml
    ---
    search:
      boost: 2 # (1)!
    ---

    # Page title
    ...
    ```

    1.  :woman_in_lotus_position: When boosting pages, be gentle and start with
        __low values__.

=== ":material-arrow-down-circle: Rank down"

    ``` yaml
    ---
    search:
      boost: 0.5
    ---

    # Page title
    ...
    ```

### 搜索排除

<!-- md:version 9.0.0 -->
<!-- md:flag metadata -->
<!-- md:flag experimental -->

可以通过在页面前置事项（front matter）中添加 `search.exclude` 属性来将页面从搜索中排除，从而将其从索引中移除。在 Markdown 文件的顶部添加以下行：


``` yaml
---
search:
  exclude: true
---

# Page title
...
```

#### 排除部分章节

当启用 [属性列表][Attribute Lists] 时，可以通过在 Markdown 标题后添加 `data-search-exclude` 指令来将页面的特定部分从搜索中排除：

=== ":octicons-file-code-16: `docs/page.md`"

    ``` markdown
    # Page title

    ## Section 1

    The content of this section is included

    ## Section 2 { data-search-exclude }

    The content of this section is excluded
    ```

=== ":octicons-codescan-16: `search_index.json`"

    ``` json
    {
      ...
      "docs": [
        {
          "location":"page/",
          "text":"",
          "title":"Document title"
        },
        {
          "location":"page/#section-1",
          "text":"<p>The content of this section is included</p>",
          "title":"Section 1"
        }
      ]
    }
    ```

  [Attribute Lists]: extensions/python-markdown.md#attribute-lists

#### 排除区块

当启用[属性列表][Attribute Lists]时，可以通过在 Markdown 内联元素或区块级元素后添加 `data-search-exclude` 指令来将页面的特定部分从搜索中排除：



=== ":octicons-file-code-16: `docs/page.md`"

    ``` markdown
    # Page title

    The content of this block is included

    The content of this block is excluded
    { data-search-exclude }
    ```

=== ":octicons-codescan-16: `search_index.json`"

    ``` json
    {
      ...
      "docs": [
        {
          "location":"page/",
          "text":"<p>The content of this block is included</p>",
          "title":"Document title"
        }
      ]
    }
    ```

## 建议

- 目前搜索框模块启动，关闭设置不生效

