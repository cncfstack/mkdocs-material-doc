# Footer 设置

文档的页面底部(Footer或页脚)是很好的位置来添加网站链接、或者其他的营销渠道，如 :fontawesome-brands-mastodon:{ style="color: #5A4CE0" } 或 :fontawesome-brands-youtube:{ style="color: #EE0F0F" }，
可以通过 `mkdocs.yml` 来进行配置。

## 配置

### 导航

<!-- md:version 9.0.0 -->
<!-- md:feature -->

页脚可以包含指向当前页面的上一页和下一页的链接。
如果您希望启用此行为，请在 `mkdocs.yml` 中添加以下行：


``` yaml
theme:
  features:
    - navigation.footer
```

### 社交媒体链接

<!-- md:version 1.0.0 -->
<!-- md:default none -->


社交媒体链接旁边可以给项目文档添加版权声明，在 `mkdocs.yml` 中添加：

``` yaml
extra:
  social:
    - icon: fontawesome/brands/mastodon # (1)!
      link: https://fosstodon.org/@squidfunk
```

1.  Enter a few keywords to find the perfect icon using our [icon search] and
    click on the shortcode to copy it to your clipboard:

    <div class="mdx-iconsearch" data-mdx-component="iconsearch">
      <input class="md-input md-input--stretch mdx-iconsearch__input" placeholder="Search icon" data-mdx-component="iconsearch-query" value="mastodon" />
      <div class="mdx-iconsearch-result" data-mdx-component="iconsearch-result" data-mdx-mode="file">
        <div class="mdx-iconsearch-result__meta"></div>
        <ol class="mdx-iconsearch-result__list"></ol>
      </div>
    </div>

以下属性可用于所有链接：

<!-- md:option social.icon -->

:   <!-- md:default none --> <!-- md:flag required -->
    此属性必须包含与主题绑定的对应图标有效路径，否则构建将无法成功。例如一些主流的选择：

    * :fontawesome-brands-github: – `fontawesome/brands/github`
    * :fontawesome-brands-gitlab: – `fontawesome/brands/gitlab`
    * :fontawesome-brands-x-twitter: – `fontawesome/brands/x-twitter`
    * :fontawesome-brands-mastodon: – `fontawesome/brands/mastodon`  <small>automatically adds [`rel=me`][rel=me]</small>
    * :fontawesome-brands-docker: – `fontawesome/brands/docker`
    * :fontawesome-brands-facebook: – `fontawesome/brands/facebook`
    * :fontawesome-brands-instagram: – `fontawesome/brands/instagram`
    * :fontawesome-brands-linkedin: – `fontawesome/brands/linkedin`
    * :fontawesome-brands-slack: – `fontawesome/brands/slack`
    * :fontawesome-brands-discord: – `fontawesome/brands/discord`
    * :fontawesome-brands-pied-piper-alt: – `fontawesome/brands/pied-piper-alt`

<!-- md:option social.link -->

:   <!-- md:default none --> <!-- md:flag required -->
    This property must be set to a relative or absolute URL including the URI
    scheme. All URI schemes are supported, including `mailto` and `bitcoin`:

    此属性必须设置为包含协议的 URI，并提供相对或绝对的 URL。支持所有 URI 方案，包括 `mailto`和 `bitcoin`：

    === ":fontawesome-brands-mastodon: Mastodon"

        ``` yaml
        extra:
          social:
            - icon: fontawesome/brands/mastodon
              link: https://fosstodon.org/@squidfunk
        ```

    === ":octicons-mail-16: Email"

        ``` yaml
        extra:
          social:
            - icon: fontawesome/solid/paper-plane
              link: mailto:<email-address>
        ```

<!-- md:option social.name -->

:   <!-- md:default _domain name from_ `link`_, if available_ -->
    name 属性用作链接的 `title` 标题属性，可以设置为一个可识别的名称，以提高可识别性。会在鼠标移动到对象上时显示 name 的内容。

    ``` yaml
    extra:
      social:
        - icon: fontawesome/brands/mastodon
          link: https://fosstodon.org/@squidfunk
          name: squidfunk on Fosstodon
    ```

  [icon search]: ../reference/icons-emojis.md#search
  [rel=me]: https://docs.joinmastodon.org/user/profile/#verification

### 版权声明

<!-- md:version 0.1.0 -->
<!-- md:default none -->

自定义版权横幅可以呈现为页脚的一部分，即显示在社交媒体链接旁边(页面左侧)。可以直接在  `mkdocs.yml` 中定义：

``` yaml
copyright: Copyright &copy; 2025 - 2025 Martin Donath
```

### 网站生成器声明(Generator notice)

<!-- md:version 7.3.0 -->
<!-- md:default `true` -->

页脚显示一个 _Made with Material-for-MkDocs_ 提示来表示当前网站是基于什么生成器生成的。
可以在 `mkdocs.yml` 使用以下选项删除该通知

``` yaml
extra:
  generator: false
```

!!! info "请阅读以下内容后，在考虑是否删除网站生成器声明 "

    在脚注中添加 __Made with Material-for-MkDocs__ 提示是这个项目如此受欢迎的重要原因，因为他告诉用户当前网站是基于什么方式生成的，帮助新用户发现这个项目。
    在删除前请考虑到你正在享受 @squidfunk 带来的好处，因为这个项目是开源的，并且有一个宽松的许可证。作者在这个项目上投入了数千小时，大部分时间没有任何经济回报。

    因此，如果您删除此通知，请考虑 [赞助][Insiders] 该项目。__Thank you__ :octicons-heart-fill-24:{ .mdx-heart .mdx-insiders }

  [Insiders]: ../insiders/index.md

## 使用

### 隐藏 prev/next 链接

通过 `hide` 属性设置可以隐藏页脚导航中的 “上一页”和“下一页”链接的。

在 Markdown 文档的顶部添加如下内容

``` yaml
---
hide:
  - footer
---

# Page title
...
```

## 自定义

### 自定义 Copyright

<!-- md:version 8.0.0 -->
<!-- md:flag customization -->

为了自定义和覆盖 [copyright notice], [extend the theme] 和 [override the `copyright.html` partial][overriding partials], 通常在 `mkdocs.yml` 中 `copyright` 属性进行设置。

  [copyright notice]: #copyright-notice
  [generator notice]: #generator-notice
  [extend the theme]: ../customization.md#extending-the-theme
  [overriding partials]: ../customization.md#overriding-partials


## 参考

- <https://github.com/squidfunk/mkdocs-material/blob/master/docs/setup/setting-up-the-footer.md>