# 颜色设置

正如任何符合规范的 Material Design 实现一样，Material-for-MkDocs 支持 Google 的原始[主题颜色][color palette]，并且可以通过 `mkdocs.yml` 文件轻松配置这些颜色。此外，通过使用[CSS变量][custom colors]，你还可以仅用几行CSS代码来自定义颜色，以符合你的品牌形象。

## 配置

### 主题颜色

#### 配色方案

<!-- md:version 5.2.0 -->
<!-- md:default `default` -->

Material-for-MkDocs 支持两种配色方案：一个 __light 模式__，它被称为 `default` ，和一个 __dark模式__，它被称为 `slate` 。配色方案可以通过 `mkdocs.yml` 来设置：


``` yaml
theme:
  palette:
    scheme: default
```


点击下面的按钮进行主题颜色的切换：

<div class="mdx-switch">
  <button data-md-color-scheme="default"><code>default</code></button>
  <button data-md-color-scheme="slate"><code>slate</code></button>
</div>

<script>
  var buttons = document.querySelectorAll("button[data-md-color-scheme]")
  buttons.forEach(function(button) {
    button.addEventListener("click", function() {
      document.body.setAttribute("data-md-color-switching", "")
      var attr = this.getAttribute("data-md-color-scheme")
      document.body.setAttribute("data-md-color-scheme", attr)
      var name = document.querySelector("#__code_0 code span.l")
      name.textContent = attr
      setTimeout(function() {
        document.body.removeAttribute("data-md-color-switching")
      })
    })
  })
</script>


#### 主色调

<!-- md:version 0.2.0 -->
<!-- md:default `indigo` -->

主色调用于标题、侧边栏、文本链接和其他几个组件。为了改变主色调，在 `mkdocs.yml` 中设置以下值。需要一个有效的颜色名称：

``` yaml
theme:
  palette:
    primary: indigo
```

点击下面的按钮切换站点的主色调：

<div class="mdx-switch">
  <button data-md-color-primary="red"><code>red</code></button>
  <button data-md-color-primary="pink"><code>pink</code></button>
  <button data-md-color-primary="purple"><code>purple</code></button>
  <button data-md-color-primary="deep-purple"><code>deep purple</code></button>
  <button data-md-color-primary="indigo"><code>indigo</code></button>
  <button data-md-color-primary="blue"><code>blue</code></button>
  <button data-md-color-primary="light-blue"><code>light blue</code></button>
  <button data-md-color-primary="cyan"><code>cyan</code></button>
  <button data-md-color-primary="teal"><code>teal</code></button>
  <button data-md-color-primary="green"><code>green</code></button>
  <button data-md-color-primary="light-green"><code>light green</code></button>
  <button data-md-color-primary="lime"><code>lime</code></button>
  <button data-md-color-primary="yellow"><code>yellow</code></button>
  <button data-md-color-primary="amber"><code>amber</code></button>
  <button data-md-color-primary="orange"><code>orange</code></button>
  <button data-md-color-primary="deep-orange"><code>deep orange</code></button>
  <button data-md-color-primary="brown"><code>brown</code></button>
  <button data-md-color-primary="grey"><code>grey</code></button>
  <button data-md-color-primary="blue-grey"><code>blue grey</code></button>
  <button data-md-color-primary="black"><code>black</code></button>
  <button data-md-color-primary="white"><code>white</code></button>
</div>

<script>
  var buttons = document.querySelectorAll("button[data-md-color-primary]")
  buttons.forEach(function(button) {
    button.addEventListener("click", function() {
      var attr = this.getAttribute("data-md-color-primary")
      document.body.setAttribute("data-md-color-primary", attr)
      var name = document.querySelector("#__code_1 code span.l")
      name.textContent = attr.replace("-", " ")
    })
  })
</script>

请参阅下面的指南来了解如何设置  [custom colors].

#### 强调色

<!-- md:version 0.2.0 -->
<!-- md:default `indigo` -->

强调色用于表示可以与之交互的元素，例如悬停链接、按钮和滚动条。它可以在  `mkdocs.yml` 中更改。通过选择一个有效的颜色名称：

``` yaml
theme:
  palette:
    accent: indigo
```

点击下面的按钮切换站点的强调色：

<style>
  .md-typeset button[data-md-color-accent] > code {
    background-color: var(--md-code-bg-color);
    color: var(--md-accent-fg-color);
  }
</style>

<div class="mdx-switch">
  <button data-md-color-accent="red"><code>red</code></button>
  <button data-md-color-accent="pink"><code>pink</code></button>
  <button data-md-color-accent="purple"><code>purple</code></button>
  <button data-md-color-accent="deep-purple"><code>deep purple</code></button>
  <button data-md-color-accent="indigo"><code>indigo</code></button>
  <button data-md-color-accent="blue"><code>blue</code></button>
  <button data-md-color-accent="light-blue"><code>light blue</code></button>
  <button data-md-color-accent="cyan"><code>cyan</code></button>
  <button data-md-color-accent="teal"><code>teal</code></button>
  <button data-md-color-accent="green"><code>green</code></button>
  <button data-md-color-accent="light-green"><code>light green</code></button>
  <button data-md-color-accent="lime"><code>lime</code></button>
  <button data-md-color-accent="yellow"><code>yellow</code></button>
  <button data-md-color-accent="amber"><code>amber</code></button>
  <button data-md-color-accent="orange"><code>orange</code></button>
  <button data-md-color-accent="deep-orange"><code>deep orange</code></button>
</div>

<script>
  var buttons = document.querySelectorAll("button[data-md-color-accent]")
  buttons.forEach(function(button) {
    button.addEventListener("click", function() {
      var attr = this.getAttribute("data-md-color-accent")
      document.body.setAttribute("data-md-color-accent", attr)
      var name = document.querySelector("#__code_2 code span.l")
      name.textContent = attr.replace("-", " ")
    })
  })
</script>

请参阅下面的指南来了解如何设置  [custom colors].

### 主题颜色切换

<!-- md:version 7.1.0 -->
<!-- md:default none -->
<!-- md:example color-palette-toggle -->

提供浅色和深色主题颜色使您的文档在一天中的不同时间阅读起来很愉快，因此用户可以相应地进行选择。在 `mkdocs.yml` 中添加以下行：

``` yaml
theme:
  palette: # (1)!

    # Palette toggle for light mode
    - scheme: default
      toggle:
        icon: material/brightness-7 # (2)!
        name: Switch to dark mode

    # Palette toggle for dark mode
    - scheme: slate
      toggle:
        icon: material/brightness-4
        name: Switch to light mode
```


1、 注意 `theme.palette` 设置现在定义为一个列表。

2、输入几个关键字，使用我们的 [icon search] 找到匹配的图标，然后点击短代码将其复制到剪贴板：

<div class="mdx-iconsearch" data-mdx-component="iconsearch">
    <input class="md-input md-input--stretch mdx-iconsearch__input" placeholder="Search icon" data-mdx-component="iconsearch-query" value="brightness" />
    <div class="mdx-iconsearch-result" data-mdx-component="iconsearch-result" data-mdx-mode="file">
    <div class="mdx-iconsearch-result__meta"></div>
    <ol class="mdx-iconsearch-result__list"></ol>
    </div>
</div>

这个配置将在搜索栏旁边呈现一个主题颜色切换。注意，您还可以为 [`primary`][palette.primary] 和 [`accent`][palette.accent] 定义单独的设置。

每个切换必须设置以下属性：

<!-- md:option palette.toggle.icon -->

:   <!-- md:default none --> <!-- md:flag required -->
    此属性必须指向引用与主题绑定的有效图标路径，否则构建将不成功。一些流行的组合：

    * :material-brightness-7: + :material-brightness-4: – `material/brightness-7` + `material/brightness-4`
    * :material-toggle-switch: + :material-toggle-switch-off-outline: – `material/toggle-switch` + `material/toggle-switch-off-outline`
    * :material-weather-night: + :material-weather-sunny: – `material/weather-night` + `material/weather-sunny`
    * :material-eye: + :material-eye-outline: – `material/eye` + `material/eye-outline`
    * :material-lightbulb: + :material-lightbulb-outline: – `material/lightbulb` + `material/lightbulb-outline`

<!-- md:option palette.toggle.name -->

:   <!-- md:default none --> <!-- md:flag required -->
    此属性用作开关的 `title` 属性，并应设置为可识别的名称，以提高可识别性。它呈现为[tooltip]。

  [palette.scheme]: #color-scheme
  [palette.primary]: #primary-color
  [palette.accent]: #accent-color
  [icon search]: ../reference/icons-emojis.md#search
  [tooltip]: ../reference/tooltips.md

### 系统设置

<!-- md:version 7.1.0 -->
<!-- md:default none -->
<!-- md:example color-palette-system-preference -->

通过使用媒体查询，每个主题颜色都可以与用户对于明暗外观的系统偏好相关联。只需在 `mkdocs.yml` 中的  `scheme`  定义旁边添加一个 `media` 属性：

``` yaml
theme:
  palette:

    # Palette toggle for light mode
    - media: "(prefers-color-scheme: light)"
      scheme: default
      toggle:
        icon: material/brightness-7
        name: Switch to dark mode

    # Palette toggle for dark mode
    - media: "(prefers-color-scheme: dark)"
      scheme: slate
      toggle:
        icon: material/brightness-4
        name: Switch to light mode
```

当用户首次访问您的网站时，媒体查询会按照它们被定义的顺序进行评估。第一个匹配的媒体查询会选择默认的主题颜色。

#### 自动 light / dark 模块

<!-- md:version 9.5.0 -->
<!-- md:flag experimental -->
<!-- md:example color-palette-system-preference -->

最新一些的操作系统允许在白天和夜晚之间自动切换明暗外观。Material-for-MkDocs 增加了对自动明暗模式的支持，将主题颜色的选择委托给用户的操作系统。在 `mkdocs.yml` 文件中添加以下行：

``` yaml
theme:
  palette:

    # Palette toggle for automatic mode
    - media: "(prefers-color-scheme)"
      toggle:
        icon: material/brightness-auto
        name: Switch to light mode

    # Palette toggle for light mode
    - media: "(prefers-color-scheme: light)"
      scheme: default # (1)!
      toggle:
        icon: material/brightness-7
        name: Switch to dark mode

    # Palette toggle for dark mode
    - media: "(prefers-color-scheme: dark)"
      scheme: slate
      toggle:
        icon: material/brightness-4
        name: Switch to system preference
```

1、 您还可以为每种主题颜色分别定义[primary][palette.primary]和[accent][palette.accent]的设置，即为明模式和暗模式设置不同的颜色。

现在，每当操作系统在明暗外观之间切换时，Material-for-MkDocs 都会更改主题颜色，即使用户没有重新加载网站也是如此。

  [Insiders]: ../insiders/index.md

## 自定义

### 自定义颜色

<!-- md:version 5.0.0 -->
<!-- md:example custom-colors -->

Material-for-MkDocs 使用 [CSS variables](custom properties) 来实现颜色。如果您想要对主题颜色之外的颜色进行自定义（例如，使用您品牌的特定颜色），您可以添加一个 [additional style sheet] 并调整CSS变量的值。

首先，在 `mkdocs.yml` 中将[`primary`][palette.primary]或[`accent`][palette.accent]的值设置为 `custom` ，以向主题表明您想要定义自定义颜色，例如，当您想要覆盖 `primary` 颜色时：

``` yaml
theme:
  palette:
    primary: custom
```

假设您是 :fontawesome-brands-youtube:{ style="color: #EE0F0F" } __YouTube__ ，并且想要将主色设置为您品牌配色。只需添加：

=== ":octicons-file-code-16: `docs/stylesheets/extra.css`"

    ``` css
    :root {
      --md-primary-fg-color:        #EE0F0F;
      --md-primary-fg-color--light: #ECB7B7;
      --md-primary-fg-color--dark:  #90030C;
    }
    ```

=== ":octicons-file-code-16: `mkdocs.yml`"

    ``` yaml
    extra_css:
      - stylesheets/extra.css
    ```

请参阅包含[color definitions]的文件，以获取所有CSS变量的列表。


### 自定义配色

除了覆盖特定颜色之外，您还可以通过将定义包装在 `[data-md-color-scheme="..."]`[attribute selector] 中来创建自己的命名配色方案，然后您可以在 `mkdocs.yml` 文件中按照 [color schemes][palette.scheme] 部分中的说明进行设置：

=== ":octicons-file-code-16: `docs/stylesheets/extra.css`"

    ``` css
    [data-md-color-scheme="youtube"] {
      --md-primary-fg-color:        #EE0F0F;
      --md-primary-fg-color--light: #ECB7B7;
      --md-primary-fg-color--dark:  #90030C;
    }
    ```

=== ":octicons-file-code-16: `mkdocs.yml`"

    ``` yaml
    theme:
      palette:
        scheme: youtube
    extra_css:
      - stylesheets/extra.css
    ```

此外，`slate` 配色方案通过 `hsla` 颜色函数定义了所有颜色，并根据 `--md-hue` CSS变量推导其颜色。您可以使用以下方式调整 `slate` 主题：



``` css
[data-md-color-scheme="slate"] {
  --md-hue: 210; /* (1)! */
}
```

1、 其中 `hue` 值必须在`[0, 360]` 范围内


  [attribute selector]: https://www.w3.org/TR/selectors-4/#attribute-selectors
  [color palette]: http://www.materialui.co/colors
  [custom colors]: #custom-colors
  [CSS variables]: https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties
  [color definitions]: https://github.com/squidfunk/mkdocs-material/blob/master/src/templates/assets/stylesheets/main/_colors.scss
  [additional style sheet]: ../customization.md#additional-css