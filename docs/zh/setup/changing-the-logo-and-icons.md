# 修改 logo and icons

在安装 MkDocs For Material主 题时，您会立即获得超过8,000个图标的使用权限，这些图标可用于自定义主题的特定部分，或者在您使用Markdown编写文档时使用。觉得这些还不够吗？您还可以毫不费力地添加更多图标。

  [additional icons]: #additional-icons

## 配置

### Logo

<!-- md:version 0.1.0 -->
<!-- md:default `material/library` -->

可以将 logo 更改为位于docs文件夹中的用户提供的图像（任何类型，包括 `*.png` 和 `*.svg`），或者更改为与该主题捆绑在一起的任何图标。在 `mkdocs.yml` 文件中添加以下行：

=== ":octicons-image-16: Image"

    ``` yaml
    theme:
      logo: assets/logo.png
    ```

=== ":octicons-package-16: Icon, bundled"

    ``` yaml
    theme:
      icon:
        logo: material/library # (1)!
    ```

    1、输入几个关键字，使用我们的 [图标搜索][icon search] 功能找到合适的图标，并点击短代码将其复制到您的剪贴板：


        <div class="mdx-iconsearch" data-mdx-component="iconsearch">
          <input class="md-input md-input--stretch mdx-iconsearch__input" placeholder="Search icon" data-mdx-component="iconsearch-query" value="material library" />
          <div class="mdx-iconsearch-result" data-mdx-component="iconsearch-result" data-mdx-mode="file">
            <div class="mdx-iconsearch-result__meta"></div>
            <ol class="mdx-iconsearch-result__list"></ol>
          </div>
        </div>

  [icon search]: ../reference/icons-emojis.md#search


通常，页眉和侧边栏中的 LOGO 会链接到文档的首页，这与 `site_url` 相同。可以通过以下配置更改此行为：

``` yaml
extra:
  homepage: https://example.com
```

### 网站图标（Favicon）

<!-- md:version 0.1.0 -->
<!-- md:default [`assets/images/favicon.png`][Favicon default] -->

可以将网站图标（favicon）更改为指向用户提供的图像的路径，该图像必须位于 docs 文件夹中。在 `mkdocs.yml` 文件中添加以下行：


``` yaml
theme:
  favicon: images/favicon.png
```

  [Favicon default]: https://github.com/squidfunk/mkdocs-material/blob/master/material/assets/images/favicon.png

### 网站 icons

[:octicons-tag-24: 9.2.0][Site icon support]

您网站上看到的大多数图标，如导航图标，也是可以更改的。例如，要更改页脚中的导航箭头，请在 `mkdocs.yml` 文件中添加以下行：

```yaml
theme:
  icon:
    previous: fontawesome/solid/angle-left
    next: fontawesome/solid/angle-right
```


以下是该主题所使用的可自定义图标的完整列表：

| Icon name    | Purpose                                                                       |
|:-------------|:------------------------------------------------------------------------------|
| `logo`       | See [Logo](#logo)                                                             |
| `menu`       | Open drawer                                                                   |
| `alternate`  | Change language                                                               |
| `search`     | Search icon                                                                   |
| `share`      | Share search                                                                  |
| `close`      | Reset search, dismiss announcements                                           |
| `top`        | Back-to-top button                                                            |
| `edit`       | Edit current page                                                             |
| `view`       | View page source                                                              |
| `repo`       | Repository icon                                                               |
| `admonition` | See [Admonition icons](../reference/admonitions.md#admonition-icons)          |
| `tag`        | See [Tag icons and identifiers](setting-up-tags.md#tag-icons-and-identifiers) |
| `previous`   | Previous page in footer, hide search on mobile                                |
| `next`       | Next page in footer                                                           |


  [Site icon support]: https://github.com/squidfunk/mkdocs-material/releases/tag/9.2.0

## 自定义

### 添加 icons

为了使用自定义图标，请[扩展主题][extend the theme]，并在您希望用于覆盖的[`custom_dir`][custom_dir]中创建一个名为`.icons`的新文件夹。
接下来，将您的`*.svg`图标文件添加到`.icons`文件夹的子文件夹中。假设您已经下载并解压了[Bootstrap]图标集，并且想将其添加到您的项目文档中。
您的项目结构应该如下所示：


``` { .sh .no-copy }
.
├─ overrides/
│  └─ .icons/
│     └─ bootstrap/
│        └─ *.svg
└─ mkdocs.yml
```

然后，添加如下行到 `mkdocs.yml`:

``` yaml
markdown_extensions:
  - pymdownx.emoji:
      emoji_index: !!python/name:material.extensions.emoji.twemoji
      emoji_generator: !!python/name:material.extensions.emoji.to_svg
      options:
        custom_icons:
          - overrides/.icons
```

现在，您可以在 Markdown 文件的任何位置以及 `mkdocs.yml` 中可以使用图标的任何位置使用所有 :fontawesome-brands-bootstrap: Bootstrap图标。但是，请注意，语法略有不同：

- __在配置中使用图标__ ：从`.icons`文件夹开始，取`*.svg`图标文件的路径，并去掉文件扩展名。例如，对于`.icons/bootstrap/envelope-paper.svg`，应使用：

    ``` yaml
    theme:
      icon:
        logo: bootstrap/envelope-paper
    ```

- __在Markdown文件中使用图标__ ：除了像上面提到的那样从`.icons`文件夹中取路径外，还需要将所有`/`替换为`-`，并用两个冒号将图标短代码括起来：

    ```
    :bootstrap-envelope-paper:
    ```
关于图标使用的更多说明，请参阅[图标参考][icon reference]。

  [extend the theme]: ../customization.md#extending-the-theme
  [custom_dir]: https://www.mkdocs.org/user-guide/configuration/#custom_dir
  [Bootstrap]: https://icons.getbootstrap.com/
  [icon reference]: ../reference/icons-emojis.md#using-icons