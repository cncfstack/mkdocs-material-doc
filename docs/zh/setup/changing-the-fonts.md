# 字体设置

*MkDocs Material 主题* 可以很方便修改项目文档的字体，因为它直接与 [Google Fonts](https://fonts.google.com) 集成。
另外，出于数据隐私原因进行私有化离线管理场景，或者需要使用其他来源字体时，也可以自定义加载字体。

  [Google Fonts]: https://fonts.google.com

## 配置

### 常规字体

<!-- md:version 0.1.2 -->
<!-- md:default [`Roboto`][Roboto] -->

正文字体、标题以及几乎所有不需要等宽字体的内容都使用常规字体。
您可以通过 `mkdocs.yml` 将其设置为任何有效的 [Google 字体][Google Fonts].


``` yaml
theme:
  font:
    text: Roboto
```

默认字体会加载 300, 400, _400i_ 和 __700__ 几种字重类型。

  [Roboto]: https://fonts.google.com/specimen/Roboto

### 等宽字体

<!-- md:version 0.1.2 -->
<!-- md:default [`Roboto Mono`][Roboto Mono] -->

_monospaced字体_ 用于代码块，可以单独配置。
就像常规字体一样，您可以通过 `mkdocs.yml` 将其设置为任何有效的 [Google 字体][Google Fonts].


``` yaml
theme:
  font:
    code: Roboto Mono
```

默认字体会加载 400 字重.

  [Roboto Mono]: https://fonts.google.com/specimen/Roboto+Mono

### 自动加载

<!-- md:version 1.0.0 -->
<!-- md:default none -->

如果你想阻止字体从 [Google Fonts] 加载，
例如：要遵守【数据隐私】规定，并退回到系统字体，请在 `mkdocs.yml` 添加下面几行

``` yaml
theme:
  font: false
```

!!! tip "Automatically bundle Google Fonts"

    [built-in privacy plugin] 可以在遵守 __通用数据保护条例__ （GDPR）的情况下，便捷地使用谷歌字体的同时，使用自动下载的和自托管的web字体文件。

  [data privacy]: https://developers.google.com/fonts/faq/privacy
  [built-in privacy plugin]:../plugins/privacy.md

## 自定义

### 其他来源字体

如果你想从其他来源加载字体或者覆盖系统字体，你可以使用 [additional style sheet] 来添加对应的 `@font-face` 定义：

=== ":octicons-file-code-16: `docs/stylesheets/extra.css`"

    ``` css
    @font-face {
      font-family: "<font>";
      src: "...";
    }
    ```

=== ":octicons-file-code-16: `mkdocs.yml`"

    ``` yaml
    extra_css:
      - stylesheets/extra.css
    ```

字体可以应用于特定的元素，例如，只有标题，或全局性地用作整个网站的常规字体或等宽字体：


=== "常规字体"

    ``` css
    :root {
      --md-text-font: "<font>"; /* (1)! */
    }
    ```

    1.  通过 CSS 变量来定义字体，而不是使用 `font-family` 方式，这样可以禁用系统字体回退.


=== "等宽字体"

    ``` css
    :root {
      --md-code-font: "<font>";
    }
    ```

  [additional style sheet]: ../customization.md#additional-css

## 使用建议

- 1、如果没有自定义字体的需求，请设置 `font: false`，否则可能会由于 Google 网站连接问题出现加载异常。
