# 数据隐私保护

Material-for-MkDocs 让遵守数据隐私法规变得非常容易，因为它提供了一个内置的[cookie同意][cookie consent]解决方案，用于在设置[分析][analytics]功能之前明确征得用户的同意。此外，外部资源可以自动下载以用于[自行托管][self-hosting]。

  [cookie consent]: #cookie-consent
  [analytics]: setting-up-site-analytics.md
  [self-hosting]: #built-in-privacy-plugin

## 配置

### Cookie 同意

<!-- md:version 8.4.0 -->
<!-- md:default none -->
<!-- md:flag experimental -->
<!-- md:example cookie-consent -->

Material-for-MkDocs 提供了一个内置的且可扩展的 Cookie 同意表单，该表单在向第三方发送请求之前会请求用户同意。请将以下内容添加到 `mkdocs.yml` 文件中：

``` yaml
extra:
  consent:
    title: Cookie consent
    description: >- # (1)!
      We use cookies to recognize your repeated visits and preferences, as well
      as to measure the effectiveness of our documentation and whether users
      find what they're searching for. With your consent, you're helping us to
      make our documentation better.
```

1、 您可以在 `description` 中添加任意 HTML 标签，例如，用于链接到您的服务条款或网站的其他部分。

以下属性可用：

<!-- md:option consent.title -->

:   <!-- md:default none --> <!-- md:flag required -->
    此属性设置 Cookie 同意表单的标题，该标题会显示在表单的顶部，并且必须设置为非空字符串。

<!-- md:option consent.description -->

:   <!-- md:default none --> <!-- md:flag required -->
    此属性设置 Cookie 同意的描述，该描述会显示在标题下方，并且可以包含原始 HTML（例如，指向服务条款的链接）。

<!-- md:option consent.cookies -->

:   <!-- md:default none --> 
    您可以自定义 cookie 或更改内置 cookie 的初始 `checked` 状态和名称。目前，以下 cookie 是内置的：

    - __Google Analytics（谷歌分析）__– `analytics`（默认启用）
    - __GitHub__ – `github`（默认启用）

    每个 cookie 必须具有一个唯一标识符，该标识符在 `cookies` 映射中用作键，并且可以被设置为一个字符串，或者一个定义了 `name` 和 `checked` 状态的映射：


    ===  "Custom cookie name"

        ``` yaml
        extra:
          consent:
            cookies:
              analytics: Custom name
        ```

    ===  "Custom initial state"

        ``` yaml
        extra:
          consent:
            cookies:
              analytics:
                name: Google Analytics
                checked: false
        ```

    ===  "Custom cookie"

        ``` yaml
        extra:
          consent:
            cookies:
              analytics: Google Analytics # (1)!
              custom: Custom cookie
        ```

        1、 如果您将自定义 Cookie 定义为 `cookies` 属性的一部分，那么必须明确地将 `analytics` Cookie 添加回来，否则分析功能将不会被触发。

    如果 Google Analytics 是通过 `mkdocs.yml` 配置的，那么 Cookie 同意表单将自动包含一个设置，允许用户禁用它。[自定义 Cookie][Custom cookies] 可以从 JavaScript 中使用。

<!-- md:option consent.actions -->

:   <!-- md:default `[accept, manage]` --> 此属性定义了显示哪些按钮以及它们的显示顺序，例如，允许用户接受 Cookie 并管理设置：

    ``` yaml
    extra:
      consent:
        actions:
          - accept
          - manage # (1)!
    ```

    1、 如果在 `actions` 属性中省略了 `manage`（管理）设置按钮，则设置将始终显示。

    Cookie 同意表单包含三种类型的按钮：

    - `accept（接受）`——用于接受所选 Cookie 的按钮
    - `reject（拒绝）`——用于拒绝所有 Cookie 的按钮
    - `manage（管理）`——用于管理设置的按钮





当用户首次访问您的网站时，会显示一个cookie同意表单：

[![Cookie consent enabled]][Cookie consent enabled]

  [Custom cookies]: #custom-cookies
  [Cookie consent enabled]: ../../assets/screenshots/consent.png

#### 修改 Cookie 设置

为了遵守 GDPR（欧盟通用数据保护条例），用户必须能够随时更改其cookie设置。这可以通过在 `mkdocs.yml` 文件中的[版权声明][copyright notice]部分添加一个简单链接来实现：

``` yaml
copyright: >
  Copyright &copy; 2016 - 2024 Martin Donath –
  <a href="#__consent">Change cookie settings</a>
```

  [copyright notice]: setting-up-the-footer.md#copyright-notice

### 内置隐私插件

<!-- md:version 9.5.0 -->
<!-- md:plugin [privacy][built-in privacy plugin] -->
<!-- md:flag experimental -->

内置隐私插件在构建过程中会自动识别外部资源，并下载所有资源以便于进行非常简单的自主托管。请将以下几行添加到`mkdocs.yml`文件中：

``` yaml
plugins:
  - privacy
```

有关所有设置的列表，请参阅[插件文档][plugin documentation]。

  [plugin documentation]: ../plugins/privacy.md

!!! tip "外部托管图片并自动优化它们"

    这个选项使得[内置隐私插件][built-in privacy plugin]成为当您想要将诸如图片等资源托管在git仓库之外的另一个位置时的绝佳选择，以保持资源的最新状态并使您的仓库保持精简。

    此外，自 <!-- md:version insiders-4.30.0 --> 版本起，内置隐私插件已被完全重写，现在与[内置优化插件][built-in optimize plugin]完美配合工作，这意味着外部资源可以通过与文档其他部分相同的优化流程进行处理。这意味着您可以在仓库之外存储和编辑未优化的文件，并让这两个插件为您构建一个高度优化的网站。

    如果您想要实现单独的管道，即对某些图片进行与其他图片不同的优化，或者排除某些图片的下载，您可以使用多个[内置隐私插件][built-in privacy plugin].实例。

!!! question "Material-for-MkDocs为什么不能通过设计来打包所有资源？"

    Material-for-MkDocs不能简单地将所有自身资源打包的主要原因是它与[Google Fonts]的集成，Google Fonts提供了超过一千种不同的字体，可用于渲染您的文档。大多数字体都包含多种粗细，并被拆分成不同的字符集，以保持下载文件的大小适中，这样浏览器就只会下载真正需要的内容。以我们的默认[常规字体][regular font] Roboto为例，这会导致总共有[42个 `*.woff2`文件][example]。

    如果Material-for-MkDocs将所有字体文件打包，那么下载大小将达到数百兆字节，这会拖慢自动化构建的速度。此外，作者可能还会添加外部资源，如第三方脚本或样式表，这些都需要被记住并定义为进一步的本地资源。

    这正是[内置隐私插件][built-in privacy plugin] 存在的原因——它自动化了手动下载所有外部资源的过程，以确保在存在一些[技术限制][technical limitations]的情况下遵守GDPR。

  [Google Fonts]: changing-the-fonts.md
  [regular font]: changing-the-fonts.md#regular-font
  [example]: #example
  [built-in optimize plugin]: ../plugins/optimize.md
  [technical limitations]: ../plugins/privacy.md#limitations

??? example "展开以查看示例"

    对于官方文档，[内置隐私插件][built-in privacy plugin]会下载以下资源：

    ``` { .sh .no-copy #example }
    .
    └─ assets/external/
       ├─ unpkg.com/tablesort@5.3.0/dist/tablesort.min.js
       ├─ fonts.googleapis.com/css
       └─ fonts.gstatic.com/s/
          ├─ roboto/v29/
          │  ├─ KFOjCnqEu92Fr1Mu51TjASc-CsTKlA.woff2
          │  ├─ KFOjCnqEu92Fr1Mu51TjASc0CsTKlA.woff2
          │  ├─ KFOjCnqEu92Fr1Mu51TjASc1CsTKlA.woff2
          │  ├─ KFOjCnqEu92Fr1Mu51TjASc2CsTKlA.woff2
          │  ├─ KFOjCnqEu92Fr1Mu51TjASc3CsTKlA.woff2
          │  ├─ KFOjCnqEu92Fr1Mu51TjASc5CsTKlA.woff2
          │  ├─ KFOjCnqEu92Fr1Mu51TjASc6CsQ.woff2
          │  ├─ KFOjCnqEu92Fr1Mu51TzBic-CsTKlA.woff2
          │  ├─ KFOjCnqEu92Fr1Mu51TzBic0CsTKlA.woff2
          │  ├─ KFOjCnqEu92Fr1Mu51TzBic1CsTKlA.woff2
          │  ├─ KFOjCnqEu92Fr1Mu51TzBic2CsTKlA.woff2
          │  ├─ KFOjCnqEu92Fr1Mu51TzBic3CsTKlA.woff2
          │  ├─ KFOjCnqEu92Fr1Mu51TzBic5CsTKlA.woff2
          │  ├─ KFOjCnqEu92Fr1Mu51TzBic6CsQ.woff2
          │  ├─ KFOkCnqEu92Fr1Mu51xEIzIFKw.woff2
          │  ├─ KFOkCnqEu92Fr1Mu51xFIzIFKw.woff2
          │  ├─ KFOkCnqEu92Fr1Mu51xGIzIFKw.woff2
          │  ├─ KFOkCnqEu92Fr1Mu51xHIzIFKw.woff2
          │  ├─ KFOkCnqEu92Fr1Mu51xIIzI.woff2
          │  ├─ KFOkCnqEu92Fr1Mu51xLIzIFKw.woff2
          │  ├─ KFOkCnqEu92Fr1Mu51xMIzIFKw.woff2
          │  ├─ KFOlCnqEu92Fr1MmSU5fABc4EsA.woff2
          │  ├─ KFOlCnqEu92Fr1MmSU5fBBc4.woff2
          │  ├─ KFOlCnqEu92Fr1MmSU5fBxc4EsA.woff2
          │  ├─ KFOlCnqEu92Fr1MmSU5fCBc4EsA.woff2
          │  ├─ KFOlCnqEu92Fr1MmSU5fCRc4EsA.woff2
          │  ├─ KFOlCnqEu92Fr1MmSU5fChc4EsA.woff2
          │  ├─ KFOlCnqEu92Fr1MmSU5fCxc4EsA.woff2
          │  ├─ KFOlCnqEu92Fr1MmWUlfABc4EsA.woff2
          │  ├─ KFOlCnqEu92Fr1MmWUlfBBc4.woff2
          │  ├─ KFOlCnqEu92Fr1MmWUlfBxc4EsA.woff2
          │  ├─ KFOlCnqEu92Fr1MmWUlfCBc4EsA.woff2
          │  ├─ KFOlCnqEu92Fr1MmWUlfCRc4EsA.woff2
          │  ├─ KFOlCnqEu92Fr1MmWUlfChc4EsA.woff2
          │  ├─ KFOlCnqEu92Fr1MmWUlfCxc4EsA.woff2
          │  ├─ KFOmCnqEu92Fr1Mu4WxKOzY.woff2
          │  ├─ KFOmCnqEu92Fr1Mu4mxK.woff2
          │  ├─ KFOmCnqEu92Fr1Mu5mxKOzY.woff2
          │  ├─ KFOmCnqEu92Fr1Mu72xKOzY.woff2
          │  ├─ KFOmCnqEu92Fr1Mu7GxKOzY.woff2
          │  ├─ KFOmCnqEu92Fr1Mu7WxKOzY.woff2
          │  └─ KFOmCnqEu92Fr1Mu7mxKOzY.woff2
          └─ robotomono/v13/
             ├─ L0xTDF4xlVMF-BfR8bXMIhJHg45mwgGEFl0_3vrtSM1J-gEPT5Ese6hmHSV0mf0h.woff2
             ├─ L0xTDF4xlVMF-BfR8bXMIhJHg45mwgGEFl0_3vrtSM1J-gEPT5Ese6hmHSZ0mf0h.woff2
             ├─ L0xTDF4xlVMF-BfR8bXMIhJHg45mwgGEFl0_3vrtSM1J-gEPT5Ese6hmHSd0mf0h.woff2
             ├─ L0xTDF4xlVMF-BfR8bXMIhJHg45mwgGEFl0_3vrtSM1J-gEPT5Ese6hmHSh0mQ.woff2
             ├─ L0xTDF4xlVMF-BfR8bXMIhJHg45mwgGEFl0_3vrtSM1J-gEPT5Ese6hmHSt0mf0h.woff2
             ├─ L0xTDF4xlVMF-BfR8bXMIhJHg45mwgGEFl0_3vrtSM1J-gEPT5Ese6hmHSx0mf0h.woff2
             ├─ L0xdDF4xlVMF-BfR8bXMIjhOsXG-q2oeuFoqFrlnAIe2Imhk1T8rbociImtElOUlYIw.woff2
             ├─ L0xdDF4xlVMF-BfR8bXMIjhOsXG-q2oeuFoqFrlnAIe2Imhk1T8rbociImtEleUlYIw.woff2
             ├─ L0xdDF4xlVMF-BfR8bXMIjhOsXG-q2oeuFoqFrlnAIe2Imhk1T8rbociImtEluUlYIw.woff2
             ├─ L0xdDF4xlVMF-BfR8bXMIjhOsXG-q2oeuFoqFrlnAIe2Imhk1T8rbociImtEm-Ul.woff2
             ├─ L0xdDF4xlVMF-BfR8bXMIjhOsXG-q2oeuFoqFrlnAIe2Imhk1T8rbociImtEmOUlYIw.woff2
             └─ L0xdDF4xlVMF-BfR8bXMIjhOsXG-q2oeuFoqFrlnAIe2Imhk1T8rbociImtEn-UlYIw.woff2
    ```

  [built-in privacy plugin]: ../plugins/privacy.md
  [preconnect]: https://developer.mozilla.org/en-US/docs/Web/Performance/dns-prefetch

#### 高级设置

<!-- md:sponsors -->
<!-- md:version insiders-4.50.0 -->

以下高级设置目前仅对我们的[赞助者][Insiders]开放。它们完全是可选的，不会影响博客的功能，但可能对自定义有所帮助：

- [`log`][config.log]
- [`log_level`][config.log_level]

随着我们发现新的应用场景，我们将会在这里添加更多设置。

  [Insiders]: ../insiders/index.md
  [config.log]: ../plugins/privacy.md#config.log
  [config.log_level]: ../plugins/privacy.md#config.log_level

## 自定义

### 自定义 cookies

<!-- md:version 8.4.0 -->
<!-- md:example custom-cookies -->

如果您已自定义了[cookie同意][cookie consent]并添加了一个 `custom` cookie，系统将提示用户接受或拒绝您的自定义cookie。一旦用户接受或拒绝cookie同意，或者[更改了设置][changes the settings]，页面将重新加载[^1]。您可以使用[额外的JavaScript][additional JavaScript] 来查询结果：

  [^1]:
    我们重新加载页面是为了使与自定义cookie的互操作更加简单。如果Material-for-MkDocs采用基于回调的方法，作者需要确保正确更新所有使用cookie的脚本。此外，cookie同意只在最初时给出回应，因此我们认为这是在开发者体验（DX）和用户体验（UX）之间做出的良好权衡。

=== ":octicons-file-code-16: `docs/javascripts/consent.js`"

    ``` js
    var consent = __md_get("__consent")
    if (consent && consent.custom) {
      /* The user accepted the cookie */
    } else {
      /* The user rejected the cookie */
    }
    ```

=== ":octicons-file-code-16: `mkdocs.yml`"

    ``` yaml
    extra_javascript:
      - javascripts/consent.js
    ```

  [additional JavaScript]: ../customization.md#additional-javascript
  [changes the settings]: #change-cookie-settings
