# 添加 Git 仓库

如果您的文档与源代码相关，Material-for-MkDocs 提供了将项目存储库的信息作为静态网站的一部分进行显示的功能，包括星标和分叉数。此外，还可以显示[最后更新和创建的日期][date of last update and creation]以及[贡献者][contributors]。


## 配置

### 仓库

<!-- md:version 0.1.0 -->
<!-- md:default none -->

为了将指向您的项目存储库的链接作为文档的一部分进行显示，请在`mkdocs.yml`中将[`repo_url`][repo_url]设置为存储库的公共URL，例如：

``` yaml
repo_url: https://github.com/squidfunk/mkdocs-material
```


在大屏幕上，存储库的链接将显示在搜索栏旁边；而在小屏幕上，它将作为主导航抽屉（或侧边栏导航）的一部分进行显示。

此外，对于托管在[GitHub]或[GitLab]上的公共存储库，系统会自动获取并显示最新的发布标签[^1]、星标数量以及分叉数量。

  [^1]:
    不幸的是，GitHub仅提供一个API端点来获取[最新发布版本][latest release]，而不是最新的标签。
    因此，请确保为您希望显示在星标数量和分叉数量旁边的最新标签[创建一个发布版本][create a release]（而非预发布版本）。对于GitLab，虽然可以获取按更新时间排序的[标签列表][list of tags sorted by update time]，但使用的是[等效的API端点][equivalent API endpoint]。因此，请确保您也为GitLab存储库[创建一个发布版本][create a release for GitLab repositories]。

  [repo_url]: https://www.mkdocs.org/user-guide/configuration/#repo_url
  [latest release]: https://docs.github.com/en/rest/reference/releases#get-the-latest-release
  [create a release]: https://docs.github.com/en/repositories/releasing-projects-on-github/managing-releases-in-a-repository#creating-a-release
  [list of tags sorted by update time]: https://docs.gitlab.com/ee/api/tags.html#list-project-repository-tags
  [equivalent API endpoint]: https://docs.gitlab.com/ee/api/releases/#get-the-latest-release
  [create a release for GitLab repositories]: https://docs.gitlab.com/ee/user/project/releases/#create-a-release

#### 仓库名称

<!-- md:version 0.1.0 -->
<!-- md:default _automatically set to_ `GitHub`, `GitLab` _or_ `Bitbucket` -->

MkDocs 会通过检查URL来推断源代码提供者，并尝试自动设置 _存储库名称_。如果您希望自定义该名称，请在`mkdocs.yml`中设置[`repo_name`][repo_name]：

``` yaml
repo_name: squidfunk/mkdocs-material
```

  [repo_name]: https://www.mkdocs.org/user-guide/configuration/#repo_name

#### 参考图标

<!-- md:version 5.0.0 -->
<!-- md:default computed -->

虽然默认的存储库图标是一个通用的git图标，但可以通过在`mkdocs.yml`中引用有效的图标路径，将其设置为与主题捆绑的任何图标：

``` yaml
theme:
  icon:
    repo: fontawesome/brands/git-alt # (1)!
```

1、 输入几个关键词，使用我们的[图标搜索][icon search]功能找到完美的图标，并点击简码将其复制到剪贴板：


    <div class="mdx-iconsearch" data-mdx-component="iconsearch">
      <input class="md-input md-input--stretch mdx-iconsearch__input" placeholder="Search icon" data-mdx-component="iconsearch-query" value="git" />
      <div class="mdx-iconsearch-result" data-mdx-component="iconsearch-result" data-mdx-mode="file">
        <div class="mdx-iconsearch-result__meta"></div>
        <ol class="mdx-iconsearch-result__list"></ol>
      </div>
    </div>


一些主流的选择：

- :fontawesome-brands-git: – `fontawesome/brands/git`
- :fontawesome-brands-git-alt: – `fontawesome/brands/git-alt`
- :fontawesome-brands-github: – `fontawesome/brands/github`
- :fontawesome-brands-github-alt: – `fontawesome/brands/github-alt`
- :fontawesome-brands-gitlab: – `fontawesome/brands/gitlab`
- :fontawesome-brands-gitkraken: – `fontawesome/brands/gitkraken`
- :fontawesome-brands-bitbucket: – `fontawesome/brands/bitbucket`
- :fontawesome-solid-trash: – `fontawesome/solid/trash`

  [icon search]: ../reference/icons-emojis.md#search

#### 代码操作

<!-- md:version 9.0.0 -->
<!-- md:feature -->


如果[仓库URL][repository URL]指向一个有效的[GitHub]、[GitLab]或[Bitbucket]仓库，[MkDocs]提供了一个名为[`edit_uri`][edit_uri]的设置，该设置会解析到你托管文档的子文件夹。

如果你的默认分支被命名为 `main`，请将设置更改为：

``` yaml
edit_uri: edit/main/docs/
```

在确保 `edit_uri` 已正确配置后，可以添加代码操作按钮。支持两种类型的代码操作：`edit`（编辑）和 `view`（仅GitHub支持查看）：

=== ":material-file-edit-outline: 编辑本月没"

    ``` yaml
    theme:
      features:
        - content.action.edit
    ```

=== ":material-file-eye-outline: 查看此页面的源代码"

    ``` yaml
    theme:
      features:
        - content.action.view
    ```

编辑和查看的按钮可以通过以下方法修改图标：

``` yaml
theme:
  icon:
    edit: material/pencil # (1)!
    view: material/eye
```

1、输入几个关键词，使用我们的[图标搜索][icon search]功能来找到合适的图标，并点击短代码将其复制到您的剪贴板：


    <div class="mdx-iconsearch" data-mdx-component="iconsearch">
      <input class="md-input md-input--stretch mdx-iconsearch__input" placeholder="Search icon" data-mdx-component="iconsearch-query" value="material pencil" />
      <div class="mdx-iconsearch-result" data-mdx-component="iconsearch-result" data-mdx-mode="file">
        <div class="mdx-iconsearch-result__meta"></div>
        <ol class="mdx-iconsearch-result__list"></ol>
      </div>
    </div>

  [repository URL]: #repository
  [GitHub]: https://github.com/
  [GitLab]: https://about.gitlab.com/
  [Bitbucket]: https://bitbucket.org/
  [MkDocs]: https://www.mkdocs.org
  [edit_uri]: https://www.mkdocs.org/user-guide/configuration/#edit_uri

### 版本控制

以下插件与 Material-for-MkDocs 完全集成，可以显示文档的[最后更新日期和创建日期][date of last update and creation]，以及链接到所有涉及的[贡献者][contributors]或[作者][authors]。

  [date of last update and creation]: #document-dates
  [contributors]: #document-contributors
  [authors]: #document-authors

#### 文档日期

<!-- md:version 4.6.0 -->
<!-- md:plugin [git-revision-date-localized] -->


[git-revision-date-localized] 插件支持在每页的底部添加文档的最后更新日期和创建日期。你可以使用`pip`来安装它：


```
pip install mkdocs-git-revision-date-localized-plugin
```

然后，在 `mkdocs.yml` 中添加一下几行：

``` yaml
plugins:
  - git-revision-date-localized:
      enable_creation_date: true
```

支持以下配置选项：

<!-- md:option git-revision-date-localized.enabled -->

:   <!-- md:default `true` -->  此选项用于指定在构建项目时是否启用该插件。如果您想关闭该插件（例如，在本地构建时），可以使用一个[环境变量][environment variable]来实现。

    ``` yaml
    plugins:
      - git-revision-date-localized:
          enabled: !ENV [CI, false]
    ```

<!-- md:option git-revision-date-localized.type -->

:   <!-- md:default `date` --> 要显示的日期格式。有效值包括 `date`、`datetime`、`iso_date`、`iso_datetime`和`timeago`：


    ``` yaml
    plugins:
      - git-revision-date-localized:
          type: date
    ```

<!-- md:option git-revision-date-localized.enable_creation_date -->

:   <!-- md:default `false` --> 启用在页面底部紧邻最后更新日期显示与该页面相关联文件的创建日期：

    ``` yaml
    plugins:
      - git-revision-date-localized:
          enable_creation_date: true
    ```

    !!! note "当使用构建环境时"

        If you are deploying through a CI system, you might need to adjust your
        CI settings when fetching the code. For more information, see
        [git-revision-date-localized].

<!-- md:option git-revision-date-localized.fallback_to_build_date -->

:   <!-- md:default `false` --> 允许回退到执行`mkdocs build` 命令时的时间点。当在 git 仓库外部执行构建时，可以用作回退方案。


    ``` yaml
    plugins:
      - git-revision-date-localized:
          fallback_to_build_date: true
    ```

该扩展的其他配置选项不受 Material-for-MkDocs 官方支持，因此可能会导致意外结果。使用它们需自行承担风险。


  [git-revision-date-localized]: https://github.com/timvink/mkdocs-git-revision-date-localized-plugin

#### 文档贡献者


<!-- md:version 9.5.0 -->
<!-- md:plugin [git-committers] -->
<!-- md:flag experimental -->

[git-committers][^2] 插件会在每页底部渲染所有贡献者的 GitHub 头像，并链接到他们的 GitHub 个人资料。和往常一样，它可以通过 `pip` 安装：

  [^2]:
    我们目前推荐使用 [git-committers] 插件的一个分支版本，因为它包含了许多尚未合并回原始插件的改进。有关更多信息，请参见 byrnereese/mkdocs-git-committers-plugin#12。


```
pip install mkdocs-git-committers-plugin-2
```

然后，在 `mkdocs.yml` 中添加以下行：

``` yaml
plugins:
  - git-committers:
      repository: squidfunk/mkdocs-material
      branch: main
```

支持以下配置选项：

<!-- md:option git-committers.enabled -->

:   <!-- md:default `true` -->   此选项指定在构建项目时是否启用该插件。如果您想关闭该插件（例如，在进行本地构建时），请使用[环境变量][environment variable]：


    ``` yaml
    plugins:
      - git-committers:
          enabled: !ENV [CI, false]
    ```

<!-- md:option git-committers.repository -->

:   <!-- md:default none --> <!-- md:flag required -->
    此属性必须设置为包含您的文档的存储库的slug。slug必须遵循 `<用户名>/<存储库>` 的模式：

    ``` yaml
    plugins:
      - git-committers:
          repository: squidfunk/mkdocs-material
    ```

<!-- md:option git-committers.branch -->

:   <!-- md:default `master` --> 
    此属性应设置为从中检索贡献者的存储库分支。要使用 `main` 分支，请将其设置为：

    ``` yaml
    plugins:
      - git-committers:
          branch: main
    ```

该扩展的其他配置选项不受 Material-for-MkDocs 官方支持，因此可能会导致不可预见的结果。使用它们需自行承担风险。

  [Insiders]: ../insiders/index.md
  [git-committers]: https://github.com/ojacques/mkdocs-git-committers-plugin-2
  [environment variable]: https://www.mkdocs.org/user-guide/configuration/#environment-variables
  [rate limits]: https://docs.github.com/en/rest/overview/resources-in-the-rest-api#rate-limiting

#### 文档作者

<!-- md:version 9.5.0 -->
<!-- md:plugin [git-authors] -->
<!-- md:flag experimental -->

[git-authors] 插件是 [git-committers] 插件的一个轻量级替代方案，它可以从 git 中提取文档的作者，并在每页的底部显示他们。

Material-for-MkDocs 为 [git-authors] 提供了深度集成。这意味着不需要进行自定义覆盖，并且会添加额外的样式（如漂亮的图标）。只需使用 `pip` 安装它即可：


```
pip install mkdocs-git-authors-plugin
```


在 `mkdocs.yml` 中添加以下内容：

``` yaml
plugins:
  - git-authors
```

  [git-authors]: https://github.com/timvink/mkdocs-git-authors-plugin/