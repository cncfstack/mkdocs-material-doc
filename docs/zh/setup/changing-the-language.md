# 语言修改

Material-for-MkDocs 支持国际化（i18n），并为 60 多种语言的模板变量和标签提供翻译。
此外，如果可用，网站搜索可以配置为使用特定语言的词干提取器。

## 配置

### 站点语言

<!-- md:version 1.12.0 -->
<!-- md:default `en` -->

可以通过 `mkdocs.yml` 设置站点语言：

``` yaml
theme:
  language: en # (1)!
```

1、 HTML5 仅允许为[每个文档设置一种语言][single language per document]，因此 MkDocs For Material 主题仅支持为整个项目设置一个规范语言，即每个 `mkdocs.yml` 文件只能设置一个语言。   
    构建多语言文档的最简单方法是为每种语言在子文件夹中创建一个项目，然后使用[语言选择器][language selector]将这些项目相互链接。

支持以下语言：

<!-- hooks/translations.py -->

请注意，由于默认 slug 函数的工作方式，某些语言会生成无法阅读的锚点链接。请考虑使用[支持Unicode的slug函数][Unicode-aware slug function]。

!!! tip "缺少翻译吗？来帮帮我们吧，只需要5分钟！"

    Material-for-MkDocs 主题依赖于外部贡献来为其支持的60多种语言添加和更新翻译。如果您的语言显示缺少某些翻译，请点击链接进行添加。如果您的语言不在列表中，请点击此处[添加新语言][add a new language]。


  [single language per document]: https://www.w3.org/International/questions/qa-html-language-declarations.en#attributes
  [language selector]: #site-language-selector
  [Unicode-aware slug function]: extensions/python-markdown.md#+toc.slugify
  [add a new language]: https://github.com/squidfunk/mkdocs-material/issues/new?template=04-add-a-translation.yml&title=Add+translations+for+...

### 站点语言选择

<!-- md:version 7.0.0 -->
<!-- md:default none -->

如果您的文档提供多种语言版本，可以在页眉中添加一个指向这些语言的语言选择器。可以通过 `mkdocs.yml` 文件定义其他语言。


``` yaml
extra:
  alternate:
    - name: English
      link: /en/ # (1)!
      lang: en
    - name: Deutsch
      link: /de/
      lang: de
```

1、请注意，这必须是一个绝对链接。如果它包含域名部分，则会按定义使用。否则，会将 `mkdocs.yml` 中设置的[`site_url`][site_url]的域名部分添加到链接的前面。

每个备用语言具有以下属性可供选择：

<!-- md:option alternate.name -->

:   <!-- md:default none --> <!-- md:flag required -->
    此属性的值在语言选择器中用作语言的名称，并且必须设置为非空字符串。

<!-- md:option alternate.link -->

:   <!-- md:default none --> <!-- md:flag required -->
    此属性必须设置为绝对链接，该链接也可以指向另一个域或子域，这些域或子域不一定是由 MkDocs 生成的。



<!-- md:option alternate.lang -->

:   <!-- md:default none --> <!-- md:flag required -->
    此属性必须包含[ISO 639-1语言代码][ISO 639-1 language code]，并用于链接的 `hreflang` 属性，以提高搜索引擎的发现能力。

[![Language selector preview]][Language selector preview]

  [site_url]: https://www.mkdocs.org/user-guide/configuration/#site_url
  [ISO 639-1 language code]: https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
  [Language selector preview]: ../assets/screenshots/language-selection.png

#### 留在当前页面

<!-- md:sponsors -->
<!-- md:version insiders-4.47.0 -->
<!-- md:flag experimental -->


[Insiders] 改善了在不同语言之间切换时的用户体验，例如，如果语言 `en` 和 `de` 包含具有相同路径名称的页面，用户将留在当前页面：

=== "Insiders"

    ```
    docs.example.com/en/     -> docs.example.com/de/
    docs.example.com/en/foo/ -> docs.example.com/de/foo/
    docs.example.com/en/bar/ -> docs.example.com/de/bar/
    ```

=== "Material-for-MkDocs"

    ```
    docs.example.com/en/     -> docs.example.com/de/
    docs.example.com/en/foo/ -> docs.example.com/de/
    docs.example.com/en/bar/ -> docs.example.com/de/
    ```

无需进行任何配置。我们正在努力改进 2024 年的多语言支持，包括让未来的语言切换更加无缝。

  [Insiders]: ../insiders/index.md

### 方向性

<!-- md:version 2.5.0 -->
<!-- md:default computed -->

虽然许多语言都是从左到右（`ltr`）阅读的，但  Material-for-MkDocs 主题也支持从右到左（`rtl`）的方向性，这是根据所选语言推断出来的，但也可以通过以下方式设置


``` yaml
theme:
  direction: ltr
```

点击下面的标题切换阅读方向

<div class="mdx-switch">
  <button data-md-dir="ltr"><code>ltr</code></button>
  <button data-md-dir="rtl"><code>rtl</code></button>
</div>

<script>
  var buttons = document.querySelectorAll("button[data-md-dir]")
  buttons.forEach(function(button) {
    button.addEventListener("click", function() {
      var attr = this.getAttribute("data-md-dir")
      document.body.dir = attr
      var name = document.querySelector("#__code_2 code span.l")
      name.textContent = attr
    })
  })
</script>

## 自定义

### 自定义翻译


如果您想为某种语言自定义一些翻译，只需遵循[主题扩展][translations]的指南，并在 `overrides` 文件夹中创建一个新的局部文件。然后，导入该语言的[翻译]作为备用，并仅调整您想要覆盖的翻译：


=== ":octicons-file-code-16: `overrides/partials/languages/custom.html`"

    ``` html
    <!-- Import translations for language and fallback -->
    {% import "partials/languages/de.html" as language %}
    {% import "partials/languages/en.html" as fallback %} <!-- (1)! -->

    <!-- Define custom translations -->
    {% macro override(key) %}{{ {
      "source.file.date.created": "Erstellt am", <!-- (2)! -->
      "source.file.date.updated": "Aktualisiert am"
    }[key] }}{% endmacro %}

    <!-- Re-export translations -->
    {% macro t(key) %}{{
      override(key) or language.t(key) or fallback.t(key)
    }}{% endmacro %}
    ```

    1.  Note that `en` must always be used as a fallback language, as it's the
        default theme language.

    2.  Check the [list of available languages], pick the translation you want
        to override for your language and add them here.

=== ":octicons-file-code-16: `mkdocs.yml`"

    ``` yaml
    theme:
      language: custom
    ```

  [theme extension]: ../customization.md#extending-the-theme
  [translations]: https://github.com/squidfunk/mkdocs-material/blob/master/src/templates/partials/languages/
  [list of available languages]: https://github.com/squidfunk/mkdocs-material/blob/master/src/templates/partials/languages/
