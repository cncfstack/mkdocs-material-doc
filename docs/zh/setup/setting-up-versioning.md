# 设置多版本

Material-for-MkDocs通过与将这些功能添加到MkDocs（即 [mike]）的外部实用程序集成，可以轻松部署多个版本的项目文档。部署新版本时，文档的旧版本保持不变。

  [mike]: https://github.com/jimporter/mike

## 配置

### 版本控制

<!-- md:version 7.0.0 -->
<!-- md:utility [mike] -->

[mike] 可以轻松部署多个版本的项目文档。它与 Material-for-MkDocs 原生集成，可以通过`mkdocs.yml`启用：

``` yaml
extra:
  version:
    provider: mike
```

这会在标题中呈现一个版本选择器：

<figure markdown>

[![Version selector preview]][Version selector preview]

  <figcaption markdown>

Check out the versioning example to see it in action –
[mkdocs-material.github.io/example-versioning][version example]

  </figcaption>
</figure>

!!! quote "[为什么使用 mike?][Why use mike?]"

    Mike 的设计理念是，一旦您为特定版本生成了文档，就再也不需要对该版本进行任何修改。这意味着您无需担心 MkDocs 的破坏性更改，因为您的旧文档（使用旧版本的 MkDocs 生成）已经生成并保存在您的 `gh-pages` 分支中。

    虽然 Mike 非常灵活，但它最擅长将文档放置在 `<主版本号>.<次版本号>` 目录中，并为特别重要的版本提供可选的别名（例如 `latest` 或 `dev`）。这使得为任何您希望引导用户查看的文档版本创建永久链接变得轻而易举。

  [Version selector preview]: ../assets/screenshots/versioning.png
  [version example]: https://mkdocs-material.github.io/example-versioning/
  [Why use mike?]: https://github.com/jimporter/mike#why-use-mike

### 在切换版本时保持在同一页面

当用户在版本选择器中选择一个版本时，他们通常希望跳转到与之前查看的页面相对应的页面。MkDocs 的 Material 主题默认实现了这种行为，但有几个需要注意的事项：

- 必须在 `mkdocs.yml` 中正确设置 [`site_url`][mkdocs.site_url]。有关示例，请参见["Publishing a new version"](#publishing-a-new-version)部分。

- 重定向是通过 JavaScript 实现的，因此无法提前知道将被重定向到哪个页面。

### 版本警告


<!-- md:version 8.0.0 -->
<!-- md:flag customization -->

如果您正在使用版本控制，那么当用户访问除最新版本以外的任何其他版本时，您可能希望显示一个警告。通过使用[主题扩展][theme extension]，您可以[覆盖 outdated 块][overriding block]：



``` html
{% extends "base.html" %}

{% block outdated %}
  You're not viewing the latest version.
  <a href="{{ '../' ~ base_url }}"> <!-- (1)! -->
    <strong>Click here to go to latest.</strong>
  </a>
{% endblock %}
```

1、 给定 `href` 属性的这个值，链接将始终重定向到您的网站的根目录，然后该根目录将重定向到最新版本。这确保了您网站的旧版本不依赖于特定的别名（例如 `latest`），以便以后更改别名而不会破坏早期版本。

这将在标题上方呈现一个版本警告：

[![版本警告预览]][Version warning preview]

默认版本由 `latest` 别名标识。如果您希望将另一个别名设置为最新版本，例如 `stable`，请在 `mkdocs.yml` 中添加以下行：


``` yaml
extra:
  version:
    default: stable # (1)!
```

1、您还可以将多个别名定义为默认版本，例如 `stable` 和 `development`。


    ``` yaml
    extra:
      version:
        default:
          - stable
          - development
    ```

    现在，所有具有 `stable` 和 `development` 别名的版本都不会显示版本警告。

请确保一个别名与[默认版本][default version]匹配，因为这是用户将被重定向到的位置。

  [theme extension]: ../customization.md#extending-the-theme
  [overriding blocks]: ../customization.md#overriding-blocks
  [Version warning preview]: ../assets/screenshots/version-warning.png
  [default version]: #setting-a-default-version

### 版本别名

<!-- md:version 9.5.23 -->
<!-- md:default `false` -->

如果您正在使用别名进行版本控制，并且希望在版本号旁边显示版本别名，您可以通过将 `alias` 选项设置为 `true` 来启用此功能：



``` yaml
extra:
  version:
    alias: true
```

## 文章使用

虽然本节概述了发布新版本的基本工作流程，但最好查阅[mike的文档][mike]，以便熟悉其机制。

### 发布一个新版本

如果您想发布项目文档的新版本，请选择一个版本标识符，并使用以下命令更新设置为默认版本的别名集：


```
mike deploy --push --update-aliases 0.1 latest
```

请注意，每个版本都将部署为您 `site_url` 的一个子目录，您应该明确设置它。例如，如果您的 `mkdocs.yml` 包含以下内容：


``` yaml
site_url: 'https://docs.example.com/'  # Trailing slash is recommended
```


文档将被发布到类似以下的 URL：


- _docs.example.com/0.1/_
- _docs.example.com/0.2/_
- ...

### 设置一个默认版本

当开始使用 [mike] 时，一个很好的做法是将一个别名设置为默认版本，例如 latest，并且在发布新版本时，始终更新该别名以指向最新版本：


```
mike set-default --push latest
```

在发布新版本时，[mike] 将在您的项目文档的根目录中创建一个重定向，指向与别名关联的版本

_docs.example.com_ :octicons-arrow-right-24: _docs.example.com/0.1_

## 参考
- <https://github.com/squidfunk/mkdocs-material/blob/master/docs/setup/setting-up-versioning.md>