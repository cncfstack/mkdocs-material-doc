# 离线构建

如果您希望将文档与产品一同发布，MkDocs 能够满足您的需求 - 借助主题的支持， [MkDocs] 可以构建支持离线的文档。值得注意的是，Material-for-MkDocs 为其众多功能提供了离线支持。

  [MkDocs]: https://www.mkdocs.org

## 配置

### 内置离线插件

<!-- md:version 9.0.0 -->
<!-- md:plugin [offline] – built-in -->

内置离线插件确保在您将 [站点目录] 的内容作为下载包分发时，[站内搜索]功能仍然可以正常工作。只需在 `mkdocs.yml` 文件中添加以下几行代码：

``` yaml
plugins:
  - offline
```

要了解所有设置的列表，请参阅 [插件文档] 。

  [offline]: ../plugins/offline.md
  [插件文档]: ../plugins/offline.md

!!! tip "自动打包所有依赖资源"

    [内置隐私插件] 使得在构建用于离线使用的文档时，能够轻松地使用外部资源，因为它会自动下载所有外部资源，并将它们与您的文档一起分发。

  [站内搜索]: setting-up-site-search.md
  [站点目录]: https://www.mkdocs.org/user-guide/configuration/#site_dir
  [内置隐私插件]:../plugins/privacy.md

#### 限制

Material-for-MkDocs 提供了许多交互式功能，但由于现代浏览器的限制，其中一些功能在从文件系统直接打开时可能无法正常工作。特别是，所有使用 `fetch` API 的功能都会出错。

因此，在构建用于离线使用的文档时，请确保禁用以下配置设置：[即时加载]、[站点分析]、[Git 仓库]、[版本控制]和[评论系统]。

  [即时加载]: setting-up-navigation.md#instant-loading
  [站点分析]: setting-up-site-analytics.md
  [版本控制]: setting-up-versioning.md
  [Git 仓库]: adding-a-git-repository.md
  [评论系统]: adding-a-comment-system.md
