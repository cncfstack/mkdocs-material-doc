# Header 设置

Material-for-MkDocs 的 header 部分可以进行定制，可以显示一个公告栏，在滚动页面时自动消失，并且可以进行一些选项配置。
他还包括了搜索框 [search bar]  和 显示项目 Git 代码仓库的 [git repository] 的地方。在他们各自的模块中会有详细的解释。

  [search bar]: setting-up-site-search.md
  [git repository]: adding-a-git-repository.md

## 配置

### 自动隐藏

<!-- md:version 6.2.0 -->
<!-- md:feature -->

启用自动隐藏后，当用户屏幕滚动超过一定的阈值时，整个 Header 会自动隐藏，为页面内容留下更多的空间。
在 `mkdocs.yml` 添加下面几行：

``` yaml
theme:
  features:
    - header.autohide
```

### 公告栏

<!-- md:version 5.0.0 -->
<!-- md:flag customization -->

MkDocs Material 主题包括一个公告栏，这是非常好的位置向用户显示项目新闻或其他重要信息。

当用户滚动过标题后，公告将自动消失。为了添加一个公告栏，需要配置 [extend the theme]  和 [override the `announce`block][overriding blocks]，他们默认是空的。

修改路径: `overrides/main.html`

``` html
{% extends "base.html" %}

{% block announce %}
  <!-- Add announcement here, including arbitrary HTML -->
{% endblock %}
```

  [extend the theme]: ../customization.md#extending-the-theme
  [overriding blocks]: ../customization.md#overriding-blocks


#### 已读标记

<!-- md:version 8.4.0 -->
<!-- md:feature -->
<!-- md:flag experimental -->

用户可以对公告进行已读标记，通过一个按钮来取消当前的公告显示。
在 `mkdocs.yml` 添加如下几行：

``` yaml
theme:
  features:
    - announce.dismiss
```

当用户单击该按钮时，当前的公告将被关闭，在通知的内容发生变化前不会再次显示。这个过程是自动的。

## 配置建议

- 无

## 参考
- <https://github.com/squidfunk/mkdocs-material/blob/master/docs/setup/setting-up-the-header.md>