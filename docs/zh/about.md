# 说明

## 站点说明

本栈内容主要基于 Github 项目 [https://github.com/squidfunk/mkdocs-material/tree/master/docs](https://github.com/squidfunk/mkdocs-material/tree/master/docs) 内容翻译，结合 AI 与个人技术理解生成的内容。 

特殊情况请根据页面地步的联系方式进行沟通。


## 代办
- `<!-- md:option git-revision-date-localized.enabled -->` 这种文档，目前渲染不支持，可能在软件依赖或配置上没有处理好

## 翻译说明

### Material-for-MkDocs

- 翻译：__Material-for-MkDocs__

原意是指基于 `MkDocs` 且符合 `Material` 设计规划的一种主题，但结合当前的项目名称 _Material-for-MkDocs_ ，可以作为一个整体的名称来进行处理，不需要特殊的翻译，所以以连字符 `-` 组合这些词语。


### Front Matter

- 翻译：__元数据__

在 Markdown 文档场景中，Front Matter 表示文档顶部基于 yaml 语法声明的一段内容，在很多 Markdown 转换工具中会识别该内容进行配置。

它是位于 HTML/Markdown 文件的顶部遵循 YAML 语法的一块区域。

不直接翻译成“前言”，是因为在中文中“前言”的内容读者是可以看到的，到 front matter 的内容读者不能直接查看。

例如

``` yaml
---
tags:
  - foo
  - bar
---

# 文章的内容
...
```

### Slug

- 翻译：slug（最好的翻译就是不翻译了，找不到合适的精简词语）

“slug”通常指的是一个简短、唯一且对人类友好的字符串，用于标识和访问网页或内容。

示例：

1、例如在WordPress等博客平台中，文章的标题会被自动转换为一个slug，用作文章的 URL 的一部分。

2、例如在 Github中，slug必须遵循 `<用户名>/<存储库>` 的模式：

!!! tip "URL、URI、SLUG URN 区别"

    定义范围：URI是一个更广泛的概念，包括URL和URN。URL是URI的一种特定类型，专门用于定位资源。Slug则是URL中的一个组成部分，用于标识和访问特定的网页或内容。URN则是URI的一种特定类型，通过名称来标识资源。

    功能差异：URI的主要功能是唯一地标识资源。URL的主要功能是描述如何在网络上定位资源。Slug的主要功能是改善URL的可读性和用户体验。URN的主要功能是提供一种持久、稳定的资源标识方式。

### landing page

- 翻译：推广宣传页

用户首次访问网站时所到达的网页，尤其是针对营销活动或潜在客户生成进行优化的网页。 [解释说明](https://www.bilibili.com/video/BV1X5411c7XQ/?vd_source=ebb9c7018c3a75c1ab28e80c200aec6f)

### social cards

- 翻译：社交名片

是一种便捷、高效的社交工具，它能够帮助人们在各种社交场合中快速建立联系、分享信息并促进交流与合作。直接翻译成 __社交卡片__ 的有点生硬，

### Post URLs

- 翻译：文章 URLs