---
title: Built-in blog plugin
icon: material/newspaper-variant-outline
---

# 内置博客插件

该博客插件使得创建博客变得非常轻松，无论是作为您文档的附属品还是作为主要内容。您可以专注于内容创作，而插件则负责繁重的任务，包括生成所有最新文章的视图、[归档][archive]和[分类][category] 页面、可配置的[分页][pagination]功能等等。


  [archive]: #archive
  [category]: #categories
  [pagination]: #pagination

## 目标

### 如何工作

该插件会扫描已配置的[`posts`目录][config.post_dir]，查找`.md`文件，并据此自动生成分页视图[^1]。如果未进行其他配置，该插件会默认您的项目具有以下目录结构，并且会为您创建任何缺失的目录或文件：

  [^1]:
    视图是自动生成的页面，即博客的入口点，它列出所有最新的文章，以及[归档][archive]和[分类][category]页面，这些页面会按照时间顺序列出与它们通过[元数据][metadata]相关联的所有文章。

``` { .sh .no-copy }
.
├─ docs/
│  └─ blog/
│     ├─ posts/
│     └─ index.md
└─ mkdocs.yml
```

位于 [`blog`目录][config.blog_dir] 中的`index.md`文件是您的博客的入口点——一个分页视图，按逆时间顺序列出所有文章。除此之外，该插件还支持自动创建[归档][archive]和[分类][category]页面，这些页面会列出特定时间段或分类下的一组文章。

[文章 URL][config.post_url_format] 完全可自定义，无论您是否希望URL中包含帖子的日期。渲染后的日期始终根据您项目的[站点语言][site language]的本地设置显示。与其他静态博客框架一样，帖子可以使用各种[元数据][metadata]进行注解，从而轻松与其他[内置插件][built-in plugins]集成，例如[社交][social]和[标签][tags]插件。

文章可以根据您的具体需求，使用适合的目录布局组织在嵌套文件夹中，并且可以利用 MkDocs-For-Material 主题提供的所有组件和语法，包括[警告][admonitions]、[注解][annotations]、[代码块][code blocks]、[内容选项卡][content tabs]、[图表][diagrams]、[图标][icons],、[数学公式][math]等更多功能。


  [metadata]: #metadata
  [built-in plugins]: index.md
  [social]: social.md
  [tags]: tags.md
  [admonitions]: ../reference/admonitions.md
  [annotations]: ../reference/annotations.md
  [code blocks]: ../reference/code-blocks.md
  [content tabs]: ../reference/content-tabs.md
  [diagrams]: ../reference/diagrams.md
  [icons]: ../reference/icons-emojis.md
  [math]: ../reference/math.md

### 何时使用它


如果您想为您的项目添加一个博客，或者因为其出色的技术写作能力而从另一个博客框架迁移到Material-for-MkDocs，那么这个插件是一个绝佳的选择，因为它能够与其他许多内置插件完美集成：


<div class="grid cards" markdown>

-   :material-file-tree: &nbsp; __[Built-in meta plugin][meta]__

    ---

    The meta plugin makes it easy to apply [metadata] to a subset of posts,
    including authors, tags, categories, draft status, as well as social card
    layouts.

    ---

    __Simpler organization, categorization and management of post metadata__

-   :material-share-circle: &nbsp; __[Built-in social plugin][social]__

    ---

    The social plugin automatically generates beautiful and customizable
    social cards for each post and page, showing as previews on social media.

    ---

    __Links to your blog render beautiful social cards when shared on social
    media__

-   :material-rabbit: &nbsp; __[Built-in optimize plugin][optimize]__

    ---

    The optimize plugin automatically identifies and optimizes all media files
    that you reference in your project by using compression and conversion
    techniques.

    ---

    __Your blog loads faster as smaller images are served to your users__

-   :material-tag-text: &nbsp; __[Built-in tags plugin][tags]__

    ---

    The tags plugin allows to categorize posts alongside with pages in your
    project, to improve their discoverability and connect posts to your
    documentation.

    ---

    __Your documentation's tag system integrates with your blog__

</div>

  [meta]: meta.md
  [social]: social.md
  [optimize]: optimize.md
  [tags]: tags.md

## 配置

<!-- md:version 9.2.0 -->
<!-- md:plugin [blog] – built-in -->
<!-- md:flag multiple -->
<!-- md:flag experimental -->


与所有[内置插件][built-in plugins]一样，使用博客插件入门非常简单。只需在`mkdocs.yml`文件中添加以下几行，您就可以开始撰写您的第一篇帖子了：

``` yaml
plugins:
  - blog
```

博客插件已内置于Material-for-MkDocs中，无需单独安装。

  [blog]: blog.md
  [built-in plugins]: index.md

### 导航

如果您在`mkdocs.yml`中没有配置站点导航，那么无需进行其他操作。博客的[归档][archive]和[分类][category]页面将自动显示在自动生成的导航下方。

如果您已经定义了导航结构，则需要指定博客应该在此结构中的哪个位置。为博客创建一个[带有索引页面的导航部分][navigation section with an index page]：

```yaml
theme:
  name: material
  features:
    - navigation.indexes
nav:
  - ...
  - Blog:
    - blog/index.md
```


[归档][archive]和[分类][category]页面将作为博客部分中页面下方的子部分出现在该部分内。在这种情况下，它们将出现在`index.md`之后。`index.md`文件的路径必须与[`blog_dir`][config.blog_dir]相匹配。这意味着您可以为博客导航条目命名任何您喜欢的名称：“Blog”（博客）、“News”（新闻）或“Tips”（技巧）等。


[navigation section with an index page]: ../setup/setting-up-navigation.md#section-index-pages

### 概述

以下设置可供使用：

---

#### <!-- md:setting config.enabled -->

<!-- md:version 9.2.0 -->
<!-- md:default `true` -->


使用此设置在[构建项目][building your project]时启用或禁用插件。通常无需指定此设置，但如果您想禁用插件，请使用：



``` yaml
plugins:
  - blog:
      enabled: false
```

  [building your project]: ../creating-your-site.md#building-your-site

---

#### <!-- md:setting config.blog_dir -->

<!-- md:version 9.2.0 -->
<!-- md:default `blog` -->


使用此设置来更改博客在[`docs`目录][mkdocs.docs_dir]中的位置路径。该路径将作为所有博客和视图的前缀包含在生成的URL中。您可以使用以下方式更改它：

=== "Documentation + Blog"

    ``` yaml
    plugins:
      - blog:
          blog_dir: blog
    ```

=== "Blog only"

    ``` yaml
    plugins:
      - blog:
          blog_dir: .
    ```


提供的路径是从[`docs`目录][mkdocs.docs_dir]解析得到的。




#### <!-- md:setting config.blog_toc -->

<!-- md:version 9.2.0 -->
<!-- md:default `false` -->

使用此设置可以利用目录来在视图中显示文章标题。如果您的文章摘要较长，这可能会很有用。如果您想启用它，请使用：

``` yaml
plugins:
  - blog:
      blog_toc: true
```

### 文章


帖子具有以下可用设置：


---

#### <!-- md:setting config.post_dir -->

<!-- md:version 9.2.0 -->
<!-- md:default `{blog}/posts` -->


使用此设置来更改帖子所在的文件夹。通常无需更改此设置，但如果您想重命名文件夹或更改其文件系统位置，请使用：


``` yaml
plugins:
  - blog:
      post_dir: "{blog}/articles"
```

请注意，[`posts`目录][config.post_dir]仅用于组织帖子——它不会包含在帖子的URL中，因为这些URL是由本插件自动且方便地生成的。

以下占位符可用：

- `blog` – [`blog` directory][config.blog_dir]

The provided path is resolved from the [`docs` directory][mkdocs.docs_dir].

---

#### <!-- md:setting config.post_date_format -->

<!-- md:version 9.2.0 -->
<!-- md:default `long` -->

使用此设置可以更改帖子的日期格式。本插件使用[babel]来以配置的[站点语言][site language]呈现日期。您可以使用[babel]的[模式语法][pattern syntax]或以下简写代码：


=== "Monday, January 31, 2024"

    ``` yaml
    plugins:
      - blog:
          post_date_format: full
    ```

=== "January 31, 2024"

    ``` yaml
    plugins:
      - blog:
          post_date_format: long
    ```

=== "Jan 31, 2024"

    ``` yaml
    plugins:
      - blog:
          post_date_format: medium
    ```

=== "1/31/24"

    ``` yaml
    plugins:
      - blog:
          post_date_format: short
    ```


请注意，根据[站点语言][site language]的不同，其他语言的显示结果可能会有所不同。


  [babel]: https://pypi.org/project/Babel/
  [site language]: ../setup/changing-the-language.md#site-language
  [pattern syntax]: https://babel.pocoo.org/en/latest/dates.html#pattern-syntax

---

#### <!-- md:setting config.post_url_date_format -->

<!-- md:version 9.2.0 -->
<!-- md:default `yyyy/MM/dd` -->


使用此设置可以更改帖子URL中使用的日期格式。格式字符串必须遵循[babel]的[模式语法][pattern syntax]，并且不应包含空格。一些常见的选择包括：



=== ":material-link: blog/2024/01/31/:material-dots-horizontal:/"

    ``` yaml
    plugins:
      - blog:
          post_url_date_format: yyyy/MM/dd
    ```

=== ":material-link: blog/2024/01/:material-dots-horizontal:/"

    ``` yaml
    plugins:
      - blog:
          post_url_date_format: yyyy/MM
    ```

=== ":material-link: blog/2024/:material-dots-horizontal:/"

    ``` yaml
    plugins:
      - blog:
          post_url_date_format: yyyy
    ```


如果您想从帖子URL中删除日期，例如，当您的博客主要发布常青内容时，您可以从[`post_url_format`][config.post_url_format]格式字符串中删除date占位符。



#### <!-- md:setting config.post_url_format -->

<!-- md:version 9.2.0 -->
<!-- md:default `{date}/{slug}` -->

使用此设置可以更改生成帖子URL时使用的格式字符串。您可以自由组合占位符，并使用斜杠或其他字符将它们连接起来：


=== ":material-link: blog/2024/:material-dots-horizontal:/"

    ``` yaml
    plugins:
      - blog:
          post_url_format: "{date}/{slug}"
    ```

=== ":material-link: blog/:material-dots-horizontal:/"

    ``` yaml
    plugins:
      - blog:
          post_url_format: "{slug}"
    ```

以下占位符可用：



- `categories` – 帖子类别，使用[`categories_slugify`][config.categories_slugify]进行slug化（即生成URL友好的字符串）
- `date` – 帖子日期，使用[`post_url_date_format`][config.post_url_date_format]进行格式化
- `slug` – 帖子标题，使用[`post_slugify`][config.post_slugify]进行slug化，或者通过元数据属性[`slug`][meta.slug]明确设置
- `file` –帖子文件名（不包含`.md`文件扩展名）


如果您移除了`date`占位符，请确保帖子URL不会与托管在[`blog`目录][config.blog_dir]下的其他页面的URL冲突，因为这会导致未定义行为。



#### <!-- md:setting config.post_url_max_categories -->

<!-- md:version 9.2.0 -->
<!-- md:default `1` -->

如果`categories`占位符是[`post_url_format`][config.post_url_format]的一部分，并且帖子定义了类别，则可以使用此设置来设置包含在帖子URL中的类别数量的上限：


``` yaml
plugins:
  - blog:
      post_url_format: "{categories}/{slug}"
      post_url_max_categories: 2
```

如果指定了多个类别，它们将在slug化之后使用`/`连接起来。


#### <!-- md:setting config.post_slugify -->

<!-- md:version 9.2.0 -->
<!-- md:default [`pymdownx.slugs.slugify`][pymdownx.slugs.slugify] -->

使用此设置可以更改从帖子标题生成与URL兼容的slug的函数。默认情况下，将使用[Python Markdown Extensions]中的[`slugify`][pymdownx.slugs.slugify]函数，如下所示：


``` yaml
plugins:
  - blog:
      post_slugify: !!python/object/apply:pymdownx.slugs.slugify
        kwds:
          case: lower
```

默认配置支持 Unicode，应该能够为所有语言生成良好的 slug。当然，您也可以提供自定义的slug化函数以实现更精细的控制。

  [pymdownx.slugs.slugify]: https://github.com/facelessuser/pymdown-extensions/blob/01c91ce79c91304c22b4e3d7a9261accc931d707/pymdownx/slugs.py#L59-L65
  [Python Markdown Extensions]: https://facelessuser.github.io/pymdown-extensions/extras/slugs/

---

#### <!-- md:setting config.post_slugify_separator -->

<!-- md:version 9.2.0 -->
<!-- md:default `-` -->

Use this setting to change the separator that is passed to the slugification
function set as part of [`post_slugify`][config.post_slugify]. While the default
is a hyphen, it can be set to any string, e.g., `_`:

``` yaml
plugins:
  - blog:
      post_slugify_separator: _
```

---

#### <!-- md:setting config.post_excerpt -->

<!-- md:version 9.2.0 -->
<!-- md:default `optional` -->

By default, the plugin makes [post excerpts](../setup/setting-up-a-blog.md#adding-an-excerpt)
optional. When a post doesn't define an excerpt, views include the entire post.
This setting can be used to make post excerpts required:

=== "Optional"

    ``` yaml
    plugins:
      - blog:
          post_excerpt: optional
    ```

=== "Required"

    ``` yaml
    plugins:
      - blog:
          post_excerpt: required
    ```

When post excerpts are required, posts without excerpt separators raise an
error. Thus, this setting is useful when you want to make sure that all posts
have excerpts defined.

---

#### <!-- md:setting config.post_excerpt_max_authors -->

<!-- md:version 9.2.0 -->
<!-- md:default `1` -->

Use this setting to set an upper bound for the number of authors rendered in
post excerpts. While each post may be written by multiple authors, this setting
allows to limit the display to just a few or even a single author, or disable
authors in post excerpts:

=== "Render up to 2 authors"

    ``` yaml
    plugins:
      - blog:
          post_excerpt_max_authors: 2
    ```

=== "Disable authors"

    ``` yaml
    plugins:
      - blog:
          post_excerpt_max_authors: 0
    ```

This only applies to post excerpts in views. Posts always render all authors.

---

#### <!-- md:setting config.post_excerpt_max_categories -->

<!-- md:version 9.2.0 -->
<!-- md:default `5` -->

Use this setting to set an upper bound for the number of categories rendered in
post excerpts. While each post may be assigned to multiple categories, this
setting allows to limit the display to just a few or even a single category, or
disable categories in post excerpts:

=== "Render up to 2 categories"

    ``` yaml
    plugins:
      - blog:
          post_excerpt_max_categories: 2
    ```

=== "Disable categories"

    ``` yaml
    plugins:
      - blog:
          post_excerpt_max_categories: 0
    ```

This only applies to post excerpts in views. Posts always render all categories.

---

#### <!-- md:setting config.post_excerpt_separator -->

<!-- md:version 9.2.0 -->
<!-- md:default <code>&lt;!-- more --&gt;</code> -->

Use this setting to set the separator the plugin will look for in a post's
content when generating post excerpts. All content __before__ the separator is
considered to be part of the excerpt:

``` yaml
plugins:
  - blog:
      post_excerpt_separator: <!-- more -->
```

It is common practice to use an HTML comment as a separator.

---

#### <!-- md:setting config.post_readtime -->

<!-- md:version 9.2.0 -->
<!-- md:default `true` -->

Use this setting to control whether the plugin should automatically compute the
reading time of a post, which is then rendered in post excerpts, as well as in
posts themselves:

``` yaml
plugins:
  - blog:
      post_readtime: false
```

---

#### <!-- md:setting config.post_readtime_words_per_minute -->

<!-- md:version 9.2.0 -->
<!-- md:default `265` -->

Use this setting to change the number of words that a reader is expected to read
per minute when computing the reading time of a post. If you want to fine-tune
it, use:

``` yaml
plugins:
  - blog:
      post_readtime_words_per_minute: 300
```

A reading time of 265 words per minute is considered to be the
[average reading time of an adult].

  [average reading time of an adult]: https://help.medium.com/hc/en-us/articles/214991667-Read-time

### Archive

The following settings are available for archive pages:

---

#### <!-- md:setting config.archive -->

<!-- md:version 9.2.0 -->
<!-- md:default `true` -->

Use this setting to enable or disable archive pages. An archive page shows all
posts for a specific interval (e.g. year, month, etc.) in reverse order. If you
want to disable archive pages, use:

``` yaml
plugins:
  - blog:
      archive: false
```

---

#### <!-- md:setting config.archive_name -->

<!-- md:version 9.2.0 -->
<!-- md:default computed -->

Use this setting to change the title of the archive section the plugin adds to
the navigation. If this setting is omitted, it's sourced from the translations.
If you want to change it, use:

``` yaml
plugins:
  - blog:
      archive_name: Archive
```

---

#### <!-- md:setting config.archive_date_format -->

<!-- md:version 9.2.0 -->
<!-- md:default `yyyy` -->

Use this setting to change the date format used for archive page titles. The
format string must adhere to [babel]'s [pattern syntax]. Some popular choices:

=== "2024"

    ``` yaml
    plugins:
      - blog:
          archive_date_format: yyyy
    ```

=== "January 2024"

    ``` yaml
    plugins:
      - blog:
          archive_date_format: MMMM yyyy
    ```

Note that depending on the [site language], results might look different for
other languages.

---

#### <!-- md:setting config.archive_url_date_format -->

<!-- md:version 9.2.0 -->
<!-- md:default `yyyy` -->

Use this setting to change the date format used for archive page URLs. The
format string must adhere to [babel]'s [pattern syntax] and should not contain
whitespace. Some popular choices:

=== ":material-link: blog/archive/2024/"

    ``` yaml
    plugins:
      - blog:
          archive_url_date_format: yyyy
    ```

=== ":material-link: blog/archive/2024/01/"

    ``` yaml
    plugins:
      - blog:
          archive_url_date_format: yyyy/MM
    ```

---

#### <!-- md:setting config.archive_url_format -->

<!-- md:version 9.2.0 -->
<!-- md:default `archive/{date}` -->

Use this setting to change the format string that is used when generating
archive page URLs. You can freely combine placeholders, and join them with
slashes or other characters:

=== ":material-link: blog/archive/2024/"

    ``` yaml
    plugins:
      - blog:
          archive_url_format: "archive/{date}"
    ```

=== ":material-link: blog/2024/"

    ``` yaml
    plugins:
      - blog:
          archive_url_format: "{date}"
    ```

The following placeholders are available:

- `date` – Archive date, formatted with [`archive_url_date_format`][config.archive_url_date_format]

---

#### <!-- md:setting config.archive_pagination -->

<!-- md:sponsors -->
<!-- md:version insiders-4.44.0 -->
<!-- md:default `true` -->

Use this setting to enable or disable pagination for archive pages. The value
of this setting is inherited from [`pagination`][config.pagination], unless it's
explicitly set. To disable pagination, use:

``` yaml
plugins:
  - blog:
      archive_pagination: false
```

---

#### <!-- md:setting config.archive_pagination_per_page -->

<!-- md:sponsors -->
<!-- md:version insiders-4.44.0 -->
<!-- md:default `10` -->

Use this setting to change the number of posts rendered per archive page. The
value of this setting is inherited from [`pagination_per_page`]
[config.pagination_per_page], unless it's explicitly set. To change it, use:

``` yaml
plugins:
  - blog:
      archive_pagination_per_page: 5
```

---

#### <!-- md:setting config.archive_toc -->

<!-- md:version 9.2.0 -->
<!-- md:default `false` -->

Use this setting to leverage the table of contents to display post titles on all
archive pages. The value of this setting is inherited from [`blog_toc`]
[config.blog_toc], unless it's explicitly set. To change it, use

``` yaml
plugins:
  - blog:
      archive_toc: true
```

### Categories

The following settings are available for category pages:

---

#### <!-- md:setting config.categories -->

<!-- md:version 9.2.0 -->
<!-- md:default `true` -->

Use this setting to enable or disable category pages. A category page shows all
posts for a specific category in reverse chronological order. If you want to
disable category pages, use:

``` yaml
plugins:
  - blog:
      categories: false
```

---

#### <!-- md:setting config.categories_name -->

<!-- md:version 9.2.0 -->
<!-- md:default computed -->

Use this setting to change the title of the category section the plugin adds to
the navigation. If this setting is omitted, it's sourced from the translations.
If you want to change it, use:

``` yaml
plugins:
  - blog:
      categories_name: Categories
```

---

#### <!-- md:setting config.categories_url_format -->

<!-- md:version 9.2.0 -->
<!-- md:default `category/{slug}` -->

Use this setting to change the format string that is used when generating
category page URLs. You can freely combine placeholders, and join them with
slashes or other characters:

=== ":material-link: blog/category/:material-dots-horizontal:/"

    ``` yaml
    plugins:
      - blog:
          categories_url_format: "category/{slug}"
    ```

=== ":material-link: blog/:material-dots-horizontal:/"

    ``` yaml
    plugins:
      - blog:
          categories_url_format: "{slug}"
    ```

The following placeholders are available:

- `slug` – Category, slugified with [`categories_slugify`][config.categories_slugify]

---

#### <!-- md:setting config.categories_slugify -->

<!-- md:version 9.2.0 -->
<!-- md:default [`pymdownx.slugs.slugify`][pymdownx.slugs.slugify] -->

Use this setting to change the function for generating URL-compatible slugs
from categories. By default, the [`slugify`][pymdownx.slugs.slugify] function
from [Python Markdown Extensions] is used as follows:

``` yaml
plugins:
  - blog:
      categories_slugify: !!python/object/apply:pymdownx.slugs.slugify
        kwds:
          case: lower
```

The default configuration is Unicode-aware and should produce good slugs for all
languages. Of course, you can also provide a custom slugification function for
more granular control.

---

#### <!-- md:setting config.categories_slugify_separator -->

<!-- md:version 9.2.0 -->
<!-- md:default `-` -->

Use this setting to change the separator that is passed to the slugification
function set as part of [`categories_slugify`][config.categories_slugify]. While
the default is a hyphen, it can be set to any string, e.g., `_`:

``` yaml
plugins:
  - blog:
      categories_slugify_separator: _
```

---

#### <!-- md:setting config.categories_sort_by -->

<!-- md:sponsors -->
<!-- md:version insiders-4.45.0 -->
<!-- md:default `material.plugins.blog.view_name` -->

Use this setting to specify a custom function for sorting categories. For
example, if you want to sort categories by the number of posts they contain,
use the following configuration:

``` yaml
plugins:
  - blog:
      categories_sort_by: !!python/name:material.plugins.blog.view_post_count
```

Don't forget to enable [`categories_sort_reverse`][config.categories_sort_reverse].
You can define your own comparison function, which must return something
that can be compared while sorting, i.e., a string or number.

---

#### <!-- md:setting config.categories_sort_reverse -->

<!-- md:sponsors -->
<!-- md:version insiders-4.45.0 -->
<!-- md:default `false` -->

Use this setting to reverse the order in which categories are sorted. By
default, categories are sorted in ascending order, but you can reverse ordering
as follows:

``` yaml
plugins:
  - blog:
      categories_sort_reverse: true
```

---

#### <!-- md:setting config.categories_allowed -->

<!-- md:version 9.2.0 -->
<!-- md:default none -->

The plugin allows to check categories against a predefined list, in order to
catch typos or make sure that categories are not arbitrarily added. Specify the
categories you want to allow with:

``` yaml
plugins:
  - blog:
      categories_allowed:
        - Search
        - Performance
```

The plugin stops the build if a post references a category that is not part of
this list. Posts can be assigned to categories by using the [`categories`]
[meta.categories] metadata property.

---

#### <!-- md:setting config.categories_pagination -->

<!-- md:sponsors -->
<!-- md:version insiders-4.44.0 -->
<!-- md:default `true` -->

Use this setting to enable or disable pagination for category pages. The value
of this setting is inherited from [`pagination`][config.pagination], unless it's
explicitly set. To disable pagination, use:

``` yaml
plugins:
  - blog:
      categories_pagination: false
```

---

#### <!-- md:setting config.categories_pagination_per_page -->

<!-- md:sponsors -->
<!-- md:version insiders-4.44.0 -->
<!-- md:default `10` -->

Use this setting to change the number of posts rendered per category page. The
value of this setting is inherited from [`pagination_per_page`]
[config.pagination_per_page], unless it's explicitly set. To change it, use:

``` yaml
plugins:
  - blog:
      categories_pagination_per_page: 5
```

---

#### <!-- md:setting config.categories_toc -->

<!-- md:version 9.2.0 -->
<!-- md:default `false` -->

Use this setting to leverage the table of contents to display post titles on all
category pages. The value of this setting is inherited from [`blog_toc`]
[config.blog_toc], unless it's explicitly set. To change it, use:

``` yaml
plugins:
  - blog:
      categories_toc: true
```

### Authors

The following settings are available for authors:

---

#### <!-- md:setting config.authors -->

<!-- md:version 9.2.0 -->
<!-- md:default `true` -->

Use this setting to enable or disable post authors. If this setting is enabled,
the plugin will look for a file named [`.authors.yml`][config.authors_file] and
render authors in posts and views. Disable this behavior with:

``` yaml
plugins:
  - blog:
      authors: false
```

---

#### <!-- md:setting config.authors_file -->

<!-- md:version 9.2.0 -->
<!-- md:default `{blog}/.authors.yml` -->

Use this setting to change the path of the file where the author information for
your posts resides. It's normally not necessary to change this setting, but if
you need to, use:

``` yaml
plugins:
  - blog:
      authors_file: "{blog}/.authors.yml"
```

The following placeholders are available:

- `blog` – [`blog` directory][config.blog_dir]

The provided path is resolved from the [`docs` directory][mkdocs.docs_dir].

!!! info "Format of author information"

    The `.authors.yml` file must adhere to the following format:

    ``` yaml title=".authors.yml"
    authors:
      <author>:
        name: string        # Author name
        description: string # Author description
        avatar: url         # Author avatar
        slug: url           # Author profile slug
        url: url            # Author website URL
    ```

    Note that `<author>` must be set to an identifier for associating authors
    with posts, e.g., a GitHub username like `squidfunk`. This identifier can
    then be used in the [`authors`][meta.authors] metadata property of
    a post. Multiple authors are supported. As an example, see
    [the `.authors.yml` file][.authors.yml] we're using for our blog.

  [.authors.yml]: https://github.com/squidfunk/mkdocs-material/blob/master/docs/blog/.authors.yml

---

#### <!-- md:setting config.authors_profiles -->

<!-- md:sponsors -->
<!-- md:version insiders-4.46.0 -->
<!-- md:default `false` -->

Use this setting to enable or disable automatically generated author profiles.
An author profile shows all posts by an author in reverse chronological order.
You can enable author profiles with:

``` yaml
plugins:
  - blog:
      authors_profiles: true
```

---

#### <!-- md:setting config.authors_profiles_name -->

<!-- md:sponsors -->
<!-- md:version insiders-4.46.0 -->
<!-- md:default computed -->

Use this setting to change the title of the authors section the plugin adds to
the navigation. If this setting is omitted, it's sourced from the translations.
If you want to change it, use:

``` yaml
plugins:
  - blog:
      authors_profiles_name: Authors
```

---

#### <!-- md:setting config.authors_profiles_url_format -->

<!-- md:sponsors -->
<!-- md:version insiders-4.46.0 -->
<!-- md:default `author/{slug}` -->

Use this setting to change the format string that is used when generating
author profile URLs. You can freely combine placeholders, and join them with
slashes or other characters:

=== ":material-link: blog/author/:material-dots-horizontal:/"

    ``` yaml
    plugins:
      - blog:
          authors_profiles_url_format: "author/{slug}"
    ```

=== ":material-link: blog/:material-dots-horizontal:/"

    ``` yaml
    plugins:
      - blog:
          authors_profiles_url_format: "{slug}"
    ```

The following placeholders are available:

- `slug` – Author slug or identifier from [`authors_file`][config.authors_file]
- `name` – Author name from [`authors_file`][config.authors_file]

---

#### <!-- md:setting config.authors_profiles_pagination -->

<!-- md:sponsors -->
<!-- md:version insiders-4.46.0 -->
<!-- md:default `true` -->

Use this setting to enable or disable pagination for author profiles. The value
of this setting is inherited from [`pagination`][config.pagination], unless it's
explicitly set. To disable pagination, use:

``` yaml
plugins:
  - blog:
      authors_profiles_pagination: false
```

---

#### <!-- md:setting config.authors_profiles_pagination_per_page -->

<!-- md:sponsors -->
<!-- md:version insiders-4.46.0 -->
<!-- md:default `10` -->

Use this setting to change the number of posts rendered per archive page. The
value of this setting is inherited from [`pagination_per_page`]
[config.pagination_per_page], unless it's explicitly set. To change it, use:

``` yaml
plugins:
  - blog:
      authors_profiles_pagination_per_page: 5
```

---

#### <!-- md:setting config.authors_profiles_toc -->

<!-- md:sponsors -->
<!-- md:version insiders-4.46.0 -->
<!-- md:default `false` -->

Use this setting to leverage the table of contents to display post titles on all
author profiles. The value of this setting is inherited from [`blog_toc`]
[config.blog_toc], unless it's explicitly set. To change it, use:

``` yaml
plugins:
  - blog:
      authors_profiles_toc: true
```

### Pagination

The following settings are available for pagination:

---

#### <!-- md:setting config.pagination -->

<!-- md:version 9.2.0 -->
<!-- md:default `true` -->

Use this setting to enable or disable pagination in views – generated pages
that show posts or subsets of posts in reverse chronological order. If you want
to disable pagination, use:

``` yaml
plugins:
  - blog:
      pagination: false
```

---

#### <!-- md:setting config.pagination_per_page -->

<!-- md:version 9.2.0 -->
<!-- md:default `10` -->

Use this setting to change the number of posts rendered per page. If you have
rather long post excerpts, it can be a good idea to reduce the number of posts
per page:

``` yaml
plugins:
  - blog:
      pagination_per_page: 5
```

---

#### <!-- md:setting config.pagination_url_format -->

<!-- md:version 9.2.0 -->
<!-- md:default `page/{page}` -->

Use this setting to change the format string that is used when generating
paginated view URLs. You can freely combine placeholders, and join them with
slashes or other characters:

=== ":material-link: blog/page/n/"

    ``` yaml
    plugins:
      - blog:
          pagination_url_format: "page/{page}"
    ```

=== ":material-link: blog/n/"

    ``` yaml
    plugins:
      - blog:
          pagination_url_format: "{page}"
    ```

The following placeholders are available:

- `page` – Page number

---

#### <!-- md:setting config.pagination_format -->

<!-- md:version 9.2.0 -->
<!-- md:default `~2~` -->

The plugin uses the [paginate] module to generate the pagination markup using a
special syntax. Use this setting to customize how pagination is constructed.
Some popular choices:

=== "1 2 3 .. n"

    ``` yaml
    plugins:
      - blog:
          pagination_format: "~2~"
    ```

=== "1 2 3 .. n :material-chevron-right: :material-chevron-double-right:"

    ``` yaml
    plugins:
      - blog:
          pagination_format: "$link_first $link_previous ~2~ $link_next $link_last"
    ```

=== "1 :material-chevron-right:"

    ``` yaml
    plugins:
      - blog:
          pagination_format: "$link_previous $page $link_next"
    ```

The following placeholders are supported by [paginate]:

- `#!css $first_page` – Number of first reachable page
- `#!css $last_page` – Number of last reachable page
- `#!css $page` – Number of currently selected page
- `#!css $page_count` – Number of reachable pages
- `#!css $items_per_page` – Maximal number of items per page
- `#!css $first_item` – Index of first item on the current page
- `#!css $last_item` – Index of last item on the current page
- `#!css $item_count` – Total number of items
- `#!css $link_first` – Link to first page (unless on first page)
- `#!css $link_last` – Link to last page (unless on last page)
- `#!css $link_previous` – Link to previous page (unless on first page)
- `#!css $link_next` – Link to next page (unless on last page)

  [paginate]: https://pypi.org/project/paginate/

---

#### <!-- md:setting config.pagination_if_single_page -->

<!-- md:version 9.2.0 -->
<!-- md:default `false` -->

Use this setting to control whether pagination should be automatically disabled
when the view only consists of a single page. If you want to always render
pagination, use:

``` yaml
plugins:
  - blog:
      pagination_if_single_page: true
```

---

#### <!-- md:setting config.pagination_keep_content -->

<!-- md:version 9.2.0 -->
<!-- md:default `false` -->

Use this setting to enable or disable persistence of content, i.e., if paginated
views should also display the content of their containing view. If you want to
enable this behavior, use:

``` yaml
plugins:
  - blog:
      pagination_keep_content: true
```

### Drafts

The following settings are available for drafts:

---

#### <!-- md:setting config.draft -->

<!-- md:version 9.2.0 -->
<!-- md:default `false` -->

Rendering [draft posts][meta.draft] can be useful in deploy previews. Use this
setting to specify whether the plugin should include posts marked as drafts when
[building your project]:

=== "Render drafts"

    ``` yaml
    plugins:
      - blog:
          draft: true
    ```

=== "Don't render drafts"

    ``` yaml
    plugins:
      - blog:
          draft: false
    ```

---

#### <!-- md:setting config.draft_on_serve -->

<!-- md:version 9.2.0 -->
<!-- md:default `true` -->

Use this setting to control whether the plugin should include posts marked as
drafts when [previewing your site]. If you don't wish to include draft posts
when previewing, use:

``` yaml
plugins:
  - blog:
      draft_on_serve: false
```

  [previewing your site]: ../creating-your-site.md#previewing-as-you-write

---

#### <!-- md:setting config.draft_if_future_date -->

<!-- md:version 9.2.0 -->
<!-- md:default `false` -->

The plugin can automatically mark posts with future dates as drafts. When the
date is past today, the post is automatically included when
[building your project], unless explicitly marked as draft:

``` yaml
plugins:
  - blog:
      draft_if_future_date: true
```

## Usage

### Metadata

Posts can define a handful of metadata properties that specify how the plugin
renders them, in which views they are integrated, and how they are linked to
each other. The metadata of each post is validated against a schema to allow for
a quicker discovery of syntax errors.

The following properties are available:

---

#### <!-- md:setting meta.authors -->

<!-- md:version 9.2.0 -->
<!-- md:flag metadata -->
<!-- md:default none -->

Use this property to associate a post with [authors] by providing a list of
identifiers as defined in the [`authors_file`][config.authors_file]. If an
author can't be resolved, the plugin will terminate with an error:

``` yaml
---
authors:
  - squidfunk # (1)!
---

# Post title
...
```

1.  Authors are linked by using their identifiers. As an example, see
    [the `.authors.yml` file][.authors.yml] we're using for our blog.

  [authors]: #authors

---

#### <!-- md:setting meta.categories -->

<!-- md:version 9.2.0 -->
<!-- md:flag metadata -->
<!-- md:default none -->

Use this property to associate a post with one or more [categories][category],
making the post a part of the generated category page. Categories are defined
as a list of strings (whitespaces are allowed):

``` yaml
---
categories:
  - Search
  - Performance
---

# Post title
...
```

If you want to prevent accidental typos assigning categories to posts, you
can set a predefined list of allowed categories in `mkdocs.yml` by using
the [`categories_allowed`][config.categories_allowed] setting.

---

#### <!-- md:setting meta.date -->

<!-- md:version 9.2.0 -->
<!-- md:flag metadata -->
<!-- md:flag required -->

Use this property to specify a post's date. Note that this property is required,
which means the build fails when it's not set. Additional dates can be set by
using a slightly different syntax:

=== "Date"

    ``` yaml
    ---
    date: 2024-01-31
    ---

    # Post title
    ...
    ```

=== "Update date"

    ``` yaml
    ---
    date:
      created: 2024-01-31 # (1)!
      updated: 2024-02-01
    ---

    # Post title
    ...
    ```

    1.  Each post must have a creation date set.

=== "Custom date"

    ``` yaml
    ---
    date:
      created: 2024-01-31
      my_custom_date: 2024-02-01 # (1)!
    ---

    # Post title
    ...
    ```

    1.  The blog plugin validates all dates and allows to format them with
        [babel]'s [pattern syntax] in templates. When using theme extension,
        authors can add custom dates to templates.

        This was first requested in #5733.

The following date formats are supported:

- `2024-01-31`
- `2024-01-31T12:00:00`

---

#### <!-- md:setting meta.draft -->

<!-- md:version 9.2.0 -->
<!-- md:flag metadata -->
<!-- md:default none -->

Use this property to mark a post as draft. The plugin allows to include or
exclude posts marked as drafts when [building your project] using the
[`draft`][config.draft] setting. Mark a post as draft with:

``` yaml
---
draft: true
---

# Post title
...
```

---

#### <!-- md:setting meta.pin -->

<!-- md:sponsors -->
<!-- md:version insiders-4.53.0 -->
<!-- md:flag metadata -->
<!-- md:default `false` -->
<!-- md:flag experimental -->

Use this property to pin a post to the top of a view. In case multiple posts are
pinned, the pinned posts are sorted by descending order and appear before all
other posts. Pin a post with:

``` yaml
---
pin: true
---

# Post title
...
```

---

#### <!-- md:setting meta.links -->

<!-- md:sponsors -->
<!-- md:version insiders-4.23.0 -->
<!-- md:flag metadata -->
<!-- md:default none -->
<!-- md:flag experimental -->

Use this property to define a list of links that are rendered in the sidebar of
a post. The property follows the same syntax as [`nav`][mkdocs.nav] in
`mkdocs.yml`, supporting sections and even anchors:

=== "Links"

    ``` yaml
    ---
    links:
      - setup/setting-up-site-search.md
      - insiders/index.md
    ---

    # Post title
    ...
    ```

=== "Links with sections"

    ``` yaml
    ---
    links:
      - setup/setting-up-site-search.md
      - Insiders:
        - insiders/index.md
        - insiders/getting-started.md
    ---

    # Post title
    ...
    ```

=== "Links with anchors"

    ``` yaml
    ---
    links:
      - plugins/search.md # (1)!
      - Insiders:
        - insiders/how-to-sponsor.md
        - insiders/getting-started.md#requirements
    ---

    # Post title
    ...
    ```

    1.  If a link defines an anchor, the plugin resolves the anchor from the
        linked page and sets the anchor title as a [subtitle].

All relative links are resolved from the [`docs` directory][mkdocs.docs_dir].

  [subtitle]: ../reference/index.md#setting-the-page-subtitle

---

#### <!-- md:setting meta.readtime -->

<!-- md:version 9.2.0 -->
<!-- md:flag metadata -->
<!-- md:default computed -->

Use this property to explicitly set the reading time of a post in minutes. When
[`post_readtime`][config.post_readtime] is enabled, the plugin computes the
reading time of a post, which can be overridden with:

``` yaml
---
readtime: 15
---

# Post title
...
```

---

#### <!-- md:setting meta.slug -->

<!-- md:version 9.2.0 -->
<!-- md:flag metadata -->
<!-- md:default computed -->

Use this property to explicitly set the slug of a post. By default, the slug of
a post is automatically computed by the [`post_slugify`][config.post_slugify]
function from the post's title, which can be overridden with:

``` yaml
---
slug: help-im-trapped-in-a-universe-factory
---

# Post title
...
```

Slugs are passed to [`post_url_format`][config.post_url_format].

---

!!! question "Missing something?"

    When setting up your blog or migrating from another blog framework, you
    might discover that you're missing specific functionality – we're happy to
    consider adding it to the plugin! You can [open a discussion] to
    ask a question, or create a [change request] on our [issue tracker], so we
    can find out if it might be a good fit for the plugin.

  [open a discussion]: https://github.com/squidfunk/mkdocs-material/discussions
  [change request]: ../contributing/requesting-a-change.md
  [issue tracker]: https://github.com/squidfunk/mkdocs-material/issues

## 参考
- <https://github.com/squidfunk/mkdocs-material/blob/master/docs/plugins/blog.md>